﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ScrollRectAutoScroller : MonoBehaviour, IBeginDragHandler, IEndDragHandler
{
    [SerializeField] ScrollRect rect;
    bool isDragging;
	// Use this for initialization
	void Start () {

        isDragging = false;
    }
	
	// Update is called once per frame
	void Update () {
        if(!isDragging)
        rect.verticalNormalizedPosition = 0;
    }

    public void OnBeginDrag(PointerEventData data)
    {
        isDragging = true;
    }

    public void OnEndDrag(PointerEventData data)
    {
        isDragging = false;
    }
}
