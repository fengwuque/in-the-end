﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
using System.IO;

public class ScriptableObjectExporter:MonoBehaviour
{
    public EquipmentDataScriptableObject eqData;
    public string _filePath = "";
    public ScriptableObject data;

    public void ExportEQ()
    {
        string dataAsJson = JsonUtility.ToJson(eqData.dmgRatio);
        
        string filePath = Application.dataPath + _filePath + "dmgRatio.json";
        
        File.WriteAllText(filePath, dataAsJson);

        dataAsJson = JsonUtility.ToJson(eqData.eqStats);
        filePath = Application.dataPath + _filePath + "eqStats.json";
        File.WriteAllText(filePath, dataAsJson);

        dataAsJson = JsonUtility.ToJson(eqData.equipmentSetBonuses);
        filePath = Application.dataPath + _filePath + "setBonuses.json";
        File.WriteAllText(filePath, dataAsJson);

        dataAsJson = JsonUtility.ToJson(eqData.equipmentSetsBlueprint);
        filePath = Application.dataPath + _filePath + "setsBlueprint.json";
        File.WriteAllText(filePath, dataAsJson);

        dataAsJson = JsonUtility.ToJson(eqData.rollableStatsInEquipmentParts);
        filePath = Application.dataPath + _filePath + "rollStats.json";
        File.WriteAllText(filePath, dataAsJson);

        dataAsJson = JsonUtility.ToJson(eqData.weaponData);
        filePath = Application.dataPath + _filePath + "weaponData.json";
        File.WriteAllText(filePath, dataAsJson);

        dataAsJson = JsonUtility.ToJson(eqData);
        filePath = Application.dataPath + _filePath + "AllData.json";
        File.WriteAllText(filePath, dataAsJson);
    }

    public void ExportScriptableObject()
    {
        string filePath = Application.dataPath + _filePath + data.name + ".json";

        string dataAsJson = JsonUtility.ToJson(data);
        Debug.Log(filePath);
        File.WriteAllText(filePath, dataAsJson);
    }
}

#endif