﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;



[CustomEditor(typeof(ScriptableObjectExporter))]
public class ScriptableObjectExporterEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ScriptableObjectExporter myScript = (ScriptableObjectExporter)target;
        if (GUILayout.Button("Export Equipment Data"))
        {
            myScript.ExportEQ();
        }

        if (GUILayout.Button("Export Scriptable Object"))
        {
            myScript.ExportScriptableObject();
        }
    }
}

#endif