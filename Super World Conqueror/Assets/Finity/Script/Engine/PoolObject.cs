﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolObject : MonoBehaviour {
    public bool isAlive;

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Spawn()
    {
        isAlive = true;
        gameObject.SetActive(true);
    }

    public void Death()
    {
        isAlive = false;
        gameObject.SetActive(false);
    }
}
