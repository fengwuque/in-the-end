﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PoolerManager", menuName = "ScriptableObjects/Manager/PoolerManager")]
public class PoolerManagerScriptableObject : ScriptableObject
{
    [SerializeField] PoolerObjectCount[] listOfPoolObjCounts;
    [SerializeField] GameObject poolerPrefab;

    Dictionary<PoolObject, Pooler> poolerDic;
    Dictionary<PoolObject, int> objCountDic;

    Transform _poolersParent;

    public void Init(Transform parent)
    {
        _poolersParent = parent;
        objCountDic = new Dictionary<PoolObject, int>();
        poolerDic = new Dictionary<PoolObject, Pooler>();

        for (int i = 0; i < listOfPoolObjCounts.Length; ++i)
        {
            PoolerObjectCount poolerObjectCount = listOfPoolObjCounts[i];

            objCountDic.Add(poolerObjectCount.prefab, poolerObjectCount.count);
        }
    }

    public Pooler GetPooler(PoolObject prefab)
    {
        if(!poolerDic.ContainsKey(prefab))
        {
            AddPooler(prefab);
        }

        return poolerDic[prefab];
    }

    void AddPooler(PoolObject prefab)
    {
        GameObject newPooler = Instantiate(poolerPrefab);
        newPooler.transform.SetParent(_poolersParent);
        newPooler.transform.position = Vector3.zero;

        int count = objCountDic[prefab];

        newPooler.GetComponent<Pooler>().Init(count,prefab);

        poolerDic.Add(prefab,newPooler.GetComponent<Pooler>());
    }
}

[Serializable]
public class PoolerObjectCount
{
    public PoolObject prefab;
    public int count;
}