﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "EnumName", menuName = "PrimitiveScriptableObject/Enum")]
public class EnumScriptableObject : ScriptableObject
{
    
}