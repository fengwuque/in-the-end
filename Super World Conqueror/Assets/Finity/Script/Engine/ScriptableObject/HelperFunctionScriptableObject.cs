﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;

[CreateAssetMenu]
public class HelperFunctionScriptableObject : ScriptableObject
{
    private static HelperFunctionScriptableObject _instance;
    public static HelperFunctionScriptableObject Instance
    {
        get
        {
            if (!_instance)
                _instance = Resources.FindObjectsOfTypeAll<HelperFunctionScriptableObject>().FirstOrDefault();
#if UNITY_EDITOR
            if (!_instance)
                InitializeFromDefault(UnityEditor.AssetDatabase.LoadAssetAtPath<HelperFunctionScriptableObject>("Assets/HelperFunctionScriptableObject.asset"));
#endif
            return _instance;
        }
    }

    public static int StringToIntSeed(string str)
    {
        int seed = 0;
        for(int i = 0; i < str.Length; ++i)
        {
            seed += str[i];
        }

        return seed;
    }

    public static System.Random GetRandomNumberGeneratorFromString(string str)
    {
        int seed = StringToIntSeed(str);

        return new System.Random(seed);
    }

    private static void InitializeFromDefault(HelperFunctionScriptableObject HelperFunction)
    {
        if (_instance) DestroyImmediate(_instance);
        _instance = Instantiate(HelperFunction);
        _instance.hideFlags = HideFlags.HideAndDontSave;
    }
}