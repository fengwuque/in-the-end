﻿using GameSparks.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GSEventRequestManager : MonoBehaviour
{
    bool _isInitialized;
    public delegate void OnInitialized(PlayerData playerData);
    public OnInitialized onInitialized;

    [SerializeField] float retryInterval;
    [SerializeField] PlayerAccountManager PAM;
    [SerializeField] SceneTransitionManager STM;
    [SerializeField] SceneDataScriptableObject SDSO;

    private void Start()
    {
        Init();
    }

    void Init()
    {
        _isInitialized = false;
        if (!GS.Available)
        {
            Invoke("Init", retryInterval);
            return;
        }

        DeviceAuthenticationRequest();
    }

    public float GetRetryInterval()
    {
        return retryInterval;
    }

    public bool IsInitialized()
    {
        return _isInitialized;
    }

    void DeviceAuthenticationRequest()
    {
        new GameSparks.Api.Requests.DeviceAuthenticationRequest()
            .SetDisplayName("*New Player*")
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    if((bool)response.NewPlayer || response.DisplayName == "*New Player*")
                    {
                        CreateAccountManager CAM = FindObjectOfType<CreateAccountManager>();

                        CAM.InitNewPlayer();
                    }
                    else
                    {
                        GetPlayerData();
                    }
                }
                else
                {
                    Debug.Log("error authenticating device");
                }
            });
    }

    void GetPlayerData()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("GET_PLAYER_DATA")
            .Send((response) => {
                if (!response.HasErrors)
                {
                    GSData data = response.ScriptData.GetGSData("PLAYER_DATA");
                    PlayerData pd = new PlayerData();

                    pd.displayName = data.GetString("DISPLAY_NAME");
                    pd.gold = (int)data.GetInt("GOLD");
                    pd.dreamShard = (int)data.GetInt("DREAMSHARD");
                    pd.teleportalDesc = data.GetString("TELEPORTAL_DESC");
                    pd.teleportalPos = data.GetString("TELEPORTAL_POS");
                    pd.playerPos = data.GetString("PLAYER_POS");
                    pd.currentSeed = (int)data.GetInt("SEED");

                    Debug.Log("teleportal pos = " + pd.teleportalPos);
                    Debug.Log("player pos = " + pd.playerPos);
                    onInitialized(pd);
                    _isInitialized = true;

                    if(STM.GetCurrentScene().name == SDSO.createAccountScene.SceneName)
                    {
                        STM.ChangeScene(SDSO.mapScene);
                    }
                }
                else
                {
                    Debug.Log("Error Initializing new Player Data...");
                }
            });
    }

    public void InitNewPlayerData(PlayerData pd)
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("INIT_PLAYER_DATA")
            .SetEventAttribute("DISPLAY_NAME", pd.displayName)
            .SetEventAttribute("TELEPORTAL_POS",pd.teleportalPos)
            .SetEventAttribute("TELEPORTAL_DESC", pd.teleportalDesc)
            .Send((response) => {
            if (!response.HasErrors)
            {
                onInitialized(pd);
                    _isInitialized = true;
                    STM.ChangeScene(SDSO.mapScene);
            }
            else
            {
                Debug.Log("Error Initializing new Player Data...");
            }
        });
    }

	public void SetPlayerPos(string pos)
    {
        Debug.Log("pos = " + pos);
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("SET_PLAYER_POS").SetEventAttribute("PLAYER_POS", pos).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("Player Saved To GameSparks...");
            }
            else
            {
                Debug.Log("Error Saving Player Position...");
            }
        });
    }

    public void UseTeleportal()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("SET_PLAYER_POS").SetEventAttribute("PLAYER_POS", PAM.GetTeleportalPos()).Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("Used Teleportal...");
                PAM.UseTeleportalSuccess();
                STM.ChangeScene(SDSO.mapScene);
            }
            else
            {
                Debug.Log("Error Used Teleportaling...");
            }
        });
    }
}
