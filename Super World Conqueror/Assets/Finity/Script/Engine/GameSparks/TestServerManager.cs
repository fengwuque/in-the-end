﻿using GameSparks.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestServerManager : MonoBehaviour {
    [SerializeField] float retryInterval;
    // Use this for initialization
    void Start() {
        DeviceAuthenticationRequest();

    }

    // Update is called once per frame
    void Update() {

    }

    void DeviceAuthenticationRequest()
    {
        if(!GS.Available)
        {
            Invoke("DeviceAuthenticationRequest", retryInterval);
            return;
        }
            


        new GameSparks.Api.Requests.DeviceAuthenticationRequest()
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("test");
                    TestRequest();
                }
                else
                {
                    Debug.Log("error authenticating device");
                }
            });
    }

    void TestRequest()
    {
        new GameSparks.Api.Requests.LogEventRequest().SetEventKey("WORSHIP_SHRINE")
            .SetEventAttribute("SHRINE_ID",Random.Range(0,3))
            .Send((response) => {
                if (!response.HasErrors)
                {
                    string data = response.ScriptData.GetInt("GOLD").ToString();
                    Debug.Log(data);
                }
                else
                {
                    Debug.Log("Error Test Request");
                    Debug.Log(response.Errors);
                }
            });
    }
}
