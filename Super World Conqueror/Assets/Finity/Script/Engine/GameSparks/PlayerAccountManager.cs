﻿using GameSparks.Core;
using Mapbox.Geocoding;
using Mapbox.Unity.Location;
using Mapbox.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAccountManager : MonoBehaviour
{
    #region variables
    [SerializeField] float retryInterval;
    [SerializeField] GSEventRequestManager GERM;

    PlayerData _playerData;

    #endregion
    #region Initialization
    private void Awake()
    {
        GERM.onInitialized += OnInitialized;
        
    }

    #endregion

    void OnInitialized(PlayerData pd)
    {
        Debug.Log("Initialized..");
        _playerData = pd;

        //LeanTween.value(1, 0, 1).setOnUpdate((float v) => { bootupCanvas.alpha = v; }).setOnComplete(() => { bootupCanvas.gameObject.SetActive(false); }).setEaseOutExpo();

        //if(managers == null)
        //{
        //    managers = GameObject.FindGameObjectWithTag("Managers");
        //}

        //managers.SetActive(true);
    }

    #region PlayerData
    public string GetPlayerPos()
    {
        return _playerData.playerPos;
    }

    public string GetTeleportalPos()
    {
        return _playerData.teleportalPos;
    }

    public PlayerData GetPlayerData()
    {
        return _playerData;
    }

    public void UseTeleportalSuccess()
    {
        _playerData.playerPos = _playerData.teleportalPos;
    }

    public int GetSpawnSeed()
    {
        return _playerData.currentSeed;
    }

    public int GetPlayerLevel()
    {
        return 50;
    }

    public void IncreaseGold(int increment)
    {
        _playerData.gold += increment;
    }

    public void IncreaseDreamshard(int increment)
    {
        _playerData.dreamShard += increment;
    }
    #endregion

}

[Serializable]
public class PlayerData
{
    public string displayName;
    public int gold;
    public int dreamShard;
    public string playerPos;
    public string teleportalPos;
    public string teleportalDesc;
    public int currentSeed;

    public PlayerData()
    {
        displayName = "NEW_PLAYERXXXXXXX";
        gold = -9999;
        dreamShard = -9999;
        playerPos = "ERROR";
        teleportalPos = "ERROR";
        teleportalDesc = "ERROR";
    }

    public PlayerData(PlayerData clone)
    {
        displayName = clone.displayName;
        gold = clone.gold;
        dreamShard = clone.dreamShard;
        playerPos = clone.playerPos;
        teleportalPos = clone.teleportalPos;
        teleportalDesc = clone.teleportalDesc;
        currentSeed = clone.currentSeed;
    }
}
