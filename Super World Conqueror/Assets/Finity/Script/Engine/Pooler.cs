﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pooler: MonoBehaviour{
    PoolObject _mainObj;
    
    List<PoolObject> poolObjects;
    // Use this for initialization
    public void Init(int poolObjCount, PoolObject obj) {
        
        _mainObj = obj;

        poolObjects = new List<PoolObject>();

        for (int i = 0; i < poolObjCount; ++i)
        {
            GameObject newObj = Instantiate(_mainObj.gameObject);
            newObj.transform.SetParent(transform);
            newObj.GetComponent<PoolObject>().Death();
            poolObjects.Add(newObj.GetComponent<PoolObject>());
        }
    }
	
	public GameObject GetPoolObject()
    {
        for(int i = 0; i < poolObjects.Count; ++i)
        {
            if(!poolObjects[i].isAlive)
            {
                poolObjects[i].Spawn();
                return poolObjects[i].gameObject;
            }
        }

        Debug.Log("We Require More Pool Object: " + _mainObj.name);
        GameObject newObj = Instantiate(_mainObj.gameObject);
        newObj.transform.SetParent(transform);
        newObj.transform.localScale = Vector3.one;
        newObj.GetComponent<PoolObject>().Death();
        poolObjects.Add(newObj.GetComponent<PoolObject>());

        return newObj;

    }
}
