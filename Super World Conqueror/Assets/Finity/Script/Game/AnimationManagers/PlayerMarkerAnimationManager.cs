﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMarkerAnimationManager : MonoBehaviour {
    public GameObject markerHead;
    public GameObject marker;

    float initialHeadY;
    [Header("Hover")]
    public float hoverHeight;
    public float hoverSpeed;
    bool isMoving;
    [Header("Move")]
    public float jumpHeight;
    public float jumpDuration;
    public LeanTweenType tweenTypeUp;
    public LeanTweenType tweenTypeDown;
    float fromYPos;
    float toYPos;

    [SerializeField] Animator animator;
    [SerializeField] MapCameraAnimationManager mapCameraManager;
    [SerializeField] GameObject zoomOutParent;
    [SerializeField] Camera cam;
    [SerializeField] DesignVariablesScriptableObject DVSO;
    [SerializeField] float durationToAnimateOutFaster;
    private void Awake()
    {
        mapCameraManager.zoomDele += Zoom;
    }

    // Use this for initialization
    void Start () {
        initialHeadY = markerHead.transform.localPosition.y;
        fromYPos = marker.transform.position.y;
        toYPos = fromYPos + jumpHeight;

    }
	
	// Update is called once per frame
	void Update () {
        UpdateHover();

    }

    public void Zoom(bool isZoomOut)
    {
        if (isZoomOut)
        {
            LeanTween.delayedCall(gameObject, DVSO.generalOutDuration * durationToAnimateOutFaster, () =>
            {
                zoomOutParent.transform.position = cam.WorldToScreenPoint(marker.transform.position);
                animator.SetTrigger("ZoomOut");
            });
        }
        else
        {
            animator.SetTrigger("ZoomIn");
        }
    }

    public void Move()
    {
        if(!isMoving)
        {
            isMoving = true;
            TweenMove();
        }

    }

    public void StoppedMoving()
    {
        isMoving = false;
    }

    void TweenMove()
    {
        if (!isMoving)
            return;

        LeanTween.moveY(marker, toYPos, jumpDuration).setEase(tweenTypeUp).setOnComplete(() => {
            LeanTween.moveY(marker, fromYPos, jumpDuration).setEase(tweenTypeDown).setOnComplete(TweenMove);
            });
    }

    void UpdateHover()
    {
        markerHead.transform.localPosition = new Vector3(0, initialHeadY + hoverHeight * Mathf.Sin(hoverSpeed * Time.time));
    }
}
