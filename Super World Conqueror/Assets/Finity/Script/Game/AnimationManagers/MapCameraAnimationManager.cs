﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCameraAnimationManager : MonoBehaviour {
    [SerializeField] Transform mapCameraZoomOutTransform;
    [SerializeField] Transform mapCameraZoomInTransform;
    [SerializeField] GameObject cam;
    [SerializeField] DesignVariablesScriptableObject DVSO;
    [SerializeField] MovementController MC;
    [SerializeField] ControlPanelAnimationManager CPAM;
    [SerializeField] Transform playerT;
    public delegate void ZoomDele(bool isZoomOut);
    public ZoomDele zoomDele;

    public bool isZoomOut;
	// Use this for initialization
	void Start () {
        isZoomOut = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public DesignVariablesScriptableObject GetDVSO()
    {
        return DVSO;
    }

    public void ToggleCameraZoom()
    {
        isZoomOut = !isZoomOut;
        Transform toTransform = isZoomOut ? mapCameraZoomOutTransform : mapCameraZoomInTransform;

        if(isZoomOut)
        {
            toTransform.localEulerAngles = new Vector3(toTransform.localEulerAngles.x, -playerT.eulerAngles.y, toTransform.localEulerAngles.z);
        }

        float duration = isZoomOut ? DVSO.generalOutDuration : DVSO.generalInDuration;

        LeanTween.cancel(cam);
        LeanTween.move(cam, toTransform, duration).setEase(DVSO.generalType);
        LeanTween.rotate(cam, toTransform.rotation.eulerAngles, duration).setEase(DVSO.generalType);
        LeanTween.scale(cam, toTransform.localScale, duration).setEase(DVSO.generalType);

        if(zoomDele != null)
        {
            zoomDele(isZoomOut);
        }

        if(isZoomOut)
        {
            MC.ZoomOut();
            CPAM.ZoomOut();
        }
        else
        {
            MC.ZoomIn();
            CPAM.ZoomIn();
        }
    }
}
