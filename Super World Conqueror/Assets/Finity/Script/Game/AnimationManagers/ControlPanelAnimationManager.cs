﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlPanelAnimationManager : MonoBehaviour
{
    [SerializeField] GameObject[] controlPanelObjs;
    [SerializeField] DesignVariablesScriptableObject DVSO;
    [SerializeField] CanvasGroup cg;
    [SerializeField] float inBetweenPercent;
    [SerializeField] Vector3 zoomOutScale;
    [SerializeField] Vector3 zoomInScale;

    bool isZoomOut;

    float animationDurationIn;
    float animationDurationOut;

    private void Awake()
    {
        animationDurationIn = (float)DVSO.generalInDuration / ((float)controlPanelObjs.Length)/ inBetweenPercent;
        animationDurationOut = (float)DVSO.generalOutDuration / ((float)controlPanelObjs.Length) / inBetweenPercent;
    }

    public void ZoomOut()
    {
        controlPanelObjs[0].gameObject.SetActive(true);
        cg.interactable = true;
        isZoomOut = true;
        //ZoomOut(0);
        ZoomOutAnim(0);
        //ZoomOutAnim(controlPanelObjs.Length - 1);
    }

    void ZoomOutAnim(int i)
    {
        if (i < controlPanelObjs.Length && isZoomOut)
        {
            GameObject obj = controlPanelObjs[i].gameObject;
            LeanTween.cancel(obj);
            obj.GetComponent<Animator>().SetTrigger("Appear");
            LeanTween.delayedCall(obj, inBetweenPercent * animationDurationOut, () => { ZoomOutAnim(i + 1); });

        }

        //if (i >= 0 && isZoomOut)
        //{
        //    GameObject obj = controlPanelObjs[i].gameObject;
        //    LeanTween.cancel(obj);

        //    obj.GetComponent<Animator>().SetTrigger("Appear");
        //    Debug.Log(i);
        //    Debug.Log(inBetweenPercent * animationDurationIn);

        //    LeanTween.delayedCall(obj, inBetweenPercent * animationDurationIn, () => { ZoomOutAnim(i - 1); });
        //}
    }

    //void ZoomOut(int i)
    //{

    //    if (i < controlPanelObjs.Length && isZoomOut)
    //    {
    //        GameObject obj = controlPanelObjs[i].gameObject;
    //        LeanTween.cancel(obj);

    //        Image image = obj.GetComponent<Image>();

    //        LeanTween.value(obj, image.color.a, 1, animationDurationOut).setOnUpdate((float a) =>
    //         {
    //             Color c = image.color;
    //             c.a = a;
    //             image.color = c;
    //         }).setEase(DVSO.generalType);

    //        LeanTween.delayedCall(obj, inBetweenPercent * animationDurationOut, () => { ZoomOut(i + 1); });
    //        LeanTween.scale(obj, zoomOutScale, animationDurationOut).setEase(DVSO.generalType);
    //    }
    //}

    public void ZoomIn()
    {
        cg.interactable = false;
        isZoomOut = false;
        //ZoomIn(controlPanelObjs.Length - 1);
        ZoomInAnim(controlPanelObjs.Length - 1);
    }
    void ZoomInAnim(int i)
    {
        if (i >= 0 && !isZoomOut)
        {
            GameObject obj = controlPanelObjs[i].gameObject;
            LeanTween.cancel(obj);

            obj.GetComponent<Animator>().SetTrigger("Gone");

            if (i == 0)
            {
                LeanTween.delayedCall(obj, animationDurationIn, () => { controlPanelObjs[0].gameObject.SetActive(false); });
            }
            else
            {
                LeanTween.delayedCall(obj, inBetweenPercent * animationDurationIn, () => { ZoomInAnim(i - 1); });
            }
        }
    }
    //void ZoomIn(int i)
    //{
    //    if (i >= 0 && !isZoomOut)
    //    {
    //        GameObject obj = controlPanelObjs[i].gameObject;
    //        LeanTween.cancel(obj);

    //        Image image = obj.GetComponent<Image>();

    //        LeanTween.value(obj, image.color.a, 0, animationDurationIn).setOnUpdate((float a) =>
    //        {
    //            Color c = image.color;
    //            c.a = a;
    //            image.color = c;
    //            Debug.Log(a);
    //        }).setEase(DVSO.generalType);

    //        LeanTween.scale(obj,zoomInScale, animationDurationIn).setEase(DVSO.generalType);

    //        if(i == 0)
    //        {
    //            LeanTween.delayedCall(obj, inBetweenPercent*2 * animationDurationIn, () => { controlPanelObjs[0].gameObject.SetActive(false); });
    //        }
    //        else
    //        {
    //            LeanTween.delayedCall(obj, inBetweenPercent * animationDurationIn, () => { ZoomIn(i - 1); });
    //        }
    //    }
    //}
}
