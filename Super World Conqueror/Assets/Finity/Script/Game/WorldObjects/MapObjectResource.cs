﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MapObjectResource : MapObject
{
    public Animator anim;

    [SerializeField] ResourceGainedDataScriptableObject RGD;
    [SerializeField] SpawnTypeScriptableObject gold;
    [SerializeField] SpawnTypeScriptableObject dreamshard;
    [SerializeField] ResourceGainedFeedbackManagerScriptableObject RGFM;

    bool isTapped = false;
    const string TAKEN_STATENAME = "GoldAnimTaken";

    public override void Init(string _id, int _spawnTime, int aliveTime)
    {
        base.Init(_id, _spawnTime, aliveTime);

        isTapped = false;
    }

    public void IsTapped()
    {
        if (isTapped) return;

        PlayerAccountManager PAM = GameObject.FindGameObjectWithTag("GameSparksManager").GetComponent<PlayerAccountManager>();

        if (PAM)
        {
            //canvasParent.position = mainCamera.WorldToScreenPoint(gameObject.transform.position);
            if (type == gold)
            {
                int goldGained = RGD.GetGoldGained(myID, PAM.GetPlayerLevel());
                PAM.IncreaseGold(goldGained);
                RGFM.SpawnGoldFeedback(goldGained, gameObject.transform.position);
            }
            else if (type == dreamshard)
            {
                int dreamshardGained = RGD.GetDreamShardGained(myID, PAM.GetPlayerLevel());
                RGFM.SpawnDreamshardFeedback(dreamshardGained, gameObject.transform.position);
                //text.text = RGD.GetDreamShardGained(myID, PAM.GetPlayerLevel()).ToString();
            }
            isTapped = true;
            //Debug.Log("Canvas Pos = " + canvasParent.position);
        }
        
        anim.SetTrigger("IsTaken");
    }
}
