﻿
using Mapbox.Unity.Map;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.Utilities;
using Mapbox.Utils;
using UnityEngine.UI;
using Mapbox.Unity.MeshGeneration.Interfaces;
using Mapbox.Map;
using TMPro;

public class Dominion : MonoBehaviour, IFeaturePropertySettable
{
    AbstractMap myMap;
    Vector2d myID;
    Transform player;

    [SerializeField] Text dominionNameText;

    [SerializeField] float squareDistanceToAppear;
    [SerializeField] Color activatedColor;
    [SerializeField] Color deactivatedColor;
    [SerializeField] Image[] imagesThatNeedsToChangeColor;
    [SerializeField] GameObject zoomOutParent;
    [SerializeField] GameObject zoomInParent;
    [SerializeField] Animator animator;
    [SerializeField] float durationToAnimateOutFaster;
    [SerializeField] Canvas canvas;
    [SerializeField] TextMeshProUGUI dominionNameText2;
    bool isNearPlayer;

    MapCameraAnimationManager camManager;
    Camera myCam;
    DesignVariablesScriptableObject DVSO;

    [SerializeField] float updateInterval;

    void Awake()
    {
        myMap = GameObject.FindGameObjectWithTag("Map").GetComponent<AbstractMap>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        myCam = player.GetComponentInChildren<Camera>();
        camManager = player.gameObject.GetComponent<MapCameraAnimationManager>();
        camManager.zoomDele += Zoom;

        DVSO = camManager.GetDVSO();
    }

	// Use this for initialization
	void Start () {
        

        myID = myMap.WorldToGeoPosition(transform.position);
        dominionNameText.gameObject.SetActive(false);
        isNearPlayer = false;

        InvokeRepeating("UpdatePlayerPostion", Random.Range(0, updateInterval), updateInterval);

        //camManager.zoomDele += Zoom;
    }

    void Zoom(bool isZoomOut)
    {
        if(isZoomOut)
        {
            LeanTween.delayedCall(gameObject, DVSO.generalOutDuration * durationToAnimateOutFaster, () =>
            {
                Vector3 screenPos = myCam.WorldToScreenPoint(zoomInParent.transform.position);
                zoomOutParent.transform.position = screenPos;
                canvas.sortingOrder = -(int)Mathf.Abs(screenPos.y);
                animator.SetTrigger("ZoomOut");
            });
        }
        else
        {
            animator.SetTrigger("ZoomIn");
        }
    }

    void OnEnable()
    {
    }

    void OnDisable()
    {
        //LeanTween.cancel(zoomObjectParent);
        //camManager.zoomDele -= Zoom;
        //myMap.TileProvider.OnTileAdded -= SelfDestruct;
    }

    void SelfDestruct(UnwrappedTileId tile)
    {
        gameObject.SetActive(false);
    }

    void Zoom()
    {
        //camManager.Zoom(zoomObjectParent, zoomOutTransform, zoomInTransform);

        if(camManager.isZoomOut)
        {
            dominionNameText.gameObject.SetActive(false);
            isNearPlayer = false;
            foreach (Image i in imagesThatNeedsToChangeColor)
            {
                i.color = deactivatedColor;
            }
        }
        else
        {
            dominionNameText.gameObject.SetActive(true);
            isNearPlayer = true;
            foreach (Image i in imagesThatNeedsToChangeColor)
            {
                i.color = activatedColor;
            }
        }
    }

    public void Set(Dictionary<string, object> props)
    {
        dominionNameText.text = "";

        if (props.ContainsKey("name"))
        {
            dominionNameText2.text = dominionNameText.text = props["name"].ToString();
        }
        else if (props.ContainsKey("house_num"))
        {
            dominionNameText2.text = dominionNameText.text = props["house_num"].ToString();
        }
        else if (props.ContainsKey("type"))
        {
            dominionNameText2.text = dominionNameText.text = props["type"].ToString();
        }
}

// Update is called once per frame
void Update () {
        //UpdatePlayerPostion();
    }

    void UpdatePlayerPostion()
    {
        float dist = Vector3.SqrMagnitude(transform.position - player.position);
        //dominionNameText.gameObject.transform.parent.LookAt(player);
        if (dist < squareDistanceToAppear && !isNearPlayer)
        {
            dominionNameText.gameObject.SetActive(true);
            
            isNearPlayer = true;
            foreach(Image i in imagesThatNeedsToChangeColor)
            {
                i.color = activatedColor;
            }
            
        }
        else if (dist > squareDistanceToAppear)
        {
            dominionNameText.gameObject.SetActive(false);
            isNearPlayer = false;
            foreach (Image i in imagesThatNeedsToChangeColor)
            {
                i.color = deactivatedColor;
            }
        }
    }

    void OnDestroy()
    {
        //camManager.zoomDele -= Zoom;
        //myMap.TileProvider.OnTileAdded -= SelfDestruct;
    }

}
