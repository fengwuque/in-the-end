﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObject : PoolObject
{
    public string myID;
    int spawnTime;
    int noOfTicksLeft;

    public SpawnTypeScriptableObject type;
    protected Camera mainCamera;

    public virtual void Init(string _id, int _spawnTime,int aliveTime)
    {
        myID = _id;
        spawnTime = _spawnTime;
        mainCamera = Camera.main;
        int currentT = DateTime.Now.Minute + DateTime.Now.Hour * 60;

        noOfTicksLeft = spawnTime + aliveTime - currentT;

        InvokeRepeating("UpdateDeath", 60, 60);
    }

    void UpdateDeath()
    {
        --noOfTicksLeft;

        if (noOfTicksLeft <= 0)
            GetComponent<PoolObject>().Death();

    }
}
