﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResourceFeedback : PoolObject {

    [SerializeField] TextMeshProUGUI text;

    public void Init(Vector3 pos, int resourceGained)
    {
        transform.position = pos;
        text.text = resourceGained.ToString();
    }
}
