﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargingWithImpactFeedback : SpellFeedback
{
    public ParticleSystem impactParticle;
    public ParticleSystem chargingParticle;
    public Vector3 chargingOffset;
    public bool isAutoDeath;
    public override void ActivateFeedback(Vector3 fromTarget, Vector3 toTarget)
    {
        impactParticle.transform.position = toTarget;
        chargingParticle.transform.position = fromTarget + chargingOffset;
        if(isAutoDeath)
        {
            Invoke("Death", spellDuration);
        }
    }
}