﻿using UnityEngine;
using System.Collections;
using TMPro;

public class DamageFeedback : PoolObject
{
    [SerializeField] TextMeshProUGUI text;
    public void Init(Vector3 pos, int damage)
    {
        transform.position = pos;
        text.text = damage.ToString();
    }
}