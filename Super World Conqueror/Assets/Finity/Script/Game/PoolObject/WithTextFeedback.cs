﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WithTextFeedback : ChargingWithImpactFeedback
{
    [SerializeField] Animator anim;
    [SerializeField] TextMeshProUGUI text;
    [SerializeField] Color color;
    [SerializeField] string buffName;
    [SerializeField] bool isBuff;
    [SerializeField] Transform textParent;

    const string buffString = "TriggerBuff";
    const string debuffString = "TriggerDebuff";
    public override void ActivateFeedback(Vector3 fromTarget, Vector3 toTarget)
    {
        base.ActivateFeedback(fromTarget, toTarget);

        anim.SetTrigger(isBuff ? buffString : debuffString);
        text.color = color;
        text.text = buffName;
        textParent.position = Camera.main.WorldToScreenPoint(toTarget);
    }
}