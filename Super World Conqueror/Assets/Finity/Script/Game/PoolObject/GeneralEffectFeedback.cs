﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralEffectFeedback : EffectFeedback {
    [SerializeField] ParticleSystem generalEffect;
    [SerializeField] Vector3 offset;

    public override void ActivateFeedback(Vector3 toTarget)
    {
        generalEffect.transform.position = toTarget+ offset;
    }
}
