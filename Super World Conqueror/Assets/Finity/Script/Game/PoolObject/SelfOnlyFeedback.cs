﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfOnlyFeedback : SpellFeedback
{
    public ParticleSystem selfParticle;
    public Vector3 offset;
    public override void ActivateFeedback(Vector3 fromTarget, Vector3 toTarget)
    {
        selfParticle.transform.position = fromTarget + offset;
        Invoke("Death", spellDuration);
    }
}