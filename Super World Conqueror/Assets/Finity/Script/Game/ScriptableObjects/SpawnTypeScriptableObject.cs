﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "SpawnType", menuName = "ScriptableObjects/SpawningData/SpawnType")]
public class SpawnTypeScriptableObject : ScriptableObject
{
    public float weight;
}