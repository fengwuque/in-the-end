﻿using UnityEngine;
using System.Collections;
using System;

[CreateAssetMenu(fileName = "Expression", menuName = "ScriptableObjects/BattleFormula/Expression")]
[Serializable]
public class ExpressionScriptableObject : OperandScriptableObject
{
    public OperandScriptableObject lhs;
    public OperatorScriptableObject op;
    public OperandScriptableObject rhs;
}