﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DataExporterScriptableObject),true)]
public class DataExporterEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        DataExporterScriptableObject myScript = (DataExporterScriptableObject)target;
        if (GUILayout.Button("Export"))
        {
            myScript.Export();
        }
    }
}
