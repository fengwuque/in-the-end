﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;



[CustomEditor(typeof(FormulaTrackerScriptableObject))]
public class FormulaTrackerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        FormulaTrackerScriptableObject myScript = (FormulaTrackerScriptableObject)target;
        if (GUILayout.Button("Track"))
        {
            myScript.Track();
        }
    }
}

#endif