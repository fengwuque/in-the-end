﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "DerivedStat", menuName = "ScriptableObjects/Battle/DerivedStat")]
public class DerivedStatsScriptableObject : ScriptableObject
{
    public AssignmentScriptableObject HP;
    public AssignmentScriptableObject Defence;
    public AssignmentScriptableObject SpellEffectiveness;

    public Dictionary<AssignmentScriptableObject, float> CreateDerivedStats()
    {
        Dictionary<AssignmentScriptableObject, float> derivedStats = new Dictionary<AssignmentScriptableObject, float>
        {
            { HP, 0 },
            { Defence, 0 },
            { SpellEffectiveness, 0 }
        };

        return derivedStats;
    }
}