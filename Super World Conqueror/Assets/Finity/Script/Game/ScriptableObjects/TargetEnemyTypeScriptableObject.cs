﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "TargetEnemyType", menuName = "ScriptableObjects/Battle/TargetEnemy")]
public class TargetEnemyTypeScriptableObject : TargetTypeScriptableObject
{
    
}