﻿using UnityEngine;
using System.Collections;
using RoboRyanTron.Unite2017.Variables;
using System.Collections.Generic;
using System;

[CreateAssetMenu(fileName = "OrbPrefabsManager", menuName = "ScriptableObjects/Battle/OrbPrefabsManager")]
public class OrbPrefabsManagerScriptableObject : ScriptableObject
{
    [SerializeField] OrbPrefab[] listOfOrbPrefabs;

    Dictionary<EnumScriptableObject, GameObject> _orbPrefabs;

    public GameObject RandomOrb(EnumScriptableObject[] equippedOrb)
    {
        EnumScriptableObject orbType = equippedOrb[UnityEngine.Random.Range(0, equippedOrb.Length)];
        GameObject newOrb = InstantiateOrbPrefab(orbType);
        BattleOrbController BOC = newOrb.GetComponent<BattleOrbController>();
        BOC.orbType = orbType;

        return newOrb;
    }

    public GameObject InstantiateOrbPrefab(EnumScriptableObject orbType)
    {
        if(_orbPrefabs == null)
        {
            InitOrbPrefabs();
        }

        GameObject newOrb = Instantiate(_orbPrefabs[orbType]) as GameObject;
        newOrb.transform.localScale = new Vector3(1, 1, 1);
        return newOrb;
    }

    void InitOrbPrefabs()
    {
        _orbPrefabs = new Dictionary<EnumScriptableObject, GameObject>();

        for(int i = 0; i < listOfOrbPrefabs.Length; ++i)
        {
            _orbPrefabs.Add(listOfOrbPrefabs[i].orbType, listOfOrbPrefabs[i].prefab);
        }
    }
}

[Serializable]
public class OrbPrefab
{
    public EnumScriptableObject orbType;
    public GameObject prefab;
}