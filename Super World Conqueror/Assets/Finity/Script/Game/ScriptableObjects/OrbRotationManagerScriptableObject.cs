﻿using UnityEngine;
using System.Collections;
using RoboRyanTron.Unite2017.Variables;
using System.Collections.Generic;
using RoboRyanTron.Unite2017.Events;

[CreateAssetMenu(fileName = "OrbRotationManager", menuName = "ScriptableObjects/Battle/OrbRotationManager")]
public class OrbRotationManagerScriptableObject : ScriptableObject
{
    Transform[] _orbsPos;

    List<GameObject> currentOrbs;

    EnumScriptableObject[] _equippedOrb;
    Transform _orbParent;

    [SerializeField] OrbPrefabsManagerScriptableObject OPM;
    [SerializeField] float rotateSpeed;
    [SerializeField] LeanTweenType rotateType;
    [SerializeField] LeanTweenType rotateTypeFirst;
    [SerializeField] float rotateSpeedFirst;
    [SerializeField] BattleStatsManagerScriptableObject BSM;
    [SerializeField] ReferenceOperandScriptableObject unableToAttack;
    [SerializeField] GameEvent playerEndTurn;
    [SerializeField] float unableToAttackFeedbackDuration;

    [SerializeField] GameEvent OnStartRotating;
    [SerializeField] GameEvent OnEndRotating;

    bool isNewRotation;

    public void InitOrb(EnumScriptableObject[] orbs, Transform parent, Transform[] orbsPos)
    {
        _equippedOrb = orbs;
        _orbParent = parent;
        _orbsPos = orbsPos;
        currentOrbs = new List<GameObject>();

        for(int i = 0; i < orbsPos.Length; ++i)
        {
            currentOrbs.Add(null);
        }
        isNewRotation = true;
        RefreshOrbs();
    }

    void Rotate()
    {
        float duration = 0;
        LeanTweenType tweenType = LeanTweenType.notUsed;
        if (isNewRotation)
        {
            tweenType = rotateTypeFirst;
            duration = rotateSpeedFirst;
        }
        else
        {
            tweenType = rotateType;
            duration = rotateSpeed;
        }

        for (int i = 0 ; i < _orbsPos.Length; ++i)
        {
            
            if (currentOrbs[i] == null)
            {
                GameObject newOrb = OPM.RandomOrb(_equippedOrb);
                newOrb.transform.SetParent(_orbParent);
                newOrb.transform.position = _orbsPos[_orbsPos.Length - 1].position;

                for (int j = i + 1; j < _orbsPos.Length; ++j)
                {
                    currentOrbs[j - 1] = currentOrbs[j];
                }

                currentOrbs[_orbsPos.Length - 1] = newOrb;

                for (int j = i; j < _orbsPos.Length; ++j)
                {
                    if (currentOrbs[j] != null)
                    {
                        if (j == _orbsPos.Length - 1)
                        {
                            LeanTween.move(currentOrbs[j].gameObject, _orbsPos[j], duration).setEase(tweenType).setOnComplete(() => { Rotate(); });
                        }
                        else
                        {
                            LeanTween.move(currentOrbs[j].gameObject, _orbsPos[j], duration).setEase(tweenType);
                        }
                    }
                }
                return;
            }
        }

        SetOrbActivity();

        isNewRotation = false;
    }

    public void RefreshOrbs()
    {

        if (BSM.player.mainReference[unableToAttack] > 0)
        {
            LeanTween.delayedCall(unableToAttackFeedbackDuration, () => { playerEndTurn.Raise(); });
            Debug.Log("Player unable to attack, end turn in " + unableToAttackFeedbackDuration.ToString());
            Debug.Log(BSM.player.mainReference[unableToAttack]);
        }
        else
        {
            OnStartRotating.Raise();
            Rotate();
        }
    }

    void RotateOnce()
    {
        if(isNewRotation)
        {
            OnStartRotating.Raise();
            for (int i = 0; i < _orbsPos.Length; ++i)
            {
                if (currentOrbs[i] == null || currentOrbs[i].GetComponent<BattleOrbController>().isSelected)
                {
                    GameObject newOrb = OPM.RandomOrb(_equippedOrb);
                    newOrb.transform.SetParent(_orbParent);
                    newOrb.transform.position = _orbsPos[_orbsPos.Length - 1].position;

                    for (int j = i + 1; j < _orbsPos.Length; ++j)
                    {
                        currentOrbs[j - 1] = currentOrbs[j];
                    }

                    currentOrbs[_orbsPos.Length - 1] = newOrb;

                    for (int j = i; j < _orbsPos.Length; ++j)
                    {
                        if (currentOrbs[j] != null)
                        {
                            if (j == _orbsPos.Length - 1)
                            {
                                LeanTween.move(currentOrbs[j].gameObject, _orbsPos[j], rotateSpeed).setEase(rotateType).setOnComplete(() => {
                                    isNewRotation = false;
                                    RotateOnce();
                                });
                            }
                            else
                            {
                                LeanTween.move(currentOrbs[j].gameObject, _orbsPos[j], rotateSpeed).setEase(rotateType);
                            }
                        }
                    }
                    return;
                }
            }

            isNewRotation = false;
            RotateOnce();
        }
        else
        {
            for (int i = 1; i < _orbsPos.Length; ++i)
            {
                if (currentOrbs[i] == null || currentOrbs[i].GetComponent<BattleOrbController>().isSelected)
                {
                    GameObject newOrb = OPM.RandomOrb(_equippedOrb);
                    newOrb.transform.SetParent(_orbParent);
                    newOrb.transform.position = _orbsPos[_orbsPos.Length - 1].position;

                    for (int j = i + 1; j < _orbsPos.Length; ++j)
                    {
                        currentOrbs[j - 1] = currentOrbs[j];
                    }

                    currentOrbs[_orbsPos.Length - 1] = newOrb;

                    for (int j = i; j < _orbsPos.Length; ++j)
                    {
                        LeanTween.move(currentOrbs[j].gameObject, _orbsPos[j], rotateSpeed).setEase(rotateType).setOnComplete(() => { SetOrbActivity(); });
                    }
                    return;
                }
            }

            SetOrbActivity();
        }
        
    }

    public void Refresh1Orb()
    {
        isNewRotation = true;
        RotateOnce();
    }

    void SetOrbActivity()
    {
        for (int i = 0; i < currentOrbs.Count; ++i)
        {
            if (i == 1 || i == 3 || i == 5)
            {
                currentOrbs[i].GetComponent<BattleOrbController>().SetActive(true);
            }
            else if (i == 0)
            {
                if (currentOrbs[i] != null)
                {
                    currentOrbs[i].GetComponent<BattleOrbController>().SetActive(true);
                    currentOrbs[i].GetComponent<BattleOrbController>().Break();
                }
            }
            else
            {
                currentOrbs[i].GetComponent<BattleOrbController>().SetActive(false);
            }
        }

        LeanTween.delayedCall(rotateSpeed, () => { OnEndRotating.Raise(); });
        
    }

    public void RandomOrb(Transform[] referencePos,EnumScriptableObject[] equippedOrb,Transform orbParent)
    {
        for(int i = 0; i < referencePos.Length; ++i)
        {
            EnumScriptableObject orbType = equippedOrb[Random.Range(0, equippedOrb.Length)];
            GameObject newOrb = OPM.InstantiateOrbPrefab(orbType);
            BattleOrbController BOC = newOrb.GetComponent<BattleOrbController>();
            BOC.orbType = orbType;
            newOrb.transform.SetParent(orbParent);
            newOrb.transform.position = referencePos[i].position;

            if(i == 0 || i == 2 || i == 4)
            {
                BOC.SetActive(true);
            }
        }
    }
}
