﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DamageFeedbackManager", menuName = "ScriptableObjects/Manager/DamageFeedbackManager")]
public class DamageFeedbackManagerScriptableObject : ScriptableObject
{
    [SerializeField] PoolerManagerScriptableObject PM;
    [SerializeField] DamageFeedback healFeedback;
    [SerializeField] DamageFeedback damageFeedback;
    Dictionary<DamageFeedback, Pooler> _feedbackPoolers;
    Dictionary<Dictionary<ReferenceOperandScriptableObject, float>, Transform> _statParents;
    Dictionary<Dictionary<ReferenceOperandScriptableObject, float>, float> feedbackConcatenation;

    [SerializeField] Vector3 posMin;
    [SerializeField] Vector3 posMax;
    [SerializeField] float delay;

    List<Dictionary<ReferenceOperandScriptableObject, float>> concatList;
    Camera cam;

    public void InitFeedbackManager(Transform uiParent, Dictionary<ReferenceOperandScriptableObject, float>[] stats, Transform[] parents)
    {
        cam = Camera.main;
        PM.Init(uiParent);
        _feedbackPoolers = new Dictionary<DamageFeedback, Pooler>();
        _feedbackPoolers.Add(healFeedback, PM.GetPooler(healFeedback));
        _feedbackPoolers.Add(damageFeedback, PM.GetPooler(damageFeedback));

        _statParents = new Dictionary<Dictionary<ReferenceOperandScriptableObject, float>, Transform>();

        for(int i = 0; i < parents.Length; ++i)
        {
            _statParents.Add(stats[i], parents[i]);
        }

        feedbackConcatenation = new Dictionary<Dictionary<ReferenceOperandScriptableObject, float>, float>();
        concatList = new List<Dictionary<ReferenceOperandScriptableObject, float>>();
    }

    public void UpdateConcatenation()
    {
        for(int i = concatList.Count-1; i >= 0; --i)
        {
            feedbackConcatenation[concatList[i]] -= Time.fixedDeltaTime;

            if (feedbackConcatenation[concatList[i]] <= 0)
            {
                feedbackConcatenation.Remove(concatList[i]);
                concatList.Remove(concatList[i]);
            }
        }
    }

    public void SpawnDamage(Dictionary<ReferenceOperandScriptableObject, float> stat, int damage)
    {
        if(feedbackConcatenation.ContainsKey(stat))
        {
            LeanTween.delayedCall(feedbackConcatenation[stat], () => {
                GameObject dmgObj = _feedbackPoolers[damageFeedback].GetPoolObject();
                
                dmgObj.GetComponent<DamageFeedback>().Init(cam.WorldToScreenPoint(_statParents[stat].position) + RandomOffset(), damage);
            });
            feedbackConcatenation[stat] += delay;
        }
        else
        {
            _feedbackPoolers[damageFeedback].GetPoolObject().GetComponent<DamageFeedback>().Init(cam.WorldToScreenPoint(_statParents[stat].position) + RandomOffset(), damage);
            feedbackConcatenation.Add(stat, delay);
            concatList.Add(stat);
        }
    }

    public void SpawnHeal(Dictionary<ReferenceOperandScriptableObject, float> stat, int healAmt)
    {
        if (feedbackConcatenation.ContainsKey(stat))
        {
            LeanTween.delayedCall(feedbackConcatenation[stat], () => {
                _feedbackPoolers[healFeedback].GetPoolObject().GetComponent<DamageFeedback>().Init(cam.WorldToScreenPoint(_statParents[stat].position) + RandomOffset(), healAmt);
            });
            feedbackConcatenation[stat] += delay;
        }
        else
        {
            _feedbackPoolers[healFeedback].GetPoolObject().GetComponent<DamageFeedback>().Init(cam.WorldToScreenPoint(_statParents[stat].position) + RandomOffset(), healAmt);
            feedbackConcatenation.Add(stat, delay);
            concatList.Add(stat);
        }
    }

    Vector3 RandomOffset()
    {
        return new Vector3(Random.Range(posMin.x, posMax.x), Random.Range(posMin.y, posMax.y), Random.Range(posMin.z, posMax.z));
    }

    
}
