﻿using UnityEngine;
using System.Collections;
using RoboRyanTron.SceneReference;

[CreateAssetMenu(fileName = "SceneData", menuName = "ScriptableObjects/SceneData", order = 1)]
public class SceneDataScriptableObject : ScriptableObject
{
    public SceneReference createAccountScene;
    public SceneReference mapScene;
}