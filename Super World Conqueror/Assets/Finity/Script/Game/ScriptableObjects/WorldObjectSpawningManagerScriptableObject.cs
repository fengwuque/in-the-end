﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Mapbox.Map;
using Mapbox.Unity.Map;

[CreateAssetMenu(fileName = "WorldObjectSpawningManager", menuName = "ScriptableObjects/Manager/WorldObjectSpawningManager", order = 1)]
public class WorldObjectSpawningManagerScriptableObject : ScriptableObject
{
    [SerializeField] PoolerManagerScriptableObject PM;
    [SerializeField] WorldObject[] objects;
    Dictionary<SpawnTypeScriptableObject, Pooler> _spawnTypePoolers;
    Dictionary<UnwrappedTileId, SpawnerManager> _spawnerManagers;

    public void InitSpawningManager(Transform parent)
    {
        PM.Init(parent);
        _spawnTypePoolers = new Dictionary<SpawnTypeScriptableObject, Pooler>();
        for(int i = 0; i < objects.Length; ++i)
        {
            _spawnTypePoolers.Add(objects[i].type, PM.GetPooler(objects[i].prefab));
        }

        _spawnerManagers = new Dictionary<UnwrappedTileId, SpawnerManager>();
    }

    public void AddSpawner(SpawnerScriptableObject spawnerType,Transform parent, UnwrappedTileId myID, int seed)
    {
        if (_spawnerManagers.ContainsKey(myID)) return;

        _spawnerManagers.Add(myID, new SpawnerManager(spawnerType, parent,myID));
        _spawnerManagers[myID].Init(_spawnTypePoolers,seed);
    }

    public void UpdateSpawner()
    {
        int currentT = DateTime.Now.Minute + DateTime.Now.Hour * 60;
        foreach (KeyValuePair<UnwrappedTileId, SpawnerManager> spawner in _spawnerManagers)
        {
            spawner.Value.UpdateSpawns(currentT);
        }
    }

    public void RemoveSpawner(UnwrappedTileId parent)
    {
        _spawnerManagers[parent].RemoveSpawner();
        _spawnerManagers.Remove(parent);
    }
}

[Serializable]
public class WorldObject
{
    public SpawnTypeScriptableObject type;
    public MapObject prefab;
}

public class SpawnerManager
{
    SpawnerScriptableObject _spawner;
    Transform _parent;
    List<GameObject> listOfSpawnedGO;
    Dictionary<SpawnTypeScriptableObject, Pooler> _spawnTypePoolers;
    Dictionary<string, MapSpawns> spawnedObjs;
    UnwrappedTileId _tileID;
    string _id;
    AbstractMap abstractMap;

    List<MapSpawns> listOfMapSpawns;
    const float NO_OF_MIN_PER_DAY = 1440;

    public SpawnerManager(SpawnerScriptableObject spawner,
    Transform parent, UnwrappedTileId tileID)
    {
        _spawner = spawner;
        _parent = parent;
        _tileID = tileID;
        _id = tileID.ToString();

    }

    public void Init(Dictionary<SpawnTypeScriptableObject, Pooler> spawnTypePoolers, int randomSeed)
    {
        _spawnTypePoolers = spawnTypePoolers;
        int newSeed = randomSeed * _tileID.X % _tileID.Y * _tileID.Z;
        listOfMapSpawns = new List<MapSpawns>();
        spawnedObjs = new Dictionary<string, MapSpawns>();
        listOfSpawnedGO = new List<GameObject>();
        System.Random random = new System.Random(newSeed);
        
        for (int i = 0; i < _spawner.spawns.Length; ++i)
        {
            SpawnCategoryScriptableObject spawnCat = _spawner.spawns[i];

            spawnCat.Init();
            RangedFloat rangedFloat;
            rangedFloat.minValue = spawnCat.minSpawnInterval;
            rangedFloat.maxValue = spawnCat.maxSpawnInterval;

            int t = 0;
            while (t < NO_OF_MIN_PER_DAY - spawnCat.maxSpawnInterval)
            {
                t += random.Next(spawnCat.minSpawnInterval, spawnCat.maxSpawnInterval);

                SpawnTypeScriptableObject randomSpawn = spawnCat.GetRandomSpawn((float)random.NextDouble());

                string ID = DateTime.Today.ToString("d") + "|" + _id + "|" + spawnCat.name + "|" + t.ToString() + "|" + randomSpawn.name;
                
                MapSpawns newSpawn = new MapSpawns(ID,randomSpawn,t,spawnCat.aliveTime, rangedFloat);
                listOfMapSpawns.Add(newSpawn);
            }
        }

        UpdateSpawns(DateTime.Now.Minute + DateTime.Now.Hour * 60);
    }

    public void UpdateSpawns(int currentT)
    {
        for (int i = 0; i < listOfMapSpawns.Count; ++i)
        {
            MapSpawns mapSpawns = listOfMapSpawns[i];
            if (mapSpawns.spawnTime > currentT)
                return;

            if (mapSpawns.spawnTime + mapSpawns.aliveTime > currentT && !spawnedObjs.ContainsKey(mapSpawns.myID))
            {
                Spawn(mapSpawns.myType, mapSpawns.myID, mapSpawns.aliveTime, mapSpawns.spawnTime, mapSpawns.spawnDistance);
            }
        }
    }

    public void RemoveSpawner()
    {
        for (int i = 0; i < listOfSpawnedGO.Count; ++i)
        {
            listOfSpawnedGO[i].GetComponent<PoolObject>().Death();
        }
        listOfSpawnedGO.Clear();
    }

    void Spawn(SpawnTypeScriptableObject obj, string id, int aliveTime, int spawnTime, RangedFloat spawnDistance)
    {
        GameObject newGO = _spawnTypePoolers[obj].GetPoolObject();
        newGO.GetComponent<MapObject>().Init(id, spawnTime, aliveTime);
        newGO.transform.position = _parent.position + (new Vector3(UnityEngine.Random.Range(-1, 1), 0, UnityEngine.Random.Range(-1, 1))).normalized * UnityEngine.Random.Range(spawnDistance.minValue, spawnDistance.maxValue);
        listOfSpawnedGO.Add(newGO);
    }
}

public class MapSpawns
{
    public string myID;
    public SpawnTypeScriptableObject myType;
    public int spawnTime;
    public int aliveTime;
    public RangedFloat spawnDistance;

    public MapSpawns(string id, SpawnTypeScriptableObject type, int time, int alive, RangedFloat distance)
    {
        myID = id;
        myType = type;
        spawnTime = time;
        aliveTime = alive;
        spawnDistance = distance;
    }
}