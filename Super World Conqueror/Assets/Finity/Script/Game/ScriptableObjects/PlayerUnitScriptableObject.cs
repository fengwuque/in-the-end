﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

[CreateAssetMenu(fileName = "PlayerUnit", menuName = "ScriptableObjects/Battle/UnitPlayer")]
public class PlayerUnitScriptableObject : UnitScriptableObject
{
    public BaseAttribute[] equipmentContributions;

    public override SpellDataScriptableObject GetSpell()
    {
        return null;
    }

    public override void Init()
    {
        attributes = GetTotalAttributes();
        Dictionary<AssignmentScriptableObject, float> derivedAttributes = dsso.CreateDerivedStats();

        for (int i = 0; i < derivedAttributes.Count; ++i)
        {
            KeyValuePair<AssignmentScriptableObject, float> derivedAttribute = derivedAttributes.ElementAt(i);

            float attributeValue = calculator.ResolveExpression(derivedAttribute.Key, attributes);
            if (derivedAttribute.Key == dsso.HP)
            {
                attributes.Add(unitCurrentHPOperand, attributeValue);
                attributes.Add(unitMaxHPOperand, attributeValue);
            }
            else if(derivedAttribute.Key == dsso.Defence)
            {
                attributes.Add(unitDefenceOperand, attributeValue);
            }
            else if(derivedAttribute.Key == dsso.SpellEffectiveness)
            {
                attributes.Add(unitSpellEffectivenessOperand, attributeValue);
            }
        }

        BSD.InitStatuses(attributes);
    }

    Dictionary<ReferenceOperandScriptableObject, float> GetTotalAttributes()
    {
        Dictionary<ReferenceOperandScriptableObject, float> totalAttributes = new Dictionary<ReferenceOperandScriptableObject, float>();

        for (int i = 0; i < baseAttribute.Length; ++i)
        {
            if (totalAttributes.ContainsKey(baseAttribute[i].attributeType))
            {
                totalAttributes[baseAttribute[i].attributeType] += baseAttribute[i].attributeValue;
            }
            else
            {
                totalAttributes.Add(baseAttribute[i].attributeType, baseAttribute[i].attributeValue);
            }
        }

        for (int i = 0; i < equipmentContributions.Length; ++i)
        {
            if (totalAttributes.ContainsKey(equipmentContributions[i].attributeType))
            {
                totalAttributes[equipmentContributions[i].attributeType] += equipmentContributions[i].attributeValue;
            }
            else
            {
                totalAttributes.Add(equipmentContributions[i].attributeType, equipmentContributions[i].attributeValue);
            }
        }

        return totalAttributes;
    }
}