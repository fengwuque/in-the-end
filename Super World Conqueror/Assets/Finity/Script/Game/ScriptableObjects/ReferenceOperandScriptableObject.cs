﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "ReferenceOperand", menuName = "ScriptableObjects/BattleFormula/ReferenceOperand")]
public class ReferenceOperandScriptableObject : OperandScriptableObject
{
    [Tooltip("There may be a more efficient way of reference later (maybe)")]
    public string referenceName;
}