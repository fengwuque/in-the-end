﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "AssignStatsManager", menuName = "ScriptableObjects/Battle/AssignStatsManager")]
public class AssignStatsManagerScriptableObject : ScriptableObject
{
    public BattleStatusDataScriptableObject BSD;
    public ReferenceOperandScriptableObject currentHP;
    public ReferenceOperandScriptableObject maxHP;
    [SerializeField] BattleUIManagerScriptableObject BUM;
    [SerializeField] DamageFeedbackManagerScriptableObject DFM;

    public bool showAssignmentCalculations;

    public void AssignTarget(TargetAssignmentScriptableObject assignment, ComplexReferences complexReferences, float value, Dictionary<ReferenceOperandScriptableObject, float> referenceValues)
    {
        if (assignment.toAssign != null)
        {
            if (assignment.toAssign == currentHP)
            {
                if (showAssignmentCalculations) Debug.Log("Before " + assignment.toAssign + " = " + referenceValues[assignment.toAssign]);
                float difference = referenceValues[assignment.toAssign] - value;

                if (difference > 0)//kanna damage
                {
                    if (referenceValues[BSD.reflectDamagePercent] > 0)
                    {
                        int damage = Mathf.RoundToInt(difference * referenceValues[BSD.reflectDamagePercent]);
                        complexReferences.fromTargetReferenceValues[currentHP] -= damage;
                        DFM.SpawnDamage(complexReferences.fromTargetReferenceValues, damage);
                        BUM.UpdateUI(complexReferences.fromTargetReferenceValues);
                        if (showAssignmentCalculations) Debug.Log("After " + assignment + " = " + complexReferences.fromTargetReferenceValues[currentHP]);
                    }

                    int result = Mathf.RoundToInt(value);
                    int damageToTarget = Mathf.RoundToInt(referenceValues[assignment.toAssign]) - result;

                    result = result < 0 ? 0 : result;
                    referenceValues[assignment.toAssign] = result;
                    DFM.SpawnDamage(referenceValues, damageToTarget);
                    BUM.UpdateUI(referenceValues);
                }
                else // kanna heal
                {
                    if (referenceValues[BSD.reflectHealingPercent] > 0)
                    {
                        int damage = Mathf.RoundToInt(difference * referenceValues[BSD.reflectHealingPercent]);
                        int result = Mathf.RoundToInt(complexReferences.toTargetReferenceValues[currentHP] + damage);
                        result = result < 0 ? 0 : result;
                        DFM.SpawnDamage(complexReferences.toTargetReferenceValues, damage);
                        complexReferences.toTargetReferenceValues[currentHP] = result;
                        BUM.UpdateUI(complexReferences.toTargetReferenceValues);
                    }

                    if (referenceValues[BSD.preventHealingBool] < 1)
                    {
                        int healAmount = Mathf.RoundToInt(-difference);
                        value = value > referenceValues[maxHP] ? referenceValues[maxHP] : value;
                        referenceValues[assignment.toAssign] = value;
                        DFM.SpawnHeal(referenceValues, healAmount);
                        BUM.UpdateUI(referenceValues);
                    }
                }
                if (showAssignmentCalculations) Debug.Log("After " + assignment.toAssign + " = " + referenceValues[assignment.toAssign]);

                
            }
            else
            {
                if (showAssignmentCalculations) Debug.Log("Before " + assignment.toAssign + " = " + referenceValues[assignment.toAssign]);
                referenceValues[assignment.toAssign] = value;
                if (showAssignmentCalculations) Debug.Log("After " + assignment.toAssign + " = " + referenceValues[assignment.toAssign]);
            }

        }
    }
}