﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "BattleTurnManager", menuName = "ScriptableObjects/BattleFormula/Operator")]
public class BattleTurnManagerScriptableObject : ScriptableObject
{
    Animator _turnAnimator;
    [SerializeField]
    public void Init(Animator turnAnimator)
    {

    }

    public void OnPlayerEndTurn()
    {

    }


}