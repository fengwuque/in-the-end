﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "BattleStatusData", menuName = "ScriptableObjects/Battle/BattleStatusData")]
public class BattleStatusDataScriptableObject : ScriptableObject
{
    public ReferenceOperandScriptableObject unableToAttackBool;
    public ReferenceOperandScriptableObject reflectDamagePercent;
    public ReferenceOperandScriptableObject reflectHealingPercent;
    public ReferenceOperandScriptableObject preventHealingBool;

    public void InitStatuses(Dictionary<ReferenceOperandScriptableObject,float> attributes)
    {
        attributes.Add(unableToAttackBool, 0);
        attributes.Add(reflectDamagePercent, 0);
        attributes.Add(reflectHealingPercent, 0);
        attributes.Add(preventHealingBool, 0);
    }
}