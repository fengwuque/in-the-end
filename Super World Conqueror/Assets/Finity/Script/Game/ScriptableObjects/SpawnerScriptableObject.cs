﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Spawner", menuName = "ScriptableObjects/SpawningData/Spawner")]
public class SpawnerScriptableObject : ScriptableObject
{
    public SpawnCategoryScriptableObject[] spawns;

    [Tooltip("In Unity Distance")]
    [Range(50, 500)]
    public float minSpawnDistance;
    [Range(100, 1000)]
    public float maxSpawnDistance;
}