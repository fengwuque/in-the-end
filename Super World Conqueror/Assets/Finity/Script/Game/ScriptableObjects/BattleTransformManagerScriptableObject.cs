﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "BattleTransformManager", menuName = "ScriptableObjects/Battle/BattleTransformManager")]
public class BattleTransformManagerScriptableObject : ScriptableObject
{
    public Dictionary<Dictionary<ReferenceOperandScriptableObject, float>, Transform> statsTransform;

    public void Init(Dictionary<ReferenceOperandScriptableObject, float>[] stats, Transform[] transforms)
    {
        statsTransform = new Dictionary<Dictionary<ReferenceOperandScriptableObject, float>, Transform>();

        for(int i =0; i < stats.Length; ++i)
        {
            statsTransform.Add(stats[i], transforms[i]);
        }
    }
}