﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/DesignVariables", order = 1)]
public class DesignVariablesScriptableObject : ScriptableObject
{
    [Header("Tweening")]
    public LeanTweenType generalType;
    public float generalInDuration;
    public float generalOutDuration;
}