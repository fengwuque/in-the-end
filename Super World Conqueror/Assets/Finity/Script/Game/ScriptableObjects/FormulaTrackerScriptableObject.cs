﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FormulaTracker", menuName = "ScriptableObjects/BattleFormula/FormulaTracker")]
public class FormulaTrackerScriptableObject : ScriptableObject
{
    [SerializeField] ExpressionScriptableObject expression;
    [SerializeField] CalculatorScriptableObject calc;

    public void Track()
    {
        Debug.Log(DebugExpression(expression));
    }

    public string DebugExpression(OperandScriptableObject operand)
    {
        if (operand.GetType() == typeof(OperandScriptableObject) || operand.GetType() == typeof(ReferenceOperandScriptableObject))
        {
            return operand.name;
        }
        else if (operand.GetType() == typeof(TargetAssignmentScriptableObject))
        {
            TargetAssignmentScriptableObject assignment = (TargetAssignmentScriptableObject)operand;
            if (assignment.toAssign)
                return "[Assigning" + assignment.toAssign.name + "to " + DebugExpression(assignment.formula) + "]";
            else
                return DebugExpression(assignment.formula);
        }
        else if (operand.GetType() == typeof(ConstantOperandScriptableObject))
        {
            return ((ConstantOperandScriptableObject)operand).value.ToString();
        }
        else if (operand.GetType() == typeof(ExpressionScriptableObject))
        {
            ExpressionScriptableObject expression = (ExpressionScriptableObject)operand;

            if (expression.op == calc.Add)
            {
                return "(" + DebugExpression(expression.lhs) + " + " + DebugExpression(expression.rhs) +")";
            }
            else if (expression.op == calc.Multiply)
            {
                return "(" + DebugExpression(expression.lhs) + " * " + DebugExpression(expression.rhs) + ")";
            }
            else if (expression.op == calc.Subtract)
            {
                return "(" + DebugExpression(expression.lhs) + " - " + DebugExpression(expression.rhs) + ")";
            }
            else if (expression.op == calc.Divide)
            {
                return "(" + DebugExpression(expression.lhs) + " / " + DebugExpression(expression.rhs) + ")";
            }
            else if (expression.op == calc.Power)
            {
                return "(" + DebugExpression(expression.lhs) + " Power " + DebugExpression(expression.rhs) + ")";
            }
            else if (expression.op == calc.Min)
            {
                return "(" + DebugExpression(expression.lhs) + " Min " + DebugExpression(expression.rhs) + ")";
            }
            else if (expression.op == calc.Max)
            {
                return "(" + DebugExpression(expression.lhs) + " Max " + DebugExpression(expression.rhs) + ")";
            }
            else
            {
                return "UNDEFINED OPERATOR!!!!";
            }
        }
        else if (operand.GetType() == typeof(AssignmentScriptableObject))
        {
            return DebugExpression(((AssignmentScriptableObject)operand).formula);
        }
        
        return "This is some undefined shit that I don't know";
    }
}
