﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

[CreateAssetMenu(fileName = "OrbMonsterUnit", menuName = "ScriptableObjects/Battle/UnitOrbMonster")]
public class OrbMonsterUnitScriptableObject : UnitScriptableObject
{
    public BaseAttribute[] attributeGrowth;
    [Tooltip("only the unique orb's level is counted")]
    public BaseAttribute[] orbsData;
    public EnumScriptableObject[] orbsWeight;
    [SerializeField] SpellDataManagerScriptableObject SDM;

    public override SpellDataScriptableObject GetSpell()
    {
        List<EnumScriptableObject> newList = new List<EnumScriptableObject>();

        for(int i = 0; i < 3; ++i)
        {
            newList.Add(orbsWeight[UnityEngine.Random.Range(0, orbsWeight.Length)]);
        }

        return SDM.GetSpell(newList.ToArray());
    }

    public override void Init()
    {
        attributes = GetTotalAttributes();
        Dictionary<AssignmentScriptableObject, float> derivedAttributes = dsso.CreateDerivedStats();

        for (int i = 0; i < derivedAttributes.Count; ++i)
        {
            KeyValuePair<AssignmentScriptableObject, float> derivedAttribute = derivedAttributes.ElementAt(i);

            float attributeValue = calculator.ResolveExpression(derivedAttribute.Key, attributes);
            if (derivedAttribute.Key == dsso.HP)
            {
                attributes.Add(unitCurrentHPOperand, attributeValue);
                attributes.Add(unitMaxHPOperand, attributeValue);
            }
            else if (derivedAttribute.Key == dsso.Defence)
            {
                attributes.Add(unitDefenceOperand, attributeValue);
            }
            else if (derivedAttribute.Key == dsso.SpellEffectiveness)
            {
                attributes.Add(unitSpellEffectivenessOperand, attributeValue);
            }
        }

        BSD.InitStatuses(attributes);
    }

    Dictionary<ReferenceOperandScriptableObject, float> GetTotalAttributes()
    {
        Dictionary<ReferenceOperandScriptableObject, float> totalAttributes = new Dictionary<ReferenceOperandScriptableObject, float>();

        for (int i = 0; i < baseAttribute.Length; ++i)
        {
            if (totalAttributes.ContainsKey(baseAttribute[i].attributeType))
            {
                totalAttributes[baseAttribute[i].attributeType] += baseAttribute[i].attributeValue;
            }
            else
            {
                totalAttributes.Add(baseAttribute[i].attributeType, baseAttribute[i].attributeValue);
            }
        }

        for (int i = 0; i < attributeGrowth.Length; ++i)
        {
            if (totalAttributes.ContainsKey(attributeGrowth[i].attributeType))
            {
                totalAttributes[attributeGrowth[i].attributeType] += attributeGrowth[i].attributeValue * totalAttributes[unitLevelOperand];
            }
            else
            {
                totalAttributes.Add(attributeGrowth[i].attributeType, attributeGrowth[i].attributeValue * totalAttributes[unitLevelOperand]);
            }
        }

        for(int i = 0; i< orbsData.Length; ++i)
        {
            totalAttributes.Add(orbsData[i].attributeType, orbsData[i].attributeValue);
        }

        return totalAttributes;
    }
}