﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/EquipmentData")]
public class EquipmentDataScriptableObject : ScriptableObject
{
    public List<EquipmentPartsStatTypes> rollableStatsInEquipmentParts;//Some unique equipments can have their own personal rollable stats. If not defined, then will use this.
    //public Dictionary<EQUIPMENT_PARTS, STAT_TYPE[]> rollableStatsInEquipmentParts;
    public List<EquipmentSet> equipmentSetsBlueprint;
    [Tooltip("3 equipment = index 0. total 5")]
    public List<float> equipmentSetBonuses;

    public EquipmentStatsData eqStats;

    [Header("Weapons Data")]
    public List<WeaponDamageRatio> dmgRatio;
    public List<WeaponData> weaponData;
    public List<WeaponAttackSpeed> aspdRatio;

    Dictionary<EQUIPMENT_SETS, RARITY> _EQSetsRarity;

    Dictionary<EQUIPMENT_SETS, float> _EQSetsBaseBonus;

    Dictionary<EQUIPMENT_SETS, WeaponData> _weaponData;

    Dictionary<WEAPON_TYPE, float> _aspdData;

    Dictionary<WEAPON_TYPE, float> _dmgRatioData;

    public Dictionary<EQUIPMENT_SETS,RARITY> GetEqSetsRarity()
    {
        if(_EQSetsRarity == null)
        {
            _EQSetsRarity = new Dictionary<EQUIPMENT_SETS, RARITY>();

            for(int i = 0; i < (int)EQUIPMENT_SETS.TOTAL_COUNT; ++i)
            {
                if(i < (int)EQUIPMENT_SETS.ARISTOCRATIC_V)
                {
                    _EQSetsRarity.Add((EQUIPMENT_SETS)i, (RARITY)(i / 6));
                }
                else
                {
                    _EQSetsRarity.Add((EQUIPMENT_SETS)i, RARITY.LEGENDARY);
                }
            }
        }

        return _EQSetsRarity;
    }

    float GetBonusBase(EQUIPMENT_SETS set)
    {
        if(_EQSetsBaseBonus == null)
        {
            _EQSetsBaseBonus = new Dictionary<EQUIPMENT_SETS, float>();

            for(int i = 0; i < equipmentSetsBlueprint.Count; ++i)
            {
                _EQSetsBaseBonus.Add(equipmentSetsBlueprint[i].setID, equipmentSetsBlueprint[i].baseBonusValue);
            }
        }

        return _EQSetsBaseBonus[set];
    }

    public List<Stat> GetEqSetBonus(Dictionary<EQUIPMENT_SETS,int> charEq)
    {
        List<Stat> equipmentBonuses = new List<Stat>();

        foreach (KeyValuePair<EQUIPMENT_SETS, int> setNo in charEq)
        {
            if (setNo.Value > 3)
            {
                int setBonusNumber = setNo.Value - 3;

                switch (setNo.Key)
                {
                    case EQUIPMENT_SETS.ADAMANT_I:
                    case EQUIPMENT_SETS.ADAMANT_II:
                    case EQUIPMENT_SETS.ADAMANT_III:
                    case EQUIPMENT_SETS.ADAMANT_IV:
                    case EQUIPMENT_SETS.ADAMANT_V:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.NORMAL_ARMOR, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        equipmentBonuses.Add(new Stat(STAT_TYPE.MAGIC_ARMOR, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                    case EQUIPMENT_SETS.ARISTOCRATIC_I:
                    case EQUIPMENT_SETS.ARISTOCRATIC_II:
                    case EQUIPMENT_SETS.ARISTOCRATIC_III:
                    case EQUIPMENT_SETS.ARISTOCRATIC_IV:
                    case EQUIPMENT_SETS.ARISTOCRATIC_V:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.NORMAL_ARMOR, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        equipmentBonuses.Add(new Stat(STAT_TYPE.MAGIC_ARMOR, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        equipmentBonuses.Add(new Stat(STAT_TYPE.HEALTH_POINT, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        equipmentBonuses.Add(new Stat(STAT_TYPE.MAGIC_DAMAGE, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        equipmentBonuses.Add(new Stat(STAT_TYPE.ATTACK_SPEED_INCREMENT, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                    case EQUIPMENT_SETS.COLD:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.COLD_DAMAGE_INCREMENT, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                    case EQUIPMENT_SETS.COOLDOWN_REDUCTION:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.COOLDOWN_REDUCTION, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                    case EQUIPMENT_SETS.CRIT_DAMAGE:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.CRITICAL_DAMAGE, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                    case EQUIPMENT_SETS.DAUNTLESS_I:
                    case EQUIPMENT_SETS.DAUNTLESS_II:
                    case EQUIPMENT_SETS.DAUNTLESS_III:
                    case EQUIPMENT_SETS.DAUNTLESS_IV:
                    case EQUIPMENT_SETS.DAUNTLESS_V:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.NORMAL_DAMAGE, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                    case EQUIPMENT_SETS.FIRE:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.FIRE_DAMAGE_INCREMENT, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                    case EQUIPMENT_SETS.HEALING:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.HEALING_BONUS, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                    case EQUIPMENT_SETS.HEROIC_I:
                    case EQUIPMENT_SETS.HEROIC_II:
                    case EQUIPMENT_SETS.HEROIC_III:
                    case EQUIPMENT_SETS.HEROIC_IV:
                    case EQUIPMENT_SETS.HEROIC_V:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.HEALTH_POINT, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                    case EQUIPMENT_SETS.LOKIS_MISCHIEF:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.DODGE_RATE, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                    case EQUIPMENT_SETS.MANIACAL_I:
                    case EQUIPMENT_SETS.MANIACAL_II:
                    case EQUIPMENT_SETS.MANIACAL_III:
                    case EQUIPMENT_SETS.MANIACAL_IV:
                    case EQUIPMENT_SETS.MANIACAL_V:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.ATTACK_SPEED_INCREMENT, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                    case EQUIPMENT_SETS.POISON:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.POISON_DAMAGE_INCREMENT, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                    case EQUIPMENT_SETS.REGEN_PER_SEC:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.REGEN_PER_SEC, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                    case EQUIPMENT_SETS.SAGACIOUS_I:
                    case EQUIPMENT_SETS.SAGACIOUS_II:
                    case EQUIPMENT_SETS.SAGACIOUS_III:
                    case EQUIPMENT_SETS.SAGACIOUS_IV:
                    case EQUIPMENT_SETS.SAGACIOUS_V:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.MAGIC_DAMAGE, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                    case EQUIPMENT_SETS.THORNS:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.THORNS_DAMAGE, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                    case EQUIPMENT_SETS.THORS_FURY:
                        equipmentBonuses.Add(new Stat(STAT_TYPE.LIGHTNING_DAMAGE_INCREMENT, GetBonusBase(setNo.Key) * equipmentSetBonuses[setBonusNumber]));
                        break;
                }
            }
        }

        return equipmentBonuses;
    }

    public bool GetIsMagicWeapon(EQUIPMENT_SETS set)
    {
        InitWeaponData();

        if (_weaponData[set].type == WEAPON_TYPE.AXE || _weaponData[set].type == WEAPON_TYPE.DAGGER || _weaponData[set].type == WEAPON_TYPE.GREATSWORD)
        {
            return false;
        }

        return true;
    }

    public float GetWeaponAttackSpeedBase(EQUIPMENT_SETS set)
    {
        InitWeaponData();

        if (_aspdData == null)
        {
            _aspdData = new Dictionary<WEAPON_TYPE, float>();

            for(int i = 0; i < aspdRatio.Count; ++i)
            {
                _aspdData.Add(aspdRatio[i].type, aspdRatio[i].attSpeed);
            }
        }

        return _aspdData[_weaponData[set].type];
    }

    void InitWeaponData()
    {
        if (_weaponData == null)
        {
            _weaponData = new Dictionary<EQUIPMENT_SETS, WeaponData>();

            for (int i = 0; i < weaponData.Count; ++i)
            {
                _weaponData.Add(weaponData[i].eqSet, weaponData[i]);
            }
        }
    }

    public float GetDamageRatio(EQUIPMENT_SETS set)
    {
        InitWeaponData();

        if (_dmgRatioData == null)
        {
            _dmgRatioData = new Dictionary<WEAPON_TYPE, float>();

            for(int i = 0; i < dmgRatio.Count; ++i)
            {
                _dmgRatioData.Add(dmgRatio[i].type, dmgRatio[i].ratio);
            }
        }

        return _dmgRatioData[_weaponData[set].type] * _weaponData[set].dmgMult;
    }
}

[Serializable] public class EquipmentPartsStatTypes
{
    public EQUIPMENT_PARTS partType;
    public List<STAT_TYPE> rollableStatType;
}

[Serializable] public enum EQUIPMENT_PARTS
{
    WEAPON = 0,
    HEAD,
    TOP,
    BOTTOM,
    HANDS,
    FEET,
    CAPE
}

[Serializable] public class EquipmentData
{
    public EQUIPMENT_SETS setID;
    public EQUIPMENT_PARTS partID;
    [Tooltip("If this is null, rollable stat type and stats will be standard")]
    public List<Stat> specialRollableStatMultiplier;
}

[Serializable] public enum EQUIPMENT_SETS
{
    DAUNTLESS_I, //Normal Damage %
    HEROIC_I, //hp%
    SAGACIOUS_I, //Magic damage %
    MANIACAL_I, // Attack Speed %
    ADAMANT_I, // magic and normal armor %
    ARISTOCRATIC_I, // Normal Damage, Magic Damage, HP, Attack Speed, Magic and Normal armor %
    DAUNTLESS_II, //Normal Damage %
    HEROIC_II, //hp%
    SAGACIOUS_II, //Magic damage %
    MANIACAL_II, // Attack Speed %
    ADAMANT_II, // magic and normal armor %
    ARISTOCRATIC_II, // Normal Damage, Magic Damage, HP, Attack Speed, Magic and Normal armor %
    DAUNTLESS_III, //Normal Damage %
    HEROIC_III, //hp%
    SAGACIOUS_III, //Magic damage %
    MANIACAL_III, // Attack Speed %
    ADAMANT_III, // magic and normal armor %
    ARISTOCRATIC_III, // Normal Damage, Magic Damage, HP, Attack Speed, Magic and Normal armor %
    DAUNTLESS_IV, //Normal Damage %
    HEROIC_IV, //hp%
    SAGACIOUS_IV, //Magic damage %
    MANIACAL_IV, // Attack Speed %
    ADAMANT_IV, // magic and normal armor %
    ARISTOCRATIC_IV, // Normal Damage, Magic Damage, HP, Attack Speed, Magic and Normal armor %
    DAUNTLESS_V, //Normal Damage %
    HEROIC_V, //hp%
    SAGACIOUS_V, //Magic damage %
    MANIACAL_V, // Attack Speed %
    ADAMANT_V, // magic and normal armor %
    ARISTOCRATIC_V, // Normal Damage, Magic Damage, HP, Attack Speed, Magic and Normal armor %
    THORS_FURY, // Lightning damage %, reduce opponent's cooldown reduction
    POISON, // Cold Damage %, passively slows down opponent's attack
    COLD,
    FIRE,
    CRIT_DAMAGE,
    LOKIS_MISCHIEF, // dodge rate % (does it do justice to loki? I personally think we can put alot of creativity into loki
    COOLDOWN_REDUCTION,
    THORNS,
    HEALING,
    REGEN_PER_SEC,
    TOTAL_COUNT
}

[Serializable] public enum RARITY
{
    COMMON = 0,
    UNCOMMON,
    RARE,
    EPIC,
    LEGENDARY
}

[Serializable] public class EquipmentSet
{
    public EQUIPMENT_SETS setID;
    public string setName;
    public string benefitsDescriptions;
    public float baseBonusValue;
    public List<EquipmentData> listOfEquipments;
}

[Serializable] public class EquipmentStatsData
{
    public float baseStatMult;
    public List<float> rarityMult;
    public float lvlMult;
}

[Serializable] public class WeaponData
{
    public EQUIPMENT_SETS eqSet;
    public WEAPON_TYPE type;
    public float dmgMult;
}

[Serializable] public enum WEAPON_TYPE
{
    DAGGER = 0,
    AXE,
    GREATSWORD,
    WAND,
    ORB,
    SCEPTER
}

[Serializable] public class WeaponDamageRatio
{
    public WEAPON_TYPE type;
    public float ratio=1;
}

[Serializable] public class WeaponAttackSpeed
{
    public WEAPON_TYPE type;
    public float attSpeed;
}

[Serializable] public class EquipSaveData
{
    public EQUIPMENT_SETS set;
    public EQUIPMENT_PARTS part;
    public List<Stat> stats;
    public int level;
}