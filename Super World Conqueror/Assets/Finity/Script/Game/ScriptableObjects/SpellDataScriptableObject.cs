﻿using UnityEngine;
using System.Collections;
using System;

[CreateAssetMenu(fileName = "SpellData", menuName = "ScriptableObjects/Battle/SpellData")]
public class SpellDataScriptableObject : ScriptableObject
{
    public SpellEffectScriptableObject[] listOfEffects;
    public EnumScriptableObject[] orbTypeRequired;
    public float spellAnimationDuration;
}

