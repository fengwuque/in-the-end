﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

[CreateAssetMenu(fileName = "WildMonsterUnit", menuName = "ScriptableObjects/Battle/UnitWildMonster")]
public class WildMonsterUnitScriptableObject : UnitScriptableObject
{
    public BaseAttribute[] attributeGrowth;
    public WeightedSpells[] spells;

    List<WeightedSpells> reWeightedSpells;

    public override void Init()
    {
        attributes = GetTotalAttributes();
        Dictionary<AssignmentScriptableObject, float> derivedAttributes = dsso.CreateDerivedStats();

        for (int i = 0; i < derivedAttributes.Count; ++i)
        {
            KeyValuePair<AssignmentScriptableObject, float> derivedAttribute = derivedAttributes.ElementAt(i);

            float attributeValue = calculator.ResolveExpression(derivedAttribute.Key, attributes);
            if (derivedAttribute.Key == dsso.HP)
            {
                attributes.Add(unitCurrentHPOperand, attributeValue);
                attributes.Add(unitMaxHPOperand, attributeValue);
            }
            else if (derivedAttribute.Key == dsso.Defence)
            {
                attributes.Add(unitDefenceOperand, attributeValue);
            }
            else if (derivedAttribute.Key == dsso.SpellEffectiveness)
            {
                attributes.Add(unitSpellEffectivenessOperand, attributeValue);
            }
        }

        BSD.InitStatuses(attributes);
        InitSpells();
    }

    void InitSpells()
    {
        reWeightedSpells = new List<WeightedSpells>();

        for(int i = 0; i < spells.Length;++i)
        {
            float prevWeight = 0;
            if(i > 0)
            {
                prevWeight = reWeightedSpells[i - 1].weight;
            }
            WeightedSpells reWeighted = new WeightedSpells();
            reWeighted.spell = spells[i].spell;
            reWeighted.weight = spells[i].weight + prevWeight;
            reWeightedSpells.Add(reWeighted);
        }
    }

    Dictionary<ReferenceOperandScriptableObject, float> GetTotalAttributes()
    {
        Dictionary<ReferenceOperandScriptableObject, float> totalAttributes = new Dictionary<ReferenceOperandScriptableObject, float>();

        for (int i = 0; i < baseAttribute.Length; ++i)
        {
            if (totalAttributes.ContainsKey(baseAttribute[i].attributeType))
            {
                totalAttributes[baseAttribute[i].attributeType] += baseAttribute[i].attributeValue;
            }
            else
            {
                totalAttributes.Add(baseAttribute[i].attributeType, baseAttribute[i].attributeValue);
            }
        }

        for (int i = 0; i < attributeGrowth.Length; ++i)
        {
            if (totalAttributes.ContainsKey(attributeGrowth[i].attributeType))
            {
                totalAttributes[attributeGrowth[i].attributeType] += attributeGrowth[i].attributeValue * totalAttributes[unitLevelOperand];
            }
            else
            {
                totalAttributes.Add(attributeGrowth[i].attributeType, attributeGrowth[i].attributeValue * totalAttributes[unitLevelOperand]);
            }
        }

        return totalAttributes;
    }

    public override SpellDataScriptableObject GetSpell()
    {
        float randomFloat = UnityEngine.Random.Range(0, reWeightedSpells[reWeightedSpells.Count - 1].weight);
        for (int i = 0; i < reWeightedSpells.Count; ++i)
        {
            if(reWeightedSpells[i].weight < randomFloat)
            {
                return reWeightedSpells[i].spell;
            }
        }

        return reWeightedSpells[0].spell;
    }
}

[Serializable]
public class WeightedSpells
{
    public SpellDataScriptableObject spell;
    public float weight;
}