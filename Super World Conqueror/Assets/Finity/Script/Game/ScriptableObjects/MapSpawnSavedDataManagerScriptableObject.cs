﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[CreateAssetMenu(fileName = "MapSpawnSavedDataManager", menuName = "ScriptableObjects/Manager/MapSpawnSavedDataManager", order = 1)]
public class MapSpawnSavedDataManagerScriptableObject : ScriptableObject
{
    [SerializeField] int noOfDaysSaved;
    [SerializeField] string fileLocation;
    Dictionary<string, MapSpawnSavedData> savedDataDictionary;
    List<MapSpawnSavedData> savedData;

    public void Init()
    {
        if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + fileLocation, FileMode.Open);
            savedData = (List<MapSpawnSavedData>)bf.Deserialize(file);
            file.Close();
        }

        savedDataDictionary = new Dictionary<string, MapSpawnSavedData>();
        bool needToSave = false;
        for (int i = savedData.Count-1; i >= 0; --i)
        {
            TimeSpan timeSpan = DateTime.Now - savedData[i].date;
            if (timeSpan.TotalDays > noOfDaysSaved)
            {
                savedData.RemoveAt(i);
                needToSave = true;
            }
            else
            {
                savedDataDictionary.Add(savedData[i].id, savedData[i]);
            }
        }

        if (needToSave)
            Save();
    }

    bool HasActivatedMapSpawn(string ID)
    {
        return savedDataDictionary.ContainsKey(ID);//if it doesn't contain key, its valid;
    }

    public void ActivateMapSpawn(string ID, DateTime date)
    {
        MapSpawnSavedData newSavedData = new MapSpawnSavedData();
        newSavedData.date = date;
        newSavedData.id = ID;
        savedDataDictionary.Add(ID, newSavedData);
        savedData.Add(newSavedData);
        Save();
    }

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + fileLocation);
        bf.Serialize(file, savedData);
        file.Close();
    }
}

[Serializable]
public class MapSpawnSavedData
{
    public DateTime date;
    public string id;
}