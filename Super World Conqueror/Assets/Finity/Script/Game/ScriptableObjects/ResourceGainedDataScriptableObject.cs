﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "ResourceGainedData", menuName = "ScriptableObjects/ResourceGainedData")]
public class ResourceGainedDataScriptableObject : ScriptableObject
{
    public float GoldBase;
    public float GoldMin;
    public float GoldMax;
    public float GoldLevelMult;
    public float DreamshardBase;
    public float DreamshardMin;
    public float DreamshardMax;
    public float DreamshardLevelMult;

    public int GetGoldGained(string id, int playerLevel)
    {
        System.Random RNG = HelperFunctionScriptableObject.GetRandomNumberGeneratorFromString(id);
        float randomFloat = (float)RNG.NextDouble();

        float min = GoldBase * GoldMin * playerLevel * GoldLevelMult;
        float max = GoldBase * GoldMax * playerLevel * GoldLevelMult;

        return (int)Mathf.Floor((min + (max - min) * randomFloat));
    }

    public int GetDreamShardGained(string id, int playerLevel)
    {
        System.Random RNG = HelperFunctionScriptableObject.GetRandomNumberGeneratorFromString(id);
        float randomFloat = (float)RNG.NextDouble();

        float min = DreamshardBase * DreamshardMin * playerLevel * DreamshardLevelMult;
        float max = DreamshardBase * DreamshardMax * playerLevel * DreamshardLevelMult;

        return (int)Mathf.Floor((min + (max - min) * randomFloat));
    }
}