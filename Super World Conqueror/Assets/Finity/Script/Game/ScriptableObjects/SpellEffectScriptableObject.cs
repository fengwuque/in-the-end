﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "SpellEffect", menuName = "ScriptableObjects/Battle/SpellEffect")]
public class SpellEffectScriptableObject : ScriptableObject
{
    public TargetAssignmentScriptableObject formula;
    public ReferenceOperandScriptableObject referenceType;
    public float referenceValue;
    public Reference[] otherReferences;
    public int numberOfTurns;
    public TargetTypeScriptableObject targetType;
}

[Serializable]
public class Reference
{
    public ReferenceOperandScriptableObject reference;
    public OperandScriptableObject value;
}