﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "BattleUIManager", menuName = "ScriptableObjects/Battle/BattleUIManager")]
public class BattleUIManagerScriptableObject : ScriptableObject
{
    Dictionary<Dictionary<ReferenceOperandScriptableObject, float>, BattleHPUI> unitsUI;

    [SerializeField] GameObject prefab;
    [SerializeField] ReferenceOperandScriptableObject maxHP;
    [SerializeField] ReferenceOperandScriptableObject currentHP;
    [SerializeField] ReferenceOperandScriptableObject myName;
    
    [SerializeField] Vector3 posOffset;

    [SerializeField] bool showUpdatedUI;
    Camera cam;
    Transform _uiParent;
    public void Init(Dictionary<ReferenceOperandScriptableObject, float>[] stats, Transform[] parents,Transform uiParent)
    {
        cam = Camera.main;
        _uiParent = uiParent;
        unitsUI = new Dictionary<Dictionary<ReferenceOperandScriptableObject, float>, BattleHPUI>();

        for(int i = 0; i < stats.Length; ++i)
        {
            AddUI(stats[i], parents[i]);
        }
    }

    public void AddUI(Dictionary<ReferenceOperandScriptableObject, float> stat, Transform parent)
    {
        if (unitsUI.ContainsKey(stat)) return;
        GameObject newUI = Instantiate(prefab) as GameObject;
        newUI.transform.SetParent(_uiParent);
        newUI.transform.position = cam.WorldToScreenPoint(parent.position) + posOffset;
        newUI.transform.localScale = Vector3.one;
        //newUI.transform.localPosition = posOffset;
        unitsUI.Add(stat, newUI.GetComponent<BattleHPUI>());

        //if(stat.ContainsKey(currentHP))
            UpdateUI(stat);
    }

    public void RemoveUI(Dictionary<ReferenceOperandScriptableObject, float> stat)
    {
        if (!unitsUI.ContainsKey(stat)) return;
        Destroy(unitsUI[stat].gameObject);
        unitsUI.Remove(stat);
    }

    public void UpdateUI(Dictionary<ReferenceOperandScriptableObject, float> stat)
    {
        if (!unitsUI.ContainsKey(stat)) return;
        if (showUpdatedUI)
        {
            Debug.Log(stat[myName] + " max hp = " + stat[maxHP]);
            Debug.Log(stat[myName] + "current hp = " + stat[currentHP]);
        }
        if (stat.ContainsKey(currentHP))
            unitsUI[stat].UpdateHP((int) stat[currentHP] , (int)stat[maxHP]);
        else
            unitsUI[stat].UpdateHP(0, 0);
    }
}
