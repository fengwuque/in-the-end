﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RoboRyanTron.Unite2017.Events;

[CreateAssetMenu(fileName = "CastSpellManager", menuName = "ScriptableObjects/Battle/CastSpellManager")]
public class CastSpellManagerScriptableObject : ScriptableObject
{
    [SerializeField] BattleStatsManagerScriptableObject BSM;
    [SerializeField] TargetManagerScriptableObject TM;
    [SerializeField] OrbSelectionManagerScriptableObject OSM;
    [SerializeField] SpellDataManagerScriptableObject SDM;
    [SerializeField] BattleUIManagerScriptableObject BUM;
    [SerializeField] GameEvent onPlayerEndTurn;
    [SerializeField] TargetTypeScriptableObject self;
    [SerializeField] TargetTypeScriptableObject enemySingle;
    [SerializeField] TargetTypeScriptableObject enemyAOE;
    [SerializeField] TargetTypeScriptableObject summonWall;
    [SerializeField] CalculatorScriptableObject calculator;
    [SerializeField] ReferenceOperandScriptableObject cannotAttackReference;
    Transform playerWallParent;
    Transform enemyWallParent;
    [SerializeField] ReferenceOperandScriptableObject currentHPRef;
    [SerializeField] ReferenceOperandScriptableObject maxHPRef;
    [SerializeField] ReferenceOperandScriptableObject defRef;
    [SerializeField] BattleStatusDataScriptableObject BSD;
    [SerializeField] SpellFeedbackManagerScriptableObject SFM;
#if UNITY_EDITOR

    [SerializeField] bool isAutoEndTurn;

#endif

    List<Dictionary<ReferenceOperandScriptableObject, float>> targetReferences;
    Dictionary<ReferenceOperandScriptableObject, float> aggressorReferences;
    List<PersistentSpellEffect> listOfPersistentEffectsPlayerSide;
    List<PersistentSpellEffect> listOfPersistentEffectsEnemySide;

    public void Init(Transform playerWallP, Transform enemyWallP)
    {
        listOfPersistentEffectsPlayerSide = new List<PersistentSpellEffect>();
        listOfPersistentEffectsEnemySide = new List<PersistentSpellEffect>();
        targetReferences = new List<Dictionary<ReferenceOperandScriptableObject, float>>();
        playerWallParent = playerWallP;
        enemyWallParent = enemyWallP;
    }
    

    public void PlayerCastSpell()
    {
        List<EnumScriptableObject> selectedOrb = OSM.selectedOrb;
        SpellDataScriptableObject spell = SDM.GetSpell(selectedOrb.ToArray());
        float animationDuration = spell.spellAnimationDuration;

        aggressorReferences = BSM.player.mainReference;

        bool playerTurnEnded = false;
        for (int i = 0; i < spell.listOfEffects.Length; ++i)
        {
            targetReferences.Clear();
            SpellEffectScriptableObject effect = spell.listOfEffects[i];

            if (effect.targetType == enemySingle)
            {
                if (BSM.enemy.wall.Count > 0)
                {
                    targetReferences.Add(BSM.enemy.wall);
                }
                else
                {
                    switch (TM.GetCurrentTarget())
                    {
                        case 0:
                            targetReferences.Add(BSM.enemy.mainReference);
                            break;
                        case 1:
                            targetReferences.Add(BSM.enemy.summon1Reference);
                            break;
                        case 2:
                            targetReferences.Add(BSM.enemy.summon2Reference);
                            break;
                    }
                }
            }
            else if (effect.targetType == enemyAOE)
            {
                if (BSM.enemy.wall.Count > 0)
                {
                    targetReferences.Add(BSM.enemy.wall);
                }
                else
                {
                    targetReferences.Add(BSM.enemy.mainReference);
                    targetReferences.Add(BSM.enemy.summon1Reference);
                    targetReferences.Add(BSM.enemy.summon2Reference);
                }
            }
            else if (effect.targetType == summonWall)
            {
                if (i == 0)
                {
                    InitializeWall(BSM.player.wall);
                }

                targetReferences.Add(BSM.player.wall);
            }
            else
            {
                targetReferences.Add(BSM.player.mainReference);
            }

            
            LeanTween.delayedCall(animationDuration, () => {
#if UNITY_EDITOR
                if (isAutoEndTurn && !playerTurnEnded)
                {
                    playerTurnEnded = true;
                    onPlayerEndTurn.Raise();
                }
                    

#else
            onPlayerEndTurn.Raise();
#endif
                for (int j = 0; j < targetReferences.Count; ++j)
                {
                    if (j < targetReferences.Count && targetReferences[j] == null)
                        continue;

                    Dictionary<ReferenceOperandScriptableObject, float> reference = targetReferences[j];
                    ComplexReferences newComplexReferences = new ComplexReferences(aggressorReferences, reference, effect.targetType == self);

                    newComplexReferences.AddReference(effect);

                    if (effect.numberOfTurns > 1)
                    {
                        float prevValue = newComplexReferences.toTargetReferenceValues[effect.formula.toAssign];

                        int noOfTurns = effect.formula.toAssign == cannotAttackReference ? effect.numberOfTurns + 1 : effect.numberOfTurns;
                        PoolObject effectFeedback = SFM.EffectFeedback(effect, targetReferences[j]);
                        PersistentSpellEffect persistentSpellEffect = new PersistentSpellEffect(noOfTurns, effect, newComplexReferences,effectFeedback);
                        persistentSpellEffect.uglyReference = prevValue - ActivateEffect(effect, newComplexReferences);

                        if (effect.targetType == summonWall)
                        {
                            playerWallParent.gameObject.SetActive(true);
                            persistentSpellEffect.uglyReference = 1;//player
                            BSM.player.wall[maxHPRef] = BSM.player.wall[currentHPRef];
                            BUM.AddUI(BSM.player.wall, playerWallParent);
                            BUM.UpdateUI(BSM.player.wall);
                        }

                        listOfPersistentEffectsPlayerSide.Add(persistentSpellEffect);
                    }
                    else
                    {
                        ActivateEffect(effect, newComplexReferences);
                    }

                    newComplexReferences.RemoveReference(effect);

                    BUM.UpdateUI(aggressorReferences);
                    BUM.UpdateUI(reference);
                }
            });
        }
        Debug.Log("Player Cast " + spell.name);
        SFM.CastSpellFeedback(spell, aggressorReferences, targetReferences);
        

        OSM.CastSpell();
    }

    void InitializeWall(Dictionary<ReferenceOperandScriptableObject, float> wall)
    {
        if(wall.Count > 0)
        {
            RemoveWallFromPersistent(wall);
            wall.Clear();
        }

        wall.Add(currentHPRef, 0);
        wall.Add(maxHPRef, 0);
        wall.Add(defRef, 0);
        BSD.InitStatuses(wall);
        wall[maxHPRef] = 9999999;
    }

    void RemoveWallFromPersistent(Dictionary<ReferenceOperandScriptableObject, float> wall)
    {
        for (int i = listOfPersistentEffectsEnemySide.Count - 1; i >= 0; --i)
        {
            if(listOfPersistentEffectsEnemySide[i].complexReferences.toTargetReferenceValues == wall)
            {
                listOfPersistentEffectsEnemySide.RemoveAt(i);
            }
        }

        for (int i = listOfPersistentEffectsPlayerSide.Count - 1; i >= 0; --i)
        {
            if (listOfPersistentEffectsPlayerSide[i].complexReferences.toTargetReferenceValues == wall)
            {
                listOfPersistentEffectsPlayerSide.RemoveAt(i);
            }
        }
    }

    public void UpdatePersistentEffectPlayer()
    {
        UpdatePersistentSpellEffect(listOfPersistentEffectsPlayerSide);
    }

    public void UpdatePersistentEffectEnemy()
    {
        UpdatePersistentSpellEffect(listOfPersistentEffectsEnemySide);
    }

    void UpdatePersistentSpellEffect(List<PersistentSpellEffect> listOfPersistentEffects)
    {
        for (int i = listOfPersistentEffects.Count-1; i >= 0;--i)
        {
            --listOfPersistentEffects[i].noOfTurnsLeft;

            if (listOfPersistentEffects[i].noOfTurnsLeft <= 0)
            {
                if(listOfPersistentEffects[i].effect.targetType == summonWall)
                {
                    if (listOfPersistentEffects[i].uglyReference == 1)//Player w
                    {
                        BUM.RemoveUI(BSM.player.wall);
                        BSM.player.wall.Clear();
                        playerWallParent.gameObject.SetActive(false);
                    }
                    else
                    {
                        BUM.RemoveUI(BSM.enemy.wall);
                        BSM.enemy.wall.Clear();
                        enemyWallParent.gameObject.SetActive(false);
                    }
                }
                else
                {
                    if (listOfPersistentEffects[i].effect.formula.toAssign != calculator.currentHP)
                    {
                        listOfPersistentEffects[i].complexReferences.toTargetReferenceValues[listOfPersistentEffects[i].effect.formula.toAssign] += listOfPersistentEffects[i].uglyReference;
                    }
                }
                listOfPersistentEffects[i].effectFeedback.Death();

                listOfPersistentEffects.RemoveAt(i);
            }
            else
            {
                if (listOfPersistentEffects[i].effect.targetType == summonWall)
                {
                    if(!listOfPersistentEffects[i].complexReferences.toTargetReferenceValues.ContainsKey(currentHPRef))
                    {
                        listOfPersistentEffects[i].effectFeedback.Death();

                        listOfPersistentEffects.RemoveAt(i);
                        continue;
                    }

                    if(listOfPersistentEffects[i].complexReferences.toTargetReferenceValues[currentHPRef] <=0)
                    {
                        if (listOfPersistentEffects[i].uglyReference == 1)//Player w
                        {
                            BUM.RemoveUI(BSM.player.wall);
                            BSM.player.wall.Clear();
                            playerWallParent.gameObject.SetActive(false);
                        }
                        else
                        {
                            BUM.RemoveUI(BSM.enemy.wall);
                            BSM.enemy.wall.Clear();
                            enemyWallParent.gameObject.SetActive(false);
                        }
                        listOfPersistentEffects[i].effectFeedback.Death();

                        listOfPersistentEffects.RemoveAt(i);
                        continue;
                    }
                }
                    if (listOfPersistentEffects[i].effect.formula.toAssign == calculator.currentHP && listOfPersistentEffects[i].effect.targetType != summonWall)
                {
                    listOfPersistentEffects[i].complexReferences.AddReference(listOfPersistentEffects[i].effect);

                    ActivateEffect(listOfPersistentEffects[i].effect, listOfPersistentEffects[i].complexReferences);

                    listOfPersistentEffects[i].complexReferences.RemoveReference(listOfPersistentEffects[i].effect);

                    BUM.UpdateUI(listOfPersistentEffects[i].complexReferences.fromTargetReferenceValues);
                    BUM.UpdateUI(listOfPersistentEffects[i].complexReferences.toTargetReferenceValues);
                }
            }
        }
    }

    float ActivateEffect(SpellEffectScriptableObject effect, ComplexReferences complexReferences)
    {
        return calculator.ResolveTargetAssignment(effect.formula, complexReferences);
    }

    public float EnemyCastSpell(Dictionary<ReferenceOperandScriptableObject, float> fromEnemy, SpellDataScriptableObject spell)
    {
        aggressorReferences = fromEnemy;

        for (int i = 0; i < spell.listOfEffects.Length; ++i)
        {
            targetReferences.Clear();
            SpellEffectScriptableObject effect = spell.listOfEffects[i];
            if (effect.targetType == enemySingle)
            {
                if(BSM.player.wall.Count > 0)
                {
                    targetReferences.Add(BSM.player.wall);
                }
                else
                {
                    targetReferences.Add(BSM.player.mainReference);
                }
            }
            else if (effect.targetType == enemyAOE)
            {
                if (BSM.player.wall.Count > 0)
                {
                    targetReferences.Add(BSM.player.wall);
                }
                else
                {
                    targetReferences.Add(BSM.player.mainReference);
                    targetReferences.Add(BSM.player.summon1Reference);
                    targetReferences.Add(BSM.player.summon2Reference);
                }
            }
            else if (effect.targetType == summonWall)
            {
                if (i == 0)
                {
                    enemyWallParent.gameObject.SetActive(true);
                    InitializeWall(BSM.enemy.wall);
                }

                targetReferences.Add(BSM.enemy.wall);
            }
            else
            {
                targetReferences.Add(fromEnemy);
            }

            for (int j = 0; j < targetReferences.Count; ++j)
            {
                if (targetReferences[j] == null)
                    continue;

                ComplexReferences newComplexReferences = new ComplexReferences(aggressorReferences, targetReferences[j], effect.targetType == self);
                

                newComplexReferences.AddReference(effect);

                if (effect.numberOfTurns > 1)
                {
                    float prevValue = newComplexReferences.toTargetReferenceValues[effect.formula.toAssign];
                    int noOfTurns = effect.formula.toAssign == cannotAttackReference ? effect.numberOfTurns + 1 : effect.numberOfTurns;
                    PoolObject effectFeedback = SFM.EffectFeedback(effect, targetReferences[j]);
                    PersistentSpellEffect persistentSpellEffect = new PersistentSpellEffect(noOfTurns, effect, newComplexReferences,effectFeedback);
                    persistentSpellEffect.uglyReference = prevValue - ActivateEffect(effect, newComplexReferences);

                    if (effect.targetType == summonWall)
                    {
                        persistentSpellEffect.uglyReference = -1;
                        BSM.enemy.wall[maxHPRef] = BSM.enemy.wall[currentHPRef];
                        BUM.AddUI(BSM.enemy.wall, enemyWallParent);
                        BUM.UpdateUI(BSM.enemy.wall);
                    }

                    listOfPersistentEffectsEnemySide.Add(persistentSpellEffect);
                }
                else
                {
                    ActivateEffect(effect, newComplexReferences);
                }

                newComplexReferences.RemoveReference(effect);

                BUM.UpdateUI(aggressorReferences);
                BUM.UpdateUI(targetReferences[j]);
            }
        }

        Debug.Log("Enemy Cast " + spell.name);
        return spell.spellAnimationDuration;
    }
}

class PersistentSpellEffect
{
    public int noOfTurnsLeft;
    public SpellEffectScriptableObject effect;
    public ComplexReferences complexReferences;
    public float uglyReference;
    public PoolObject effectFeedback;

    public PersistentSpellEffect(int turns, SpellEffectScriptableObject eff, ComplexReferences refer, PoolObject effectFeed)
    {
        noOfTurnsLeft = turns;
        effect = eff;
        complexReferences = refer;
        effectFeedback = effectFeed;
    }
}

public class ComplexReferences
{
    public Dictionary<ReferenceOperandScriptableObject, float> fromTargetReferenceValues;
    public Dictionary<ReferenceOperandScriptableObject, float> toTargetReferenceValues;
    public bool isFromTarget;

    public ComplexReferences(ComplexReferences copy)
    {
        fromTargetReferenceValues = copy.fromTargetReferenceValues;
        toTargetReferenceValues = copy.toTargetReferenceValues;
        isFromTarget = copy.isFromTarget;
    }

    public ComplexReferences(Dictionary<ReferenceOperandScriptableObject, float> fromReferences, Dictionary<ReferenceOperandScriptableObject, float> toReferences, bool fromTarget)
    {
        fromTargetReferenceValues = fromReferences;
        toTargetReferenceValues = toReferences;
        isFromTarget = fromTarget;
    }

    public void AddReference(SpellEffectScriptableObject effect)
    {
        if (effect.referenceType)
        {
            fromTargetReferenceValues.Add(effect.referenceType, effect.referenceValue);
            if(toTargetReferenceValues != fromTargetReferenceValues)
                toTargetReferenceValues.Add(effect.referenceType, effect.referenceValue);
        }

        for (int i = 0; i < effect.otherReferences.Length; ++i)
        {
            float value = 0;

            if(effect.otherReferences[i].value.GetType() == typeof(ReferenceOperandScriptableObject))
            {
                value = fromTargetReferenceValues[(ReferenceOperandScriptableObject)effect.otherReferences[i].value];
            }
            else if (effect.otherReferences[i].value.GetType() == typeof(ConstantOperandScriptableObject))
            {
                value = ((ConstantOperandScriptableObject)effect.otherReferences[i].value).value;
            }
            fromTargetReferenceValues.Add(effect.otherReferences[i].reference, value);

            if (toTargetReferenceValues != fromTargetReferenceValues)
                toTargetReferenceValues.Add(effect.otherReferences[i].reference, value);
        }
    }

    public void RemoveReference(SpellEffectScriptableObject effect)
    {
        if (effect.referenceType)
        {
            fromTargetReferenceValues.Remove(effect.referenceType);
            if (toTargetReferenceValues != fromTargetReferenceValues)
                toTargetReferenceValues.Remove(effect.referenceType);
        }

        for (int i = 0; i < effect.otherReferences.Length; ++i)
        {
            fromTargetReferenceValues.Remove(effect.otherReferences[i].reference);

            if (toTargetReferenceValues != fromTargetReferenceValues)
                toTargetReferenceValues.Remove(effect.otherReferences[i].reference);
        }
    }
}