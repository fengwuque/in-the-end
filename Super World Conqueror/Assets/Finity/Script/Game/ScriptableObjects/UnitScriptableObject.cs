﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

[CreateAssetMenu(fileName = "Unit", menuName = "ScriptableObjects/Battle/Unit")]
public abstract class UnitScriptableObject : ScriptableObject
{
    public BaseAttribute[] baseAttribute;
    public DerivedStatsScriptableObject dsso;
    public CalculatorScriptableObject calculator;
    public ReferenceOperandScriptableObject unitLevelOperand;
    public ReferenceOperandScriptableObject unitCurrentHPOperand;
    public ReferenceOperandScriptableObject unitMaxHPOperand;
    public ReferenceOperandScriptableObject unitDefenceOperand;
    public ReferenceOperandScriptableObject unitSpellEffectivenessOperand;
    public BattleStatusDataScriptableObject BSD;

    public Dictionary<ReferenceOperandScriptableObject, float> attributes;
    public abstract void Init();

    public abstract SpellDataScriptableObject GetSpell();
}

[Serializable]
public class BaseAttribute
{
    public ReferenceOperandScriptableObject attributeType;
    public float attributeValue;
}