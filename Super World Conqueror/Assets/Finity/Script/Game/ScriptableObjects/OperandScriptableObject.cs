﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Operand", menuName = "ScriptableObjects/BattleFormula/Operand")]
public class OperandScriptableObject : ScriptableObject
{
    float _value;

    public void Assign(float value)
    {
        _value = value;
    }

    public float Get()
    {
        return _value;
    }
}