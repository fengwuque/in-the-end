﻿using UnityEngine;
using System.Collections;
using RoboRyanTron.Unite2017.Variables;

[CreateAssetMenu(fileName = "OrbData", menuName = "ScriptableObjects/OrbData")]
public class OrbDataScriptableObject : ScriptableObject
{
    public BaseAttribute OrbAData;
    public BaseAttribute OrbBData;
    public BaseAttribute OrbCData;
    public EnumScriptableObject[] orbTypes;
}