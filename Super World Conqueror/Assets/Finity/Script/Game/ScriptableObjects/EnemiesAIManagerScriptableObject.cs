﻿using UnityEngine;
using System.Collections;
using RoboRyanTron.Unite2017.Events;

[CreateAssetMenu(fileName = "EnemiesAIManager", menuName = "ScriptableObjects/Battle/EnemiesAIManager")]
public class EnemiesAIManagerScriptableObject : ScriptableObject
{
    [SerializeField] float delayBeforeAttack;
    [SerializeField] float delayBeforeUpdatePersistentEffects;

    SideStats enemySide;
    [SerializeField] BattleStatusDataScriptableObject BSD;
    [SerializeField] CastSpellManagerScriptableObject CSM;
    [SerializeField] ReferenceOperandScriptableObject currentHP;
    bool isSummon1Attacked;
    bool isSummon2Attacked;

    [SerializeField] GameEvent enemySideComplete;

    UnitScriptableObject[] _enemies;

    public void Init(SideStats enemiesStats, UnitScriptableObject[] enemies)
    {
        enemySide = enemiesStats;
        _enemies = enemies;
    }

    void FirstEnemyAttack()
    {
        LeanTween.delayedCall(delayBeforeAttack, () => {
            if (enemySide.mainReference[currentHP] > 0)
            {
                float duration = CSM.EnemyCastSpell(enemySide.mainReference, _enemies[0].GetSpell());
                LeanTween.delayedCall(duration, () =>
                {
                    OnEnemyAttackComplete();
                });
            }
            else if (enemySide.summon1Reference != null)
            {
                if (enemySide.summon1Reference[currentHP] > 0 && enemySide.summon1Reference[BSD.unableToAttackBool] < 1)
                {
                    float duration = CSM.EnemyCastSpell(enemySide.summon1Reference, _enemies[1].GetSpell());
                    isSummon1Attacked = true;
                    LeanTween.delayedCall(duration, () =>
                    {
                        OnEnemyAttackComplete();
                    });
                }
            }
            else if (enemySide.summon2Reference != null)
            {
                if (enemySide.summon2Reference[currentHP] > 0 && enemySide.summon2Reference[BSD.unableToAttackBool] < 1)
                {
                    float duration = CSM.EnemyCastSpell(enemySide.summon2Reference, _enemies[2].GetSpell());
                    isSummon2Attacked = true;
                    LeanTween.delayedCall(duration, () =>
                    {
                        OnEnemyAttackComplete();
                    });
                }
            }
            else
            {
                Debug.Log("CONGRATULATIONS. YOU(Player) WON!");
            }
        });
    }

    public void EnemiesBeginAttack()
    {
        isSummon1Attacked = isSummon2Attacked = false;
        LeanTween.delayedCall(delayBeforeUpdatePersistentEffects, () => {
            CSM.UpdatePersistentEffectEnemy();
            FirstEnemyAttack();
            });
    }

    public void OnEnemyAttackComplete()
    {
        if (enemySide.summon1Reference != null && enemySide.summon1Reference[currentHP] > 0 && enemySide.summon1Reference[BSD.unableToAttackBool] < 1 && !isSummon1Attacked)
        {
                float duration = CSM.EnemyCastSpell(enemySide.summon1Reference, _enemies[1].GetSpell());
                isSummon1Attacked = true;
                LeanTween.delayedCall(duration, () =>
                {
                    OnEnemyAttackComplete();
                });
        }
        else if (enemySide.summon2Reference != null && enemySide.summon2Reference[currentHP] > 0 && enemySide.summon2Reference[BSD.unableToAttackBool] < 1 && !isSummon2Attacked)
        {
                float duration = CSM.EnemyCastSpell(enemySide.summon2Reference, _enemies[2].GetSpell());
                isSummon2Attacked = true;
                LeanTween.delayedCall(duration, () =>
                {
                    OnEnemyAttackComplete();
                });
        }
        else
        {
            enemySideComplete.Raise();
        }
    }
}