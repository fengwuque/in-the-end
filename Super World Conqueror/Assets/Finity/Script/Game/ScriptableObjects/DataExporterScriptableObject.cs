﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


public abstract class DataExporterScriptableObject : ScriptableObject
{
    public abstract void Export();

    [SerializeField] string filePath;

    public void ExportObject(object data, string dataName)
    {
        string concatenatedPath = Application.dataPath + filePath + dataName + ".json";

        string dataAsJson = JsonUtility.ToJson(data);

        File.WriteAllText(concatenatedPath, dataAsJson);
    }
}
