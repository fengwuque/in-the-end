﻿using UnityEngine;
using System.Collections;
using RoboRyanTron.Unite2017.Variables;
using System.Collections.Generic;
using UnityEngine.UI;
using RoboRyanTron.Unite2017.Events;
using System;

[CreateAssetMenu(fileName = "OrbSelectionManager", menuName = "ScriptableObjects/Battle/OrbSelectionManager")]
public class OrbSelectionManagerScriptableObject : ScriptableObject
{
    public List<EnumScriptableObject> selectedOrb;

    public bool isAutoCast;
    public bool breakEverySelection;

    [SerializeField] GameEvent on3OrbsSelected;
    [SerializeField] OrbRotationManagerScriptableObject ORM;
    [SerializeField] GameEvent onPlayerCastSpell;

    public void Init(GameObject castSpellBtn)
    {
        selectedOrb = new List<EnumScriptableObject>();

        if (isAutoCast) castSpellBtn.SetActive(false);
    }

    public void SelectOrb(EnumScriptableObject orbType)
    {
        selectedOrb.Add(orbType);

        if(selectedOrb.Count == 3)
        {
            if(isAutoCast)
            {
                onPlayerCastSpell.Raise();
            }

            on3OrbsSelected.Raise();
        }
        else if(breakEverySelection)
        {
            ORM.Refresh1Orb();
        }

    }

    public void CastSpell()
    {
        if(selectedOrb.Count != 3)
        {
            Debug.Log("ERRORRRRRR. SELECTED NUMBER OF ORB != 3");
        }
        else
        {
            selectedOrb.Clear();
        }
    }
}
