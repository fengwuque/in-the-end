﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

[CreateAssetMenu(fileName = "SpellFeedbackManager", menuName = "ScriptableObjects/Battle/SpellFeedbackManager")]
public class SpellFeedbackManagerScriptableObject : ScriptableObject
{
    [SerializeField] PoolerManagerScriptableObject spellFeedbackPoolerManager;
    [SerializeField] BattleTransformManagerScriptableObject BTM;
    [SerializeField] SpellFeedbackData[] spellFeedbackDatas;
    [SerializeField] EffectFeedbackData[] spellEffectDatas;

    Dictionary<SpellDataScriptableObject, PoolObject> spellFeedbackDictionary;
    Dictionary<SpellEffectScriptableObject, PoolObject> effectFeedbackDictionary;

    public void Init(Transform spellFeedbackParent)
    {
        spellFeedbackPoolerManager.Init(spellFeedbackParent);

        spellFeedbackDictionary = new Dictionary<SpellDataScriptableObject, PoolObject>();
        effectFeedbackDictionary = new Dictionary<SpellEffectScriptableObject, PoolObject>();

        for (int i = 0; i < spellFeedbackDatas.Length; ++i)
        {
            spellFeedbackDictionary.Add(spellFeedbackDatas[i].spell, spellFeedbackDatas[i].feedbackObject);
        }

        for(int i = 0; i < spellEffectDatas.Length; ++i)
        {
            effectFeedbackDictionary.Add(spellEffectDatas[i].effect, spellEffectDatas[i].feedbackObject);
        }
    }

    public void CastSpellFeedback(SpellDataScriptableObject spell, Dictionary<ReferenceOperandScriptableObject, float> fromTarget, List<Dictionary<ReferenceOperandScriptableObject, float>> toTargets)
    {
        if (spellFeedbackDictionary.ContainsKey(spell))
        {
            for(int i = 0; i < toTargets.Count; ++i)
            {
                if (toTargets[i] == null) continue;
                GameObject spellObj = spellFeedbackPoolerManager.GetPooler(spellFeedbackDictionary[spell]).GetPoolObject();
                spellObj.GetComponent<SpellFeedback>().ActivateFeedback(BTM.statsTransform[fromTarget].position, BTM.statsTransform[toTargets[i]].position);
            }
        }
        else // for spells not yet implemented
        {
            KeyValuePair<SpellDataScriptableObject, PoolObject> pair = spellFeedbackDictionary.First();
            for (int i = 0; i < toTargets.Count; ++i)
            {
                if (toTargets[i] == null) continue;
                GameObject spellObj = spellFeedbackPoolerManager.GetPooler(spellFeedbackDictionary[pair.Key]).GetPoolObject();
                spellObj.GetComponent<SpellFeedback>().ActivateFeedback(BTM.statsTransform[fromTarget].position, BTM.statsTransform[toTargets[i]].position);
            }
        }
    }

    public PoolObject EffectFeedback(SpellEffectScriptableObject effect, Dictionary<ReferenceOperandScriptableObject, float> toTarget)
    {
        if (effectFeedbackDictionary.ContainsKey(effect))
        {
            GameObject spellObj = spellFeedbackPoolerManager.GetPooler(effectFeedbackDictionary[effect]).GetPoolObject();
            spellObj.GetComponent<EffectFeedback>().ActivateFeedback(BTM.statsTransform[toTarget].position);

            return spellObj.GetComponent<PoolObject>();
        }
        else // for spells not yet implemented
        {
            KeyValuePair<SpellEffectScriptableObject, PoolObject> pair = effectFeedbackDictionary.First();
            GameObject spellObj = spellFeedbackPoolerManager.GetPooler(effectFeedbackDictionary[pair.Key]).GetPoolObject();
            spellObj.GetComponent<EffectFeedback>().ActivateFeedback(BTM.statsTransform[toTarget].position);
            return spellObj.GetComponent<PoolObject>();
        }
    }
}

[Serializable]
public class SpellFeedbackData
{
    public SpellDataScriptableObject spell;
    public PoolObject feedbackObject;
}

[Serializable]
public class EffectFeedbackData
{
    public SpellEffectScriptableObject effect;
    public PoolObject feedbackObject;
}

[Serializable]
public abstract class SpellFeedback: PoolObject
{
    public float spellDuration;
    
    public abstract void ActivateFeedback(Vector3 fromTarget, Vector3 toTarget);
}

[Serializable]
public abstract class EffectFeedback : PoolObject
{
    public float effectDuration;

    public abstract void ActivateFeedback(Vector3 toTarget);
}

