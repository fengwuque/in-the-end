﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "SpellDataManager", menuName = "ScriptableObjects/Battle/SpellDataManager")]
public class SpellDataManagerScriptableObject : ScriptableObject
{
    public SpellDataScriptableObject[] allAvailableSpells;

    Dictionary<string, SpellDataScriptableObject> _spellDictionary;

    public void Init()
    {
        _spellDictionary = new Dictionary<string, SpellDataScriptableObject>();

        for (int i = 0; i < allAvailableSpells.Length; ++i)
        {
            _spellDictionary.Add(ConvertEnumArrayToString(allAvailableSpells[i].orbTypeRequired), allAvailableSpells[i]);
        }
    }

    string ConvertEnumArrayToString(EnumScriptableObject[] enumArray)
    {
        EnumScriptableObject[] sortedEnum = SortEnumByName(enumArray);

        string enumString = "";

        for(int i = 0; i < sortedEnum.Length;++i)
        {
            enumString += sortedEnum[i].name;
        }

        return enumString;
    }

    public SpellDataScriptableObject GetSpell(EnumScriptableObject[] orbsUsed)
    {
        if(_spellDictionary == null)
        {
            Init();
        }
        
        return _spellDictionary[ConvertEnumArrayToString(orbsUsed)];
    }

    public EnumScriptableObject[] SortEnumByName(EnumScriptableObject[] enumScriptableObjects)
    {
        Array.Sort(enumScriptableObjects, (x, y) => String.Compare(x.name, y.name));

        return enumScriptableObjects;
    }
}

