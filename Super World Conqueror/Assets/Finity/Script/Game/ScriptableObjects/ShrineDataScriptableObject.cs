﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "ShrineData", menuName = "ScriptableObjects/ShrineData")]
public class ShrineDataScriptableObject : ScriptableObject
{
    public int shrineWorshipCDInMinutes;
    public int minGoldReward;
    public int maxGoldReward;
}