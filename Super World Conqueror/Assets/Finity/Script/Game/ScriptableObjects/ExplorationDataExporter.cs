﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ExplorationDataExporter", menuName = "ScriptableObjects/DataExporter/ExplorationDataExporter")]
public class ExplorationDataExporter : DataExporterScriptableObject
{
    [SerializeField] SpawnerScriptableObject tileSpawner;
    [SerializeField] SpawnCategoryScriptableObject tileResourceCategory;

    public override void Export()
    {
        SpawnerData tileSpawnerData = new SpawnerData(tileSpawner);

        ExportObject(tileSpawnerData, tileSpawner.name);

        SpawnCategoryData tileResourceCategoryData = new SpawnCategoryData(tileResourceCategory);

        ExportObject(tileResourceCategoryData, tileResourceCategory.name);
    }
}

class SpawnerData
{
    public float minSpawnDistance;
    public float maxSpawnDistance;

    public SpawnerData(SpawnerScriptableObject data)
    {
        minSpawnDistance = data.minSpawnDistance;
        maxSpawnDistance = data.maxSpawnDistance;
    }
}

class SpawnCategoryData
{
    public int minSpawnInterval;
    public int maxSpawnInterval;
    public int aliveTime;
    public string[] spawnTypes;

    public SpawnCategoryData(SpawnCategoryScriptableObject data)
    {
        minSpawnInterval = data.minSpawnInterval;
        maxSpawnInterval = data.maxSpawnInterval;
        aliveTime = data.aliveTime;

        spawnTypes = new string[data.SpawnTypes.Length];

        for(int i = 0; i < data.SpawnTypes.Length; ++i)
        {
            spawnTypes[i] = data.SpawnTypes[i].name;
        }
    }
}