﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "ResourceGainedFeedbackManager", menuName = "ScriptableObjects/Exploration/ResourceGainedFeedbackManager")]
public class ResourceGainedFeedbackManagerScriptableObject : ScriptableObject
{
    public PoolerManagerScriptableObject poolerManager;
    public PoolObject goldObj;
    public PoolObject dreamshardObj;
    Camera mainCam;
    public void Init(Transform poolerParent)
    {
        poolerManager.Init(poolerParent);
        mainCam = Camera.main;
    }

    public void SpawnGoldFeedback(int goldAmount, Vector3 worldPos)
    {
        GameObject gold = poolerManager.GetPooler(goldObj).GetPoolObject();
        gold.GetComponent<ResourceFeedback>().Init(mainCam.WorldToScreenPoint(worldPos), goldAmount);
    }

    public void SpawnDreamshardFeedback(int dreamshardAmount, Vector3 worldPos)
    {
        GameObject ds = poolerManager.GetPooler(dreamshardObj).GetPoolObject();
        ds.GetComponent<ResourceFeedback>().Init(mainCam.WorldToScreenPoint(worldPos), dreamshardAmount);
    }
}