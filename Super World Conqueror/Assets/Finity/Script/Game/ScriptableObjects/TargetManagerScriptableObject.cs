﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "TargetManager", menuName = "ScriptableObjects/Battle/TargetManager")]
public class TargetManagerScriptableObject : ScriptableObject
{
    Transform _currentTarget;

    List<Transform> _targets;

    Transform _targetUI;

    [SerializeField] GameObject targetPrefab;
    [SerializeField] Vector3 posOffset;

    //Camera cam;

    public void InitTargets(List<Transform> targets, Transform targetParent)
    {
        //cam = Camera.main;
        GameObject newTarget = Instantiate(targetPrefab) as GameObject;
        _targetUI = newTarget.transform;
        _targetUI.SetParent(targetParent);
        _targets = targets;

        SelectCurrentTarget(targets[0]);
    }

    public int GetCurrentTarget()
    {
        return _targets.IndexOf(_currentTarget);
    }

    public void SelectCurrentTarget(Transform target)
    {
        _currentTarget = target;
        _targetUI.position = new Vector3(_currentTarget.position.x,0.1f,_currentTarget.position.z);
    }
}