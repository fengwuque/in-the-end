﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/CharacterSavedData")]
public class CharacterSavedDataScriptableObject : ScriptableObject
{
    public int level;
    public bool isPlayer;
    public string characterName;
    public List<EquipSaveData> inventory;
    public List<EquipmentSlot> equipmentSlotID;
}

[Serializable]
public class EquipmentSlot
{
    public EQUIPMENT_PARTS part;
    public int slotID;
}