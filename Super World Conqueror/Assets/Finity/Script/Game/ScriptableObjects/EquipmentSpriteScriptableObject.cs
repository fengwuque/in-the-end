﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "EquipmentSpriteData", menuName = "ScriptableObjects/Equipment Sprites Data", order = 3)]
public class EquipmentSpriteScriptableObject : ScriptableObject
{
    [SerializeField] EquipmentSprite[] listOfAllSprites;
    [SerializeField] SkinData[] skins;


    Dictionary<EQUIPMENT_SETS, Dictionary<EQ_SPRITE_PART, Sprite>> _setParts;

    public Sprite GetSprite(EQUIPMENT_SETS set, EQ_SPRITE_PART part)
    {
        if(_setParts == null)
        {
            InitSetParts();
        }

        if(_setParts.ContainsKey(set))
        {
            if(_setParts[set].ContainsKey(part))
            {
                return _setParts[set][part];
            }
        }

        return null;
    }

    void InitSetParts()
    {
        _setParts = new Dictionary<EQUIPMENT_SETS, Dictionary<EQ_SPRITE_PART, Sprite>>();

        for(int i = 0; i < listOfAllSprites.Length; ++i)
        {
            Dictionary<EQ_SPRITE_PART, Sprite> partSprite = new Dictionary<EQ_SPRITE_PART, Sprite>();

            for(int j = 0; j < listOfAllSprites[i].parts.Length; ++j)
            {
                partSprite.Add(listOfAllSprites[i].parts[j].part, listOfAllSprites[i].parts[j].sprite);
            }

            _setParts.Add(listOfAllSprites[i].setName, partSprite);
        }
    }
}

[Serializable]
public class SkinData
{
    public SKIN skinName;
    public GameObject skinObject;
}

public enum SKIN
{
    HUMAN = 0,
    ORC,
    ELF,
    UNDEAD,
    ANGEL,
    DEMON,
    DWARF,
    WEREBEAST,
    PIXIE,
    GOLEM
}

public enum EQ_SPRITE_PART
{
    HEAD = 0,
    TOP,
    SHOULDER_FRONT,
    SHOULDER_BACK,
    BOTTOM,
    HAND_FRONT,
    HAND_BACK,
    FEET,
    CAPE
}
[Serializable]
public class EquipmentSprite
{
    public EQUIPMENT_SETS setName;
    public EquipmentSpritePart[] parts;
}

[Serializable]
public class EquipmentSpritePart
{
    public EQ_SPRITE_PART part;
    public Sprite sprite;
}