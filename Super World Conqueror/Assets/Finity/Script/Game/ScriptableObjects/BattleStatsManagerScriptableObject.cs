﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "BattleStatsManager", menuName = "ScriptableObjects/Battle/BattleStatsManager")]
public class BattleStatsManagerScriptableObject : ScriptableObject
{
    public SideStats player;
    public SideStats enemy;
    public bool showStats;
#if UNITY_EDITOR
    public void UpdateStats()
    {
        if (!showStats)
            return;
        DisplaySideStats(player, "Player");
        DisplaySideStats(enemy, "Enemy");
    }

    void DisplaySideStats(SideStats side, string name)
    {
        DisplayStat(side.mainReference, name + " Main");
        DisplayStat(side.summon1Reference, name + " Summon 1");
        DisplayStat(side.summon2Reference, name + " Summon 2");
        DisplayStat(side.wall, name + " Wall");
    }

    void DisplayStat(Dictionary<ReferenceOperandScriptableObject, float> stats, string name)
    {
        if (stats == null) return;

        foreach(KeyValuePair<ReferenceOperandScriptableObject, float> stat in stats)
        {
            Debug.Log(name + " " + stat.Key.name + " = " + stat.Value);
        }
    }
#endif

    public void Init(Dictionary<ReferenceOperandScriptableObject,float> playerRef, List<Dictionary<ReferenceOperandScriptableObject, float>> enemiesRefs)
    {
        
        player = new SideStats();
        player.InitSide(playerRef,null,null);

        enemy = new SideStats();

        switch (enemiesRefs.Count)
        {
            case 1:
                enemy.InitSide(enemiesRefs[0],null,null);
                break;
            case 2:
                enemy.InitSide(enemiesRefs[0], enemiesRefs[1],null);
                break;
            case 3:
                enemy.InitSide(enemiesRefs[0], enemiesRefs[1], enemiesRefs[2]);
                break;
            default:
                Debug.Log("ENEMIES STATS ERROR, 0 OR TOO MANY");
                break;
        }
#if UNITY_EDITOR
        UpdateStats();
#endif
    }
}

public class SideStats
{
    public Dictionary<ReferenceOperandScriptableObject, float> mainReference;
    public Dictionary<ReferenceOperandScriptableObject, float> summon1Reference;
    public Dictionary<ReferenceOperandScriptableObject, float> summon2Reference;
    public Dictionary<ReferenceOperandScriptableObject, float> wall;

    public void InitSide(Dictionary<ReferenceOperandScriptableObject, float> initMainReference, Dictionary<ReferenceOperandScriptableObject, float> initSummon1Reference, Dictionary<ReferenceOperandScriptableObject, float> initSummon2Reference)
    {
        mainReference = initMainReference;
        summon1Reference = initSummon1Reference;
        summon2Reference = initSummon2Reference;
        wall = new Dictionary<ReferenceOperandScriptableObject, float>();
    }
}