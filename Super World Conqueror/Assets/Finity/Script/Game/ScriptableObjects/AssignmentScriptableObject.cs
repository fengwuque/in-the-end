﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Assignment", menuName = "ScriptableObjects/BattleFormula/Assignment")]
public class AssignmentScriptableObject : OperandScriptableObject
{
    public ExpressionScriptableObject formula;
}