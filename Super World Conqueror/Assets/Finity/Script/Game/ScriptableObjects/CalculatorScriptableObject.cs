﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Calculator", menuName = "ScriptableObjects/BattleFormula/Calculator")]
public class CalculatorScriptableObject : ScriptableObject
{
    public OperatorScriptableObject Add;
    public OperatorScriptableObject Subtract;
    public OperatorScriptableObject Multiply;
    public OperatorScriptableObject Divide;
    public OperatorScriptableObject Power;
    public OperatorScriptableObject Min;
    public OperatorScriptableObject Max;
    public ReferenceOperandScriptableObject currentHP;

    [SerializeField] AssignStatsManagerScriptableObject ASM;

    Dictionary<ReferenceOperandScriptableObject, float> _referenceValues;

    public bool showAllCalculations;

    public float ResolveExpression(OperandScriptableObject operand, Dictionary<ReferenceOperandScriptableObject,float> referenceValues)
    {
        _referenceValues = referenceValues;
        return ResolveExpression(operand);
    }

    float ResolveExpression(OperandScriptableObject operand)
    {
        if (showAllCalculations) Debug.Log(operand.name);
        if (operand.GetType() == typeof(OperandScriptableObject))
        {
            if (showAllCalculations) Debug.Log(operand.name + " = " + operand.Get());
            return operand.Get();
        }
        else if (operand.GetType() == typeof(ConstantOperandScriptableObject))
        {
            if (showAllCalculations) Debug.Log(operand.name + " = " + ((ConstantOperandScriptableObject)operand).value);
            return ((ConstantOperandScriptableObject)operand).value;
        }
        else if (operand.GetType() == typeof(ReferenceOperandScriptableObject))
        {
            if(_referenceValues.ContainsKey((ReferenceOperandScriptableObject)operand))
            {
                if (showAllCalculations) Debug.Log(operand.name + " = " + _referenceValues[(ReferenceOperandScriptableObject)operand]);
                return _referenceValues[(ReferenceOperandScriptableObject)operand];
            }
            else
            {
                if (showAllCalculations) Debug.Log("ERROR. Does not contain reference" + operand.name);
                return 0;
            }
            
        }
        else if (operand.GetType() == typeof(ExpressionScriptableObject))
        {
            ExpressionScriptableObject expression = (ExpressionScriptableObject)operand;
            float expressionResult = 0;
            if (expression.op == Add)
            {
                expressionResult = ResolveExpression(expression.lhs) + ResolveExpression(expression.rhs);
            }
            else if (expression.op == Multiply)
            {
                expressionResult = ResolveExpression(expression.lhs) * ResolveExpression(expression.rhs);
            }
            else if (expression.op == Subtract)
            {
                expressionResult = ResolveExpression(expression.lhs) - ResolveExpression(expression.rhs);
            }
            else if (expression.op == Divide)
            {
                expressionResult = ResolveExpression(expression.lhs) / ResolveExpression(expression.rhs);
            }
            else if(expression.op == Power)
            {
                expressionResult = Mathf.Pow(ResolveExpression(expression.lhs), ResolveExpression(expression.rhs));
            }
            else
            {
                if (showAllCalculations) Debug.Log("UNDEFINED OPERATOR!!!!");
            }

            if (showAllCalculations) Debug.Log(operand.name + " = " + expressionResult);

            return expressionResult;
        }
        else if (operand.GetType() == typeof(AssignmentScriptableObject))
        {
            float assignmentResult = ResolveExpression(((AssignmentScriptableObject)operand).formula);
            if (showAllCalculations) Debug.Log(operand.name + " = " + assignmentResult);
            return assignmentResult;
        }

        return 0;
    }

    public float ResolveTargetAssignment(OperandScriptableObject operand, ComplexReferences complexReferences)
    {
        bool _isFromTarget = operand.GetType() == typeof(TargetAssignmentScriptableObject) ? ((TargetAssignmentScriptableObject)operand).isTargetingSelf : complexReferences.isFromTarget;// isFromTarget;
        
        Dictionary<ReferenceOperandScriptableObject,float> referenceValues = _isFromTarget ? complexReferences.fromTargetReferenceValues : complexReferences.toTargetReferenceValues;

        ComplexReferences copyComplex = new ComplexReferences(complexReferences);
        copyComplex.isFromTarget = _isFromTarget;

        if (showAllCalculations) Debug.Log(operand.name);
        if (operand.GetType() == typeof(OperandScriptableObject))
        {
            if (showAllCalculations) Debug.Log(operand.name + " = " + operand.Get());
            return operand.Get();
        }
        else if(operand.GetType() == typeof(TargetAssignmentScriptableObject))
        {
            TargetAssignmentScriptableObject assignment = (TargetAssignmentScriptableObject)operand;
            float value = ResolveTargetAssignment(assignment.formula, copyComplex);

            ASM.AssignTarget(assignment, copyComplex, value, referenceValues);
            
            return value;
        }
        else if (operand.GetType() == typeof(ConstantOperandScriptableObject))
        {
            if (showAllCalculations) Debug.Log(operand.name + " = " + ((ConstantOperandScriptableObject)operand).value);
            return ((ConstantOperandScriptableObject)operand).value;
        }
        else if (operand.GetType() == typeof(ReferenceOperandScriptableObject))
        {
            if (referenceValues.ContainsKey((ReferenceOperandScriptableObject)operand))
            {
                if (showAllCalculations) Debug.Log(operand.name + " = " + referenceValues[(ReferenceOperandScriptableObject)operand]);
                //Debug.Log(operand.name + " = " + referenceValues[(ReferenceOperandScriptableObject)operand]);
                return referenceValues[(ReferenceOperandScriptableObject)operand];
            }
            else
            {
                if (showAllCalculations) Debug.Log("ERROR. Does not contain reference" + operand.name);
                return 0;
            }

        }
        else if (operand.GetType() == typeof(ExpressionScriptableObject))
        {
            ExpressionScriptableObject expression = (ExpressionScriptableObject)operand;
            float expressionResult = 0;

            float lhs = ResolveTargetAssignment(expression.lhs, copyComplex);
            float rhs = ResolveTargetAssignment(expression.rhs, copyComplex);

            if (expression.op == Add)
            {
                expressionResult = lhs + rhs;
            }
            else if (expression.op == Multiply)
            {
                expressionResult = lhs * rhs;
            }
            else if (expression.op == Subtract)
            {
                expressionResult = lhs - rhs;
            }
            else if (expression.op == Divide)
            {
                expressionResult = lhs / rhs;
            }
            else if (expression.op == Power)
            {
                expressionResult = Mathf.Pow(lhs, rhs);
            }
            else if (expression.op == Min)
            {
                expressionResult = Mathf.Min(lhs,rhs);
            }
            else if (expression.op == Max)
            {
                expressionResult = Mathf.Max(lhs, rhs);
            }
            else
            {
                if (showAllCalculations) Debug.Log("UNDEFINED OPERATOR!!!!");
            }

            if (showAllCalculations) Debug.Log(operand.name + " = " + expressionResult);

            return expressionResult;
        }
        else if (operand.GetType() == typeof(AssignmentScriptableObject))
        {
            float assignmentResult = ResolveTargetAssignment(((AssignmentScriptableObject)operand).formula, copyComplex);
            if (showAllCalculations) Debug.Log(operand.name + " = " + assignmentResult);
            return assignmentResult;
        }

        return 0;
    }

    //void AssignTarget(TargetAssignmentScriptableObject assignment, ComplexReferences complexReferences, float value, Dictionary<ReferenceOperandScriptableObject, float> referenceValues)
    //{
    //    if (assignment.toAssign != null)
    //    {
    //        if (assignment.toAssign == currentHP)
    //        {
    //            if (showAssignmentCalculations) Debug.Log("Before " + assignment.toAssign + " = " + referenceValues[assignment.toAssign]);
    //            float difference = referenceValues[assignment.toAssign] - value;

    //            if (difference > 0)//kanna damage
    //            {
    //                if (referenceValues[BSD.reflectDamagePercent] > 0)
    //                {
                        
    //                    complexReferences.fromTargetReferenceValues[currentHP] -= difference * referenceValues[BSD.reflectDamagePercent];
    //                    if (showAssignmentCalculations) Debug.Log("After " + assignment + " = " + complexReferences.fromTargetReferenceValues[currentHP]);
    //                }

    //                value = value < 0 ? 0 : value;
    //                referenceValues[assignment.toAssign] = value;
    //            }
    //            else // kanna heal
    //            {
    //                if (referenceValues[BSD.reflectHealingPercent] > 0)
    //                {
    //                    complexReferences.toTargetReferenceValues[currentHP] += difference * referenceValues[BSD.reflectHealingPercent];
    //                }

    //                if (referenceValues[BSD.preventHealingBool] < 1)
    //                {
    //                    value = value > referenceValues[maxHP] ? referenceValues[maxHP] : value;
    //                    referenceValues[assignment.toAssign] = value;
    //                }
    //            }
    //            if (showAssignmentCalculations) Debug.Log("After " + assignment.toAssign + " = " + referenceValues[assignment.toAssign]);
    //        }
    //        else
    //        {
    //            if (showAssignmentCalculations) Debug.Log("Before " + assignment.toAssign + " = " + referenceValues[assignment.toAssign]);
    //            referenceValues[assignment.toAssign] = value;
    //            if (showAssignmentCalculations) Debug.Log("After " + assignment.toAssign + " = " + referenceValues[assignment.toAssign]);
    //        }

    //    }
    //}
}

