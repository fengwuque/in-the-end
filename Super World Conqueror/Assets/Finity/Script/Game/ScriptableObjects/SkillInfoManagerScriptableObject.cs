﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "SkillInfoManager", menuName = "ScriptableObjects/Manager/SkillInfoManager")]
public class SkillInfoManagerScriptableObject : SingletonScriptableObject<SkillInfoManagerScriptableObject>
{
    public GameObject skillInfoPrefab;
    Transform _parent;
    GameObject skillInfo;
    public void Init(Transform parent)
    {
        _parent = parent;
    }

    public void EnableSkillInfo()
    {
        skillInfo = Instantiate(skillInfoPrefab);
        skillInfo.transform.SetParent(_parent);
        skillInfo.transform.localPosition = Vector3.zero;
        skillInfo.transform.localScale = Vector3.one;
    }

    public void DisableSkillInfo()
    {
        Destroy(skillInfo);
    }
}