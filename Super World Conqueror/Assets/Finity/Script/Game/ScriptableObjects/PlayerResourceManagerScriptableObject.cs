﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "PlayerResourceManager", menuName = "ScriptableObjects/Manager/PlayerResourceManager")]
public class PlayerResourceManagerScriptableObject : ScriptableObject
{
    [SerializeField] ResourceGainedDataScriptableObject RGD;
    [SerializeField] MapSpawnSavedDataManagerScriptableObject MSSD;
}