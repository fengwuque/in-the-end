﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "TargetAssignment", menuName = "ScriptableObjects/BattleFormula/TargetAssignment")]
public class TargetAssignmentScriptableObject : AssignmentScriptableObject
{
    public ReferenceOperandScriptableObject toAssign;
    public bool isTargetingSelf;
}