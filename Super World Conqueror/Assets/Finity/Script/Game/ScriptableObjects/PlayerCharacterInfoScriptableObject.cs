﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/PlayerCharacterInfo", order = 2)]
public class PlayerCharacterInfoScriptableObject : ScriptableObject
{
    public List<Stat> baseStat;
    public List<Stat> statGrowth;
    public float criticalDamage;

    public Dictionary<STAT_TYPE,float> GetCharacterStats(int level)
    {
        Dictionary<STAT_TYPE, float> stats = new Dictionary<STAT_TYPE, float>();

        for(int i = 0; i < (int)STAT_TYPE.TOTAL_NO_OF_STATS; ++i)
        {
            stats.Add((STAT_TYPE)i, 0);
        }

        stats[STAT_TYPE.CRITICAL_DAMAGE] = criticalDamage;

        for (int i = 0; i < baseStat.Count; ++i)
        {
            stats[baseStat[i].statType] = (baseStat[i].statValue + statGrowth[i].statValue * level);
        }
        
        return stats;
    }
}