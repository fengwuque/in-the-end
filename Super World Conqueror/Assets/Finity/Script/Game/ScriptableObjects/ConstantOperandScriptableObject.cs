﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "ConstantOperand", menuName = "ScriptableObjects/BattleFormula/ConstantOperand")]
public class ConstantOperandScriptableObject : OperandScriptableObject
{
    public float value;
}