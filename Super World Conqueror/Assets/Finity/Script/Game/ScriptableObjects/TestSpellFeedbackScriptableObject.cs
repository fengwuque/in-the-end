﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "TestSpellFeedback", menuName = "ScriptableObjects/Battle/TestSpellFeedback")]
public class TestSpellFeedbackScriptableObject : ScriptableObject
{
    [SerializeField] GameObject feedbackToTest;
    [SerializeField] Vector3 fromTarget;
    [SerializeField] Vector3 toTarget;

    public void Test()
    {
        feedbackToTest.GetComponent<SpellFeedback>().ActivateFeedback(fromTarget, toTarget);
    }
}