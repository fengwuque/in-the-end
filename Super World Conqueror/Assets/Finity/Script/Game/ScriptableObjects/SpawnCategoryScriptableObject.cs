﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "SpawnCategory", menuName = "ScriptableObjects/SpawningData/SpawnCategory")]
public class SpawnCategoryScriptableObject : ScriptableObject
{
    [Tooltip("In Minutes")]
    [Range(1,1440)]
    public int minSpawnInterval;
    [Range(1, 1440)]
    public int maxSpawnInterval;
    [Tooltip("In Minutes")]
    [Range(1, 1440)]
    public int aliveTime;
    public SpawnTypeScriptableObject[] SpawnTypes;

    List<float> spawnWeights;

    [SerializeField] bool hasNewData;

    public void Init()
    {
        if (hasNewData || spawnWeights == null)
        {
            hasNewData = false;
            spawnWeights = new List<float>();

            for (int i = 0; i < SpawnTypes.Length; ++i)
            {
                if (i == 0)
                {
                    spawnWeights.Add(SpawnTypes[0].weight);
                }
                else
                {
                    spawnWeights.Add(spawnWeights[i - 1] + SpawnTypes[i].weight);
                }
                Debug.Log(spawnWeights[i]);
            }
        }
    }

    public SpawnTypeScriptableObject GetRandomSpawn(float randomNumber)
    {
        if(spawnWeights.Count== 0)
        {
            hasNewData = true;
            Init();
        }

        float inflatedNumber = spawnWeights[spawnWeights.Count - 1] * randomNumber;
        for (int i = 0; i < SpawnTypes.Length; ++i)
        {
            if(spawnWeights[i] > inflatedNumber)
            {
                //Debug.Log(inflatedNumber);
                //Debug.Log(SpawnTypes[i]);
                return SpawnTypes[i];
            }
        }

        return SpawnTypes[SpawnTypes.Length - 1];
    }
}

