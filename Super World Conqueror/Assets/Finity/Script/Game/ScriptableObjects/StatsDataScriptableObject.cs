﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "StatsData", menuName = "ScriptableObjects/StatsData")]
public class StatsDataScriptableObject : ScriptableObject
{
    [Header("Affected By Level")]
    public List<Stat> statsRelation;
    public float statDifference;

    [Header("Not Affected By Level")]
    [SerializeField] List<Stat> minStat;
    [SerializeField] List<Stat> maxStat;
    [SerializeField] List<Stat> roundToNearest;

    Dictionary<STAT_TYPE, Vector3> _minMaxStat;
    Dictionary<STAT_TYPE, float> _statRelationDictionary;

    public Dictionary<STAT_TYPE, Vector3> minMaxStat
    {
        get
        {
            if(_minMaxStat == null)
            {
                _minMaxStat = new Dictionary<STAT_TYPE, Vector3>();

                if(minStat.Count != maxStat.Count)
                {
                    Debug.Log("min stats max stats count not same");
                    return null;
                }

                for(int i = 0;i<minStat.Count;++i)
                {
                    _minMaxStat.Add(minStat[i].statType, new Vector3(minStat[i].statValue, maxStat[i].statValue, roundToNearest[i].statValue));
                }
            }

            return _minMaxStat;
        }
        set
        {
            _minMaxStat = value;
        }
    }

    public Dictionary<STAT_TYPE,float> GetStatsRelation()
    {
        if(_statRelationDictionary == null)
        {
            _statRelationDictionary = new Dictionary<STAT_TYPE, float>();

            for(int i = 0; i < statsRelation.Count; ++i)
            {
                _statRelationDictionary.Add(statsRelation[i].statType, statsRelation[i].statValue);
            }
        }

        return _statRelationDictionary;
    }
}

[Serializable]
public enum STAT_TYPE
{
    HEALTH_POINT = 0,
    NORMAL_DAMAGE,
    MAGIC_DAMAGE,
    NORMAL_ARMOR,
    MAGIC_ARMOR,
    ATTACK_SPEED,
    THORNS_DAMAGE,
    HEALING_BONUS,
    DODGE_RATE,
    CRITICAL_CHANCE,
    CRITICAL_DAMAGE,
    NORMAL_ARMOR_PENETRATION,
    MAGIC_ARMOR_PENETRATION,
    FIRE_DAMAGE_INCREMENT,
    COLD_DAMAGE_INCREMENT,
    POISON_DAMAGE_INCREMENT,
    LIGHTNING_DAMAGE_INCREMENT,
    COOLDOWN_REDUCTION,
    LIFE_STEAL,
    AREA_DAMAGE,
    ATTACK_SPEED_INCREMENT,
    REGEN_PER_SEC,
    TOTAL_NO_OF_STATS
}

[Serializable]
public class Stat
{
    public STAT_TYPE statType;
    public float statValue;

    public Stat(STAT_TYPE type, float v)
    {
        statType = type;
        statValue = v;
    }
}