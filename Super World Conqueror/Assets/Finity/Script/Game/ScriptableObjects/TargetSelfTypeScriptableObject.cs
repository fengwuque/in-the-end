﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "TargetSelfType", menuName = "ScriptableObjects/Battle/TargetSelf")]
public class TargetSelfTypeScriptableObject : TargetTypeScriptableObject
{
    
}