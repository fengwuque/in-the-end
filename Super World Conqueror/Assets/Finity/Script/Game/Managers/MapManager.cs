﻿using Mapbox.Map;
using Mapbox.Unity.Map;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour {
    [SerializeField]
    AbstractMap myMap;

	// Use this for initialization
	void Start () {
        myMap.OnInitialized += OnMapInit;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMapInit()
    {
        myMap.TileProvider.OnTileAdded += OnTileAdded;
    }

    void OnTileAdded(UnwrappedTileId tileID)
    {
        
    }

    private void OnDestroy()
    {
        myMap.OnInitialized -= OnMapInit;
    }
}
