﻿using RoboRyanTron.SceneReference;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransitionManager : MonoBehaviour
{
    [SerializeField] CanvasGroup loadingCanvas;
    [SerializeField] SceneDataScriptableObject sceneData;
    // Use this for initialization
    void Start ()
    {
        FadeOut();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ChangeScene(SceneReference scene)
    {

        loadingCanvas.gameObject.SetActive(true);
        LeanTween.value(loadingCanvas.alpha, 1, 2).setOnUpdate((float v) => { loadingCanvas.alpha = v; }).setEaseOutExpo().setOnComplete(
            () => { scene.LoadScene(); FadeOut(); }
            );
    }

    public Scene GetCurrentScene()
    {
        return SceneManager.GetActiveScene();
    }

    public void GoTestBattle()
    {
        SceneManager.LoadScene("TestFightScene");
    }

    public void GoToMap()
    {
        SceneManager.LoadScene("MapScene");
    }

    void FadeOut()
    {
        LeanTween.value(loadingCanvas.alpha, 0, 2).setOnUpdate((float v) => { loadingCanvas.alpha = v; }).setEaseOutExpo().setOnComplete(()=> { loadingCanvas.gameObject.SetActive(false); });
    }

    void FadeIn()
    {
        LeanTween.value(loadingCanvas.alpha, 1, 2).setOnUpdate((float v) => { loadingCanvas.alpha = v; }).setEaseOutExpo();
    }
}

public enum SCENE_NAME
{
    CREATE_ACC = 0,
    MAP_SCENE
}
