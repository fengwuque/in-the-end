﻿using Mapbox.Map;
using Mapbox.Unity.Map;
using RoboRyanTron.Unite2017.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSpawningManager : MonoBehaviour
{
    public AbstractMap myMap;

    AbstractTileProvider tileProvider;
    Dictionary<UnwrappedTileId, MapSpawnTileManager> tileManagers;

    [SerializeField] WorldObjectSpawningManagerScriptableObject WOSM;
    [SerializeField] SpawnerScriptableObject tileSpawner;
    [SerializeField] GameEvent explorationInitialized;

    int seed;
    GSEventRequestManager GERM;
    public void InitMap()
    {
        tileManagers = new Dictionary<UnwrappedTileId, MapSpawnTileManager>();
        tileProvider = myMap.TileProvider;
        tileProvider.OnTileAdded += AddTile;
        tileProvider.OnTileRemoved += RemoveTile;
        Debug.Log("Init Map");
        WOSM.InitSpawningManager(transform);
        explorationInitialized.Raise();
    }

    public void InitSeed(PlayerData playerData)
    {
        seed = playerData.currentSeed;
        myMap.gameObject.SetActive(true);
        myMap.OnInitialized += InitMap;
        Debug.Log("Init Seeds");
        InvokeRepeating("UpdateTileManagers", 60, 60);
    }

    // Use this for initialization
    void Start()
    {
        GERM = GameSparksManager.instance.GetComponent<GSEventRequestManager>();
        if(GERM.IsInitialized())
        {
            InitSeed(GERM.GetComponent<PlayerAccountManager>().GetPlayerData());
        }
        else
        {
            GERM.onInitialized += InitSeed;
        }
    }

    void UpdateTileManagers()
    {
        WOSM.UpdateSpawner();
    }

    public void AddTile(UnwrappedTileId tileID)
    {
        WOSM.AddSpawner(tileSpawner, myMap.MapVisualizer.ActiveTiles[tileID].transform,tileID,seed);
    }

    public void RemoveTile(UnwrappedTileId tileID)
    {
        WOSM.RemoveSpawner(tileID);
    }

    private void OnDestroy()
    {
        if (myMap != null && tileProvider != null)
        {
            myMap.OnInitialized -= InitMap;
            tileProvider.OnTileAdded -= AddTile;
            tileProvider.OnTileRemoved -= RemoveTile;
        }

    }
}
