﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEQSpriteManager : MonoBehaviour
{
    [SerializeField] EquipmentSpriteScriptableObject eqSpriteData;
    [SerializeField] CharacterSpriteManager CSM;
    EQUIPMENT_SETS _eqSet;
    EQUIPMENT_SETS eqSet
    {
        get { return _eqSet; }
        set {

            _eqSet = value;
            EQSpriteInfo spritesInfo = new EQSpriteInfo();
            spritesInfo.head            = eqSpriteData.GetSprite(_eqSet, EQ_SPRITE_PART.HEAD);
            spritesInfo.top             = eqSpriteData.GetSprite(_eqSet, EQ_SPRITE_PART.TOP);
            spritesInfo.shoulderFront   = eqSpriteData.GetSprite(_eqSet, EQ_SPRITE_PART.SHOULDER_FRONT);
            spritesInfo.shoulderBack    = eqSpriteData.GetSprite(_eqSet, EQ_SPRITE_PART.SHOULDER_BACK);
            spritesInfo.bottom          = eqSpriteData.GetSprite(_eqSet, EQ_SPRITE_PART.BOTTOM);
            spritesInfo.handFront       = eqSpriteData.GetSprite(_eqSet, EQ_SPRITE_PART.HAND_FRONT);
            spritesInfo.handBack        = eqSpriteData.GetSprite(_eqSet, EQ_SPRITE_PART.HAND_BACK);
            spritesInfo.cape            = eqSpriteData.GetSprite(_eqSet, EQ_SPRITE_PART.CAPE);
            spritesInfo.feet            = eqSpriteData.GetSprite(_eqSet, EQ_SPRITE_PART.FEET);

            CSM.SetEQSprite(spritesInfo);
        }
    }
    [SerializeField]
    EQUIPMENT_SETS testSet;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(testSet != eqSet)
        {
            eqSet = testSet;
        }
	}
}
