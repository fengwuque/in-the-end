﻿using Mapbox.Geocoding;
using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CreateAccountManager : MonoBehaviour
{
    #region Variables
    [SerializeField] TMP_InputField displayNameInput;
    [SerializeField] GameObject registerGO;
    [SerializeField] CanvasGroup loadingScreen;
    [SerializeField] Mapbox.Examples.ForwardGeocodeUserInput _searchLocation;
    [SerializeField] TextMeshProUGUI locationMapText;
    [SerializeField] Button FBButton;
    [SerializeField] Button GuestButton;
    bool isLocationValid;
    bool isNicknameValid;
    Vector2d longlat;
    #endregion

    GSEventRequestManager GERM;
    private void Awake()
    {
        
        
    }

    private void Start()
    {
        _searchLocation.OnGeocoderResponse += SearchLocation_OnGeocoderResponse;

        displayNameInput.onValueChanged.AddListener(delegate { NickValueChanged(); });
        Debug.Log("gs instance = " + GameSparksManager.instance);
        GERM = GameSparksManager.instance.GetComponent<GSEventRequestManager>();
    }

    #region PlayerInput
    void NickValueChanged()
    {
        if (displayNameInput.text.Length > 3 && displayNameInput.text.Length < 14)
        {
            isNicknameValid = true;
        }
        else
        {
            isNicknameValid = false;
        }

        CheckValidity();
    }

    void SearchLocation_OnGeocoderResponse(ForwardGeocodeResponse response)
    {
        if (null == response || response.Features.Count == 0)
        {
            locationMapText.text = "<color=red>Invalid</color>";
            isLocationValid = false;
            CheckValidity();
        }
        else if (null != response.Features && response.Features.Count > 0)
        {
            locationMapText.text = "<color=green>" + response.Features[0].PlaceName + "</color>";
            longlat = response.Features[0].Center;

            isLocationValid = true;
            CheckValidity();
        }
    }

    void CheckValidity()
    {
        if (isLocationValid && isNicknameValid)
        {
            GuestButton.interactable = FBButton.interactable = true;
        }
        else
        {
            GuestButton.interactable = FBButton.interactable = false;
        }
    }
    #endregion

    public void InitNewPlayer()
    {
        registerGO.SetActive(true);
        isLocationValid = false;
        isNicknameValid = false;

        LeanTween.delayedCall(1, () => {
            LeanTween.value(1, 0, 1).setOnUpdate((float v) => {
                loadingScreen.alpha = v;
            }).setEaseOutExpo().setOnComplete(()=> { loadingScreen.gameObject.SetActive(false); });
        });
    }
    #region device authentication




    public void DeviceRegister()
    {
        PlayerData pd = new PlayerData();
        pd.displayName = displayNameInput.text;
        pd.teleportalPos = longlat.ToString();
        pd.teleportalDesc = RemoveRichTextTag(locationMapText.text);
        pd.playerPos = longlat.ToString();

        GERM.InitNewPlayerData(pd);
    }

    //helper
    string RemoveRichTextTag(string s)
    {
        StringBuilder sb = new StringBuilder(s.Length);
        bool tag = false;
        for (int index = 0; index < s.Length; index++)
        {
            char c = s[index];
            if (tag)
            {
                if (c == '>')
                {
                    tag = false;
                }
            }
            else
            {
                if (c == '<')
                {
                    tag = true;
                }
                else
                {
                    sb.Append(c);
                }
            }
        }

        return sb.ToString();
    }
    #endregion
}
