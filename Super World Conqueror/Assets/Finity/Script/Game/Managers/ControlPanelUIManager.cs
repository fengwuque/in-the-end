﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPanelUIManager : MonoBehaviour {
    [SerializeField] GameObject[] panelUIs;

    int currentActivatedUIID;
	// Use this for initialization
	void Start () {
        currentActivatedUIID = -1;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Activate(int panelUIID)
    {
        if(currentActivatedUIID!=-1)
            panelUIs[currentActivatedUIID].SetActive(false);

        if (currentActivatedUIID == panelUIID)
        {
            currentActivatedUIID = -1;
        }
        else
        {
            currentActivatedUIID = panelUIID;
            if(currentActivatedUIID != -1)
                panelUIs[currentActivatedUIID].SetActive(true);
        }
    }
}
