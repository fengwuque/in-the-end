﻿using Mapbox.Geocoding;
using Mapbox.Unity;
using Mapbox.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForwardGeocodeManager : MonoBehaviour {

    ForwardGeocodeResource _resource;

    Vector2d _coordinate;
    public Vector2d Coordinate
    {
        get
        {
            return _coordinate;
        }
    }

    bool _hasResponse;
    public bool HasResponse
    {
        get
        {
            return _hasResponse;
        }
    }

    public ForwardGeocodeResponse Response { get; private set; }

    //public event Action<> OnGeocoderResponse = delegate { };
    public event Action<ForwardGeocodeResponse> OnGeocoderResponse = delegate { };

    void Awake()
    {
        _resource = new ForwardGeocodeResource("");
        //_resource.Proximity = new Vector2d(500, 500);
        string[] types = new string[2];
        types[0] = "place";
        types[1] = "poi";
        //_resource.Types = types;
    }

    private void Start()
    {
        Init("mall near 1.3811349, 103.7679192");
    }

    void Init(string searchString)
    {
        _hasResponse = false;
        if (!string.IsNullOrEmpty(searchString))
        {
            _resource.Query = searchString;
            MapboxAccess.Instance.Geocoder.Geocode(_resource, HandleGeocoderResponse);
        }

        
    }

    void HandleGeocoderResponse(ForwardGeocodeResponse res)
    {
        _hasResponse = true;
        if (null == res)
        {
            Debug.Log("null");

        }
        else if (null != res.Features && res.Features.Count > 0)
        {
            var center = res.Features[0].Center;
            //_inputField.text = string.Format("{0},{1}", center.x, center.y);
            _coordinate = res.Features[0].Center;
            Debug.Log("not null");
        }
        Response = res;
        OnGeocoderResponse(res);

        for(int i = 0; i < Response.Features.Count; ++i)
        {

            Debug.Log(Response.Features[i].Type);
        }

    }
}
