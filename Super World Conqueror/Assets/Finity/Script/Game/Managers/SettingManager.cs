﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingManager : MonoBehaviour {
    [SerializeField] GameObject settingObj;
    [SerializeField] int targetFrameRate;
    [SerializeField] bool useTargetFrameRate;
    // Use this for initialization
    void Start ()
    {
        if(useTargetFrameRate)
        Application.targetFrameRate = targetFrameRate;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SettingPressed()
    {
        settingObj.SetActive(true);
    }

    public void CloseSettingPressed()
    {
        settingObj.SetActive(false);
    }
}
