﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeManager : MonoBehaviour {
    GSEventRequestManager GERM;
	// Use this for initialization
	void Start () {
        GERM = GameSparksManager.instance.GetComponent<GSEventRequestManager>();

    }
	
	public void UseTeleportal()
    {
        GERM.UseTeleportal();
    }
}
