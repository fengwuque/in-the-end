﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSpriteManager : MonoBehaviour
{
    
    [SerializeField] GameObject character;
    [SerializeField] SpriteRenderer headEQSR;
    [SerializeField] SpriteRenderer bodyEQSR;
    [SerializeField] SpriteRenderer shoulderFrontEQSR;
    [SerializeField] SpriteRenderer shoulderBackEQSR;
    [SerializeField] SpriteRenderer leg1EQSR;
    [SerializeField] SpriteRenderer leg2EQSR;
    [SerializeField] SpriteRenderer handFrontEQSR;
    [SerializeField] SpriteRenderer handBackEQSR;
    [SerializeField] SpriteRenderer capeEQSR;
    [SerializeField] SpriteRenderer feet1EQSR;
    [SerializeField] SpriteRenderer feet2EQSR;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetEQSprite(EQSpriteInfo eqSpriteInfo)
    {
        headEQSR.sprite = eqSpriteInfo.head;
        bodyEQSR.sprite = eqSpriteInfo.top;
        shoulderFrontEQSR.sprite = eqSpriteInfo.shoulderFront;
        shoulderBackEQSR.sprite = eqSpriteInfo.shoulderBack;
        leg1EQSR.sprite = eqSpriteInfo.bottom;
        leg2EQSR.sprite = eqSpriteInfo.bottom;
        handFrontEQSR.sprite = eqSpriteInfo.handFront;
        handBackEQSR.sprite = eqSpriteInfo.handBack;
        capeEQSR.sprite = eqSpriteInfo.cape;

    }
}

public class EQSpriteInfo
{
    public Sprite head;
    public Sprite top;
    public Sprite shoulderFront;
    public Sprite shoulderBack;
    public Sprite bottom;
    public Sprite handFront;
    public Sprite handBack;
    public Sprite cape;
    public Sprite feet;
}