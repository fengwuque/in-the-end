﻿using Mapbox.Map;
using Mapbox.Unity.Map;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSpawnTileManager
{

    UnwrappedTileId myID;
    List<MapSpawns> listOfMapSpawns;
    const int NO_OF_MIN_PER_DAY = 1440;
    Pooler[] myPoolers;
    AbstractMap myMap;
    Dictionary<string, MapSpawns> spawnedObjs;
    List<GameObject> listOfSpawnedGO;

    List<float> spawnChance;
    public void Init(int randomSeed, UnwrappedTileId id, Pooler[] poolers, AbstractMap map, int currentT)
    {
        myMap = map;
        myID = id;
        myPoolers = poolers;

        int newSeed = randomSeed * myID.X % myID.Y * myID.Z;
        Random.InitState(newSeed);
        InitSpawnChance();
        listOfMapSpawns = new List<MapSpawns>();
        spawnedObjs = new Dictionary<string, MapSpawns>();
        listOfSpawnedGO = new List<GameObject>();

        int t = 0;
        //while (t < NO_OF_MIN_PER_DAY - _spawningData.aliveTime)
        //{
        //    t += Random.Range(_spawningData.minSpawnTimeMinutes, _spawningData.maxSpawnTimeMinutes);
        //    float randomFloat = Random.Range(0f, 1f);
        //    SpawnTypes type = SpawnTypes.MONSTER;

        //    if (randomFloat < spawnChance[(int)SpawnTypes.GOLD])
        //    {
        //        type = SpawnTypes.GOLD;
        //    }
        //    else if (randomFloat < spawnChance[(int)SpawnTypes.GEM])
        //    {
        //        type = SpawnTypes.GEM;
        //    }
        //    else if (randomFloat < spawnChance[(int)SpawnTypes.EQUIPMENT])
        //    {
        //        type = SpawnTypes.EQUIPMENT;
        //    }

        //    string ID = System.DateTime.Now.ToString() + " - " + myID.ToString() + " - " + t.ToString();

        //    listOfMapSpawns.Add(new MapSpawns(ID, type, t));
        //}

        Update(currentT);
    }

    void InitSpawnChance()
    {
        //spawnChance = new List<float>();
        //spawnChance.Add(_spawningData.goldSpawnChance);
        //spawnChance.Add(_spawningData.goldSpawnChance + _spawningData.gemSpawnChance);
        //spawnChance.Add(_spawningData.goldSpawnChance + _spawningData.gemSpawnChance + _spawningData.equipmentSpawnChance);
    }

    public void Update(int currentT)
    {
        for (int i = 0; i < listOfMapSpawns.Count; ++i)
        {
            if (listOfMapSpawns[i].spawnTime > currentT)
                return;

            //if (listOfMapSpawns[i].spawnTime + _spawningData.aliveTime > currentT && !spawnedObjs.ContainsKey(listOfMapSpawns[i].myID))
            //{
            //    spawnedObjs.Add(listOfMapSpawns[i].myID, listOfMapSpawns[i]);
            //    Spawn(listOfMapSpawns[i]);
            //}
        }
    }

    void Spawn(MapSpawns spawn)
    {
        //GameObject newSpawn = myPoolers[(int)spawn.myType].GetPoolObject();
        //newSpawn.name = "Tile ID: " + myID.ToString() + " type " + spawn.myType.ToString();
        //Vector3 tilePos = myMap.MapVisualizer.ActiveTiles[myID].transform.position;
        ////newSpawn.transform.position = tilePos + (new Vector3(Random.Range(-1, 1), 0, Random.Range(-1, 1))).normalized * Random.Range(0, _spawningData.maxDistanceFromCenterTile);
        ////newSpawn.GetComponent<MapObject>().Init(newSpawn.name, spawn.spawnTime);
        //listOfSpawnedGO.Add(newSpawn);
    }

    public void DestroySpawnedObj()
    {
        for (int i = 0; i < listOfSpawnedGO.Count; ++i)
        {
            listOfSpawnedGO[i].GetComponent<PoolObject>().Death();
        }
        listOfSpawnedGO.Clear();
    }
}
