using Mapbox.Geocoding;
using Mapbox.Json;
using Mapbox.Platform;
using Mapbox.Unity;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using Mapbox.Utils.JsonConverters;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReverseGeocodeManager : MonoBehaviour {

    ReverseGeocodeResource _resource;

    Geocoder _geocoder;

    Vector2d _coordinate;

    bool _hasResponse;
    public bool HasResponse
    {
        get
        {
            return _hasResponse;
        }
    }

    public ReverseGeocodeResponse Response { get; private set; }

    public event EventHandler<EventArgs> OnGeocoderResponse;

    void Awake()
    {
        OnGeocoderResponse += UpdateMapFeatures;
    }

    public void Init()
    {

        // hack
        _coordinate = new Vector2d(1.378446,103.763427);
        _resource = new ReverseGeocodeResource(_coordinate);
        
        
        string[] types = new string[6];
        types[0] = "place";
        types[1] = "poi";
        types[2] = "region";
        types[3] = "postcode";
        types[4] = "neighborhood";
        types[5] = "locality";

        _resource.Types = types;
        _geocoder = MapboxAccess.Instance.Geocoder;
        _geocoder.Geocode(_resource, HandleGeocoderResponse);
    }

    /// <summary>
    /// Handles the geocoder response by updating coordinates and notifying observers.
    /// </summary>
    /// <param name="res">Res.</param>
    void HandleGeocoderResponse(ReverseGeocodeResponse res)
    {
        _hasResponse = true;
        Response = res;
        if (OnGeocoderResponse != null)
        {
            OnGeocoderResponse(this, EventArgs.Empty);
        }
    }

    // Use this for initialization
    void Start () {
        Init();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void UpdateMapFeatures(object sender, System.EventArgs e)
    {
        List<Feature> listOfFeatures = Response.Features;

        for(int i = 0; i < listOfFeatures.Count; ++i)
        {
            Debug.Log(listOfFeatures[i].PlaceName);
        }
        
        //Debug.Log(JsonConvert.SerializeObject(Response, Formatting.Indented, JsonConverters.Converters));
    }
}
