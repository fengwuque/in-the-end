﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompassManager : MonoBehaviour {
    [SerializeField] Transform playerT;
    [SerializeField] Transform compassT;
    [SerializeField] Transform compass2T;

    // Update is called once per frame
    void Update () {
        compassT.localEulerAngles = new Vector3(0,0,playerT.localEulerAngles.y);
        compass2T.localEulerAngles = new Vector3(0, 0, -playerT.localEulerAngles.y*0.5f);
    }
}
