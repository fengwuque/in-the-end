﻿using Mapbox.Unity.Location;
using Mapbox.Unity.Map;
using Mapbox.Unity.Utilities;
using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MovementController : MonoBehaviour
{

    public GameObject player;
    public float maxDistFromCenter;
    public float speedMult;
    Vector3 centerPos;
    public float rotationMult;
    bool isDown;
    
    public PlayerMarkerAnimationManager PMAM;
    [SerializeField] DesignVariablesScriptableObject DVSO;

    [SerializeField] CanvasGroup CG;

    [SerializeField] Vector3 zoomOutScale;
    [SerializeField] GameObject movementBtn;
    [SerializeField] Camera myCam;
    [SerializeField] float defaultFieldOfView;
    [SerializeField] float maxFieldOfView;
    [SerializeField] GameObject outerRing;

    [Header("Track Position")]
    [SerializeField] float savePosInterval;

    GSEventRequestManager GERM;
    PlayerAccountManager PAM;

    Vector3 outerRingOriginScale;
    const float outerRingZoomInScale = 1.2f;

    Vector3 zoomInScale;

    AbstractMap myMap;


    private void Awake()
    {
        
    }

    public void OnInit(PlayerData pd)
    {
        
    }

    // Use this for initialization
    void Start () {
        GERM = GameSparksManager.instance.GetComponent<GSEventRequestManager>();
        PAM = GameSparksManager.instance.GetComponent<PlayerAccountManager>();
        GERM.onInitialized += OnInit;
        centerPos = movementBtn.transform.position;

        myMap = LocationProviderFactory.Instance.mapManager;
        zoomInScale = CG.gameObject.transform.localScale;
        outerRingOriginScale = outerRing.transform.localScale;
        Init();
    }

    void UpdateWorldPosition()
    {
        //if(myMap == null)
        //    myMap = LocationProviderFactory.Instance.mapManager;

        GERM.SetPlayerPos(myMap.WorldToGeoPosition(player.transform.position).ToString());
    }

    void Init()
    {
        InvokeRepeating("UpdateWorldPosition", savePosInterval, savePosInterval);

        //LocationProviderFactory.Instance.mapManager.OnInitialized += () =>
        //{
        //    myMap = LocationProviderFactory.Instance.mapManager;

        //    Vector2d geoPos = Conversions.StringToLatLon(PAM.GetPlayerPos());
        //        Vector3 myPos = myMap.GeoToWorldPosition(geoPos);
        //        player.transform.position = myPos;
        //    Debug.Log("world pos = " + myPos);
        //    Debug.Log("LONGLAT = " + geoPos);
        //};
    }
    

    // Update is called once per frame
    void Update () {
		if(isDown)
        {
            LeanTween.cancel(movementBtn);

            Vector3 direction = Vector3.Normalize(Input.mousePosition - centerPos);
            float speed = Vector2.Distance(centerPos, Input.mousePosition);

            if (speed < maxDistFromCenter)
            {
                movementBtn.transform.position = Input.mousePosition;
            }
            else
            {
                movementBtn.transform.position = centerPos + direction * maxDistFromCenter;
                speed = maxDistFromCenter;
            }

            myCam.fieldOfView = defaultFieldOfView + (maxFieldOfView - defaultFieldOfView) * (speed / maxDistFromCenter);


            Vector3 temp = direction * speed * speedMult * Time.deltaTime;
            //player.transform.position += new Vector3(temp.x, 0, temp.y);

            if(direction.y < 0)
            {
                player.transform.position -= player.transform.forward * Mathf.Abs(direction.y) * speed * speedMult * Time.deltaTime;

                float rotationSpeed = direction.y+1;

                Vector3 rotateDirection = direction.x > 0 ? Vector3.up : Vector3.down;
                player.transform.Rotate(rotateDirection * rotationSpeed * rotationMult * Time.deltaTime);
            }
            else
            {
                player.transform.position += player.transform.forward * Mathf.Abs(direction.y) * speed * speedMult * Time.deltaTime;

                float rotationSpeed = 1 - direction.y;

                Vector3 rotateDirection = direction.x > 0 ? Vector3.up : Vector3.down;
                player.transform.Rotate(rotateDirection * rotationSpeed * rotationMult * Time.deltaTime);
            }
        }
	}

    public void OnPointerDown()
    {
        if (!CG.interactable)
            return;
        isDown = true;
        LeanTween.scale(outerRing, outerRingOriginScale * outerRingZoomInScale, 0.5f).setEaseOutExpo();
        PMAM.Move();
    }

    public void OnPointerUp()
    {
        if (!CG.interactable)
            return;
        isDown = false;
        LeanTween.move(movementBtn, centerPos, 0.5f).setEaseOutElastic();
        LeanTween.value(myCam.gameObject, myCam.fieldOfView, defaultFieldOfView,0.5f).setOnUpdate((float v) =>
        {
            myCam.fieldOfView = v;
        }).setEaseOutExpo();

        PMAM.StoppedMoving();
        LeanTween.scale(outerRing, outerRingOriginScale, 0.5f).setEaseOutElastic();
    }

    public void ZoomOut()
    {
        CG.interactable = false;
        LeanTween.cancel(CG.gameObject);
        LeanTween.value(CG.gameObject,CG.alpha, 0, DVSO.generalOutDuration).setOnUpdate((float v) => { CG.alpha = v;}).setEase(DVSO.generalType);
        LeanTween.scale(CG.gameObject, zoomOutScale,DVSO.generalOutDuration).setEase(DVSO.generalType);
    }
    
    public void ZoomIn()
    {
        
        LeanTween.cancel(CG.gameObject);
        LeanTween.value(CG.gameObject,CG.alpha, 1, DVSO.generalInDuration).setOnUpdate((float v) => { CG.alpha = v; }).setEase(DVSO.generalType);
        LeanTween.scale(CG.gameObject, zoomInScale, DVSO.generalInDuration).setEase(DVSO.generalType).setOnComplete(()=> { CG.interactable = true; });
    }
}
