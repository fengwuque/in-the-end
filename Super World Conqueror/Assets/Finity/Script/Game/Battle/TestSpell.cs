﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSpell : MonoBehaviour
{
    [SerializeField] SpellDataScriptableObject spell;
    [SerializeField] float[] values;
    [SerializeField] ReferenceOperandScriptableObject[] references;
    [SerializeField] OperatorScriptableObject add;
    [SerializeField] OperatorScriptableObject multiply;

    Dictionary<ReferenceOperandScriptableObject, float> referenceValues;
    // Use this for initialization
    void Start () {
        referenceValues = new Dictionary<ReferenceOperandScriptableObject, float>();

        for(int i = 0; i < references.Length; ++i)
        {
            referenceValues.Add(references[i], values[i]);
        }

        
        for (int i = 0; i < spell.listOfEffects.Length; ++i)
        {
            CastSpellEffect(spell.listOfEffects[i]);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //TODO: EFFECTS NEEDS TO KNOW WHICH STAT TO AFFECT ON THE TARGET
    void CastSpellEffect(SpellEffectScriptableObject effect)
    {
        if(referenceValues.ContainsKey(effect.referenceType))
        {
            referenceValues[effect.referenceType] = effect.referenceValue;
        }
        else
        {
            referenceValues.Add(effect.referenceType, effect.referenceValue);
        }
        Debug.Log(effect.name + " is applied on " + effect.targetType + " for " + effect.numberOfTurns + " turns! value = " + ResolveExpression(effect.formula));
    }

    float ResolveExpression(OperandScriptableObject operand)
    {
        if (operand.GetType() == typeof(OperandScriptableObject))
        {
            Debug.Log(operand.name + " = " + operand.Get());
            return operand.Get();
        }
        else if (operand.GetType() == typeof(ConstantOperandScriptableObject))
        {
            Debug.Log(operand.name + " = " + ((ConstantOperandScriptableObject)operand).value);
            return ((ConstantOperandScriptableObject)operand).value;
        }
        else if(operand.GetType() == typeof(ReferenceOperandScriptableObject))
        {
            Debug.Log(operand.name + " = " + referenceValues[(ReferenceOperandScriptableObject)operand]);
            return referenceValues[(ReferenceOperandScriptableObject)operand];
        }
        else if (operand.GetType() == typeof(ExpressionScriptableObject))
        {
            ExpressionScriptableObject expression = (ExpressionScriptableObject)operand;
            float expressionResult = 0;
            if (expression.op == add)
            {
                expressionResult = ResolveExpression(expression.lhs) + ResolveExpression(expression.rhs);
            }
            else
            {
                expressionResult = ResolveExpression(expression.lhs) * ResolveExpression(expression.rhs);
            }

            Debug.Log(operand.name + " = " + expressionResult);

            return expressionResult;
        }
        else if (operand.GetType() == typeof(AssignmentScriptableObject))
        {
            float assignmentResult = ResolveExpression(((AssignmentScriptableObject)operand).formula);
            Debug.Log(operand.name + " = " + assignmentResult);
            return assignmentResult;
        }

        return 0;
    }
}
