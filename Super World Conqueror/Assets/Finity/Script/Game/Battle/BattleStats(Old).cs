﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleStatsOld : MonoBehaviour
{
    public string characterName;
    public Dictionary<STAT_TYPE, float> stats;
    int _currentHP;
    public int currentHP
    {
        get { return _currentHP; }
        set {
            _currentHP = value;
            if(_currentHP < 0)
            {
                _currentHP = 0;
                isAlive = false;
                KillingBlowSlowTime();
            }
            else if(_currentHP > Mathf.RoundToInt( stats[STAT_TYPE.HEALTH_POINT]))
            {
                _currentHP = Mathf.RoundToInt(stats[STAT_TYPE.HEALTH_POINT]);
            }
        }
    }
        
    public bool isMagicAttack;

    float _attackCD;

    float _updateInterval;

    bool isAttacking;

    public float attackInterval;
    public bool isAlive;

    float _killingBlowTimeScale, _slowTimeDuration;

    void KillingBlowSlowTime()
    {
        LeanTween.value(1, _killingBlowTimeScale, _slowTimeDuration).setOnUpdate((float v) =>
        {
            Time.timeScale = v;
        }).setEaseOutExpo().setOnComplete(() => { Time.timeScale = 1; });
    }

    public void Init(CharacterSavedDataScriptableObject characterSavedData, PlayerCharacterInfoScriptableObject PCISO, StatsDataScriptableObject SDSO, EquipmentDataScriptableObject EDSO,float updateInterval, float killingBlowTimeScale, float slowTimeDuration)
    {
        isAlive = true;
        _killingBlowTimeScale = killingBlowTimeScale;
        _slowTimeDuration = slowTimeDuration;
        //Character stats at level
        stats = PCISO.GetCharacterStats(characterSavedData.level);
        characterName = characterSavedData.characterName;
        _updateInterval = updateInterval;
        isAttacking = false;

        //Multiply by relation
        for (int i = 0; i < SDSO.statsRelation.Count; ++i)
        {
            stats[SDSO.statsRelation[i].statType] *= SDSO.statsRelation[i].statValue;
        }

        Dictionary<EQUIPMENT_SETS,RARITY> eqRarity = EDSO.GetEqSetsRarity();
        Dictionary<STAT_TYPE, float> statRelations = SDSO.GetStatsRelation();

        Dictionary<EQUIPMENT_SETS, int> noOfEqSets = new Dictionary<EQUIPMENT_SETS, int>();
        float dmgMultiplier = 0;
        for (int i = 0; i < characterSavedData.equipmentSlotID.Count; ++i)
        {
            EquipSaveData eq = characterSavedData.inventory[characterSavedData.equipmentSlotID[i].slotID];
            float rarityMultiplier = EDSO.eqStats.rarityMult[(int)eqRarity[eq.set]];
            float allMultiplier = rarityMultiplier * EDSO.eqStats.baseStatMult * Mathf.Pow(EDSO.eqStats.lvlMult, eq.level);

            if(!noOfEqSets.ContainsKey(eq.set))
            {
                noOfEqSets.Add(eq.set, 1);
            }
            else
            {
                ++noOfEqSets[eq.set];
            }

            if(eq.part == EQUIPMENT_PARTS.WEAPON)
            {
                isMagicAttack = EDSO.GetIsMagicWeapon(eq.set);
                stats[STAT_TYPE.ATTACK_SPEED] = EDSO.GetWeaponAttackSpeedBase(eq.set);
                dmgMultiplier = EDSO.GetDamageRatio(eq.set);
            }

            for (int j = 0; j < characterSavedData.inventory[characterSavedData.equipmentSlotID[i].slotID].stats.Count; ++j)
            {
                Stat current = eq.stats[j];

                if(current.statType < STAT_TYPE.ATTACK_SPEED)
                {
                    stats[current.statType] += (1 + SDSO.statDifference * current.statValue) * statRelations[current.statType] * allMultiplier;
                }
                else
                {
                    stats[current.statType] += (SDSO.minMaxStat[current.statType].x + current.statValue * (SDSO.minMaxStat[current.statType].y - SDSO.minMaxStat[current.statType].x));
                }
            }
        }

        List<Stat> setBonuses = EDSO.GetEqSetBonus(noOfEqSets);

        stats[STAT_TYPE.NORMAL_DAMAGE] *= dmgMultiplier;

        Debug.Log(characterName);
        for (int i = 0; i < setBonuses.Count; ++i)
        {
            stats[setBonuses[i].statType] += stats[setBonuses[i].statType]*setBonuses[i].statValue;
        }

        stats[STAT_TYPE.ATTACK_SPEED] *= (1+ stats[STAT_TYPE.ATTACK_SPEED_INCREMENT]);
        currentHP = (int)stats[STAT_TYPE.HEALTH_POINT];
        _attackCD = attackInterval = 1f /stats[STAT_TYPE.ATTACK_SPEED];

        foreach (KeyValuePair<STAT_TYPE, float> setNo in stats)
        {
            Debug.Log(setNo.Key + " = " + setNo.Value);
        }
    }

    public bool UpdateAttackCD(float animRatio)
    {
        _attackCD -= _updateInterval;

        if(_attackCD <= attackInterval * animRatio)
        {
            if(!isAttacking)
            {
                isAttacking = true;
                return true;
            }
            else
            {
                if (_attackCD <= 0)
                {
                    _attackCD = attackInterval;
                    isAttacking = false;
                }
                return false;
            }
        }

        return false;
    }
}
