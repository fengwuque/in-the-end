﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCalculation : MonoBehaviour {
    [SerializeField] AssignmentScriptableObject testAssignment1;
    [SerializeField] AssignmentScriptableObject testAssignment2;

    [SerializeField] OperatorScriptableObject add;
    [SerializeField] OperatorScriptableObject multiply;

    // Use this for initialization
    void Start () {
        CalculateAssignment(testAssignment1);
        CalculateAssignment(testAssignment2);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void CalculateAssignment(AssignmentScriptableObject test)
    {
        Debug.Log(ResolveExpression(test.formula));
    }

    float ResolveExpression(OperandScriptableObject operand)
    {
        if(operand.GetType() == typeof(OperandScriptableObject))
        {
            return operand.Get();
        }
        else if(operand.GetType() == typeof(ConstantOperandScriptableObject))
        {
            return ((ConstantOperandScriptableObject)operand).value;
        }
        else if(operand.GetType() == typeof(ExpressionScriptableObject))
        {
            ExpressionScriptableObject expression = (ExpressionScriptableObject)operand;

            if(expression.op == add)
            {
                return ResolveExpression(expression.lhs) + ResolveExpression(expression.rhs);
            }
            else
            {
                return ResolveExpression(expression.lhs) * ResolveExpression(expression.rhs);
            }
        }

        return 0;
    }
}
