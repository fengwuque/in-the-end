﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BattleWordManager : MonoBehaviour
{
    string[] listOfWords;
    [SerializeField] string listOfAllHardWords;
    [SerializeField] string listOfAllSimpleWords;
    public TextMeshProUGUI playerText;
    public TextMeshProUGUI[] enemiesText;
    CharacterWordManager playerWordM;
    CharacterWordManager[] enemiesWordM;


    [SerializeField] WordBattleCharacterUI playerUI;
    [SerializeField] WordBattleCharacterUI[] enemiesUI;

    [SerializeField] GameObject[] enemiesGO;
    [SerializeField] GameObject difficultyGO;

    [Header("Settings")]
    public int noOfEnemies;
    public int enemyHP;
    public int playerHP;
    public float attackResetDecrement;
    public bool isSimpleWords;
    public float playerAttackCD;
    public float enemiesAttackCD;
    float currentPlayerAttackCooldown;
    float[] currentEnemiesAttackCooldown;
    int currentPlayerHP;
    int[] currentEnemiesHP;
    int[] noOfTimesParried;

    bool isGameOver;

    [Header("Difficulty")]
    public Text leveltxt;
    public Text enemyCDtxt;
    public Text enemyResettxt;
    public Text enemyCounttxt;

    // Use this for initialization
    void Start () {
        listOfWords = isSimpleWords ? listOfAllSimpleWords.Split(',') : listOfAllHardWords.Split(',');

        Init();
    }

    void Init()
    {
        isGameOver = false;
        playerWordM = new CharacterWordManager();
        playerWordM.Init(this, GetNewWord(), playerText, true, 0);
        enemiesWordM = new CharacterWordManager[noOfEnemies];

        currentPlayerAttackCooldown = 0;
        currentPlayerHP = playerHP;
        currentEnemiesAttackCooldown = new float[noOfEnemies];
        currentEnemiesHP = new int[noOfEnemies];
        noOfTimesParried = new int[noOfEnemies];

        for(int i = 0; i < enemiesGO.Length; ++i)
        {
            enemiesGO[i].SetActive(false);
        }

        for (int i = 0; i < noOfEnemies; ++i)
        {
            enemiesGO[i].SetActive(true);
            currentEnemiesAttackCooldown[i] = 0;
            currentEnemiesHP[i] = enemyHP;
            noOfTimesParried[i] = 0;
            enemiesWordM[i] = new CharacterWordManager();
            enemiesWordM[i].Init(this, GetNewWord(), enemiesText[i], false, i);
            enemiesUI[i].UpdateHP(enemyHP);
        }
    }
	
	// Update is called once per frame
	void Update () {

	}

    private void FixedUpdate()
    {
        if (isGameOver)
            return;

        UpdatePlayerAttack();
        UpdateEnemiesAttack();
    }

    public void OnMouseOverKey(char keyString)
    {
        playerWordM.OnKeyOver(keyString);

        for(int i = 0; i < noOfEnemies; ++i)
        {
            if(currentEnemiesHP[i] > 0)
            {
                enemiesWordM[i].OnKeyOver(keyString);
            }
        }
    }

    public string GetNewWord()
    {
        return listOfWords[Random.Range(0, listOfWords.Length)];
    }

    void UpdatePlayerAttack()
    {
        if(currentPlayerAttackCooldown >= playerAttackCD)
        {
            playerUI.UpdateFill(0);
            playerWordM.SetEnableUpdate(true);
        }
        else
        {
            currentPlayerAttackCooldown += Time.fixedDeltaTime;
            float percent = (playerAttackCD - currentPlayerAttackCooldown) / playerAttackCD;
            playerUI.UpdateFill(percent);
        }
    }

    void UpdateEnemiesAttack()
    {
        for(int i = 0; i < noOfEnemies; ++i)
        {
            if( currentEnemiesHP[i] > 0)
            {

                float effectiveCD = enemiesAttackCD * Mathf.Pow(attackResetDecrement, noOfTimesParried[i]);

                if (currentEnemiesAttackCooldown[i] >= effectiveCD)
                {
                    noOfTimesParried[i] = 0;
                    currentEnemiesAttackCooldown[i] = 0;
                    enemiesUI[i].UpdateFill(1);
                    --currentPlayerHP;
                    playerUI.UpdateHP(currentPlayerHP);

                    if(currentPlayerHP <= 0)
                    {
                        isGameOver = true;
                    }

                }
                else
                {
                    currentEnemiesAttackCooldown[i] += Time.fixedDeltaTime;
                    float percent = (effectiveCD - currentEnemiesAttackCooldown[i]) / effectiveCD;
                    enemiesUI[i].UpdateFill(percent);
                }
            }
        }
    }
    

    public void CompletedWord(bool isPlayer, int enemyIndex)
    {
        if(isPlayer)
        {
            currentPlayerAttackCooldown = 0;

            for(int i = 0; i < noOfEnemies; ++i)
            {
                if(currentEnemiesHP[i]>0)
                {
                    --currentEnemiesHP[i];
                    enemiesUI[i].UpdateHP(currentEnemiesHP[i]);

                    if(i == noOfEnemies-1 && currentEnemiesHP[i] == 0)
                        isGameOver = true;

                    return;
                }
            }

            isGameOver = true;
        }
        else
        {
            ++noOfTimesParried[enemyIndex];
            currentEnemiesAttackCooldown[enemyIndex] = 0;
        }
    }

    public void Restart()
    {
        Init();
    }

    public void SetDifficultySettings(bool enabled)
    {
        isGameOver = enabled;
        difficultyGO.SetActive(enabled);

        if (!enabled)
            Restart();
    }

    public void SetLevel(bool isIncrease)
    {
        int level = int.Parse(leveltxt.text);
        if (isIncrease)
        {
            ++level;
        }
        else
        {
            --level;
        }
        enemiesAttackCD = 10 - (level - 10) * 0.2f;
        attackResetDecrement = 0.9f - (level - 10) * 0.02f;

        leveltxt.text = level.ToString();
        enemyCDtxt.text = enemiesAttackCD.ToString("F2");
        enemyResettxt.text = attackResetDecrement.ToString("F2");


    }

    public void SetEnemyCount(bool isIncrease)
    {
        if(isIncrease)
        {
            if(noOfEnemies < 5)
            {
                ++noOfEnemies;
            }
        }
        else
        {
            if(noOfEnemies>2)
            {
                --noOfEnemies;
            }
        }


        enemyCounttxt.text = noOfEnemies.ToString();
    }
}

public class CharacterWordManager
{
    string _currentWord;
    int _charIndex;
    BattleWordManager _BWM;
    TextMeshProUGUI _text;
    bool _enableUpdate;
    bool _isPlayer;
    int _enemyIndex;
    public void OnKeyOver(char keyChar)
    {
        if (_charIndex >= _currentWord.Length || !_enableUpdate)
            return;

        if(keyChar == char.ToUpper(_currentWord[_charIndex]))
        {
            ++_charIndex;

            if (_charIndex == _currentWord.Length)
            {
                _currentWord = _BWM.GetNewWord();
                _charIndex = 0;
                if(_isPlayer)
                {
                    _enableUpdate = false;
                }

                _BWM.CompletedWord(_isPlayer, _enemyIndex);
            }

            UpdateUI();
        }
    }

    public void SetEnableUpdate(bool enableUpdate)
    {
        _enableUpdate = enableUpdate;
        UpdateUI();
    }

    public void Init(BattleWordManager BWM, string word, TextMeshProUGUI text, bool isPlayer, int index)
    {
        _BWM = BWM;
        _currentWord = word;
        _charIndex = 0;
        _text = text;
        _enableUpdate = isPlayer ? false : true;
        _isPlayer = isPlayer;
        _enemyIndex = index;
        UpdateUI();
    }

    void UpdateUI()
    {
        _text.text = "";

        for(int i = 0; i < _currentWord.Length; ++i)
        {
            if(_enableUpdate)
            {
                if (i < _charIndex)
                {
                    _text.text += "<color=green>" + _currentWord[i] + "</color>";
                }
                else
                {
                    _text.text += "<color=white>" + _currentWord[i] + "</color>";
                }
            }
            else
            {
                _text.text += "<color=#808080ff>" + _currentWord[i] + "</color>";
            }
        }
    }
}