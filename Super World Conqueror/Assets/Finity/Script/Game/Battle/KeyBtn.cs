﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class KeyBtn : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler
{
    [SerializeField] char key;

    [SerializeField] BattleWordManager BWM;
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}
    

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
        if(Input.GetMouseButton(0))
            BWM.OnMouseOverKey(key);
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        BWM.OnMouseOverKey(key);
    }
}
