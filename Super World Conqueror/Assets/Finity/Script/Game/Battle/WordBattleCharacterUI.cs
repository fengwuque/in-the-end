﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WordBattleCharacterUI : MonoBehaviour {
    [SerializeField] TextMeshProUGUI hp;
    [SerializeField] Image timeRemainingImg;
    
    public void UpdateFill(float fill)
    {
        timeRemainingImg.fillAmount = fill;
    }

    public void UpdateHP(int hpText)
    {
        hp.text = hpText.ToString();
    }
}
