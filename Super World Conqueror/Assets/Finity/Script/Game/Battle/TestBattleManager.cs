﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBattleManager : MonoBehaviour
{
    [SerializeField] OrbSelectionManagerScriptableObject OSM;
    [SerializeField] SpellDataManagerScriptableObject SDM;
    [SerializeField] BattleStatsManagerScriptableObject BSM;
    [SerializeField] OrbRotationManagerScriptableObject ORM;
    [SerializeField] BattleUIManagerScriptableObject BUM;
    [SerializeField] TargetManagerScriptableObject TM;
    [SerializeField] CastSpellManagerScriptableObject CSM;
    [SerializeField] EnemiesAIManagerScriptableObject EAM;
    [SerializeField] DamageFeedbackManagerScriptableObject DFM;
    [SerializeField] SpellFeedbackManagerScriptableObject SFM;
    [SerializeField] BattleTransformManagerScriptableObject BTM;
    [SerializeField] UnitScriptableObject playerUnit;
    [SerializeField] UnitScriptableObject[] aiUnit;
    [SerializeField] OrbDataScriptableObject playerOrbData;
    [SerializeField] Transform[] orbsPos;
    [SerializeField] Transform orbsParent;
    [SerializeField] Transform playerMainTransform;
    [SerializeField] Transform playerSummon1Transform;
    [SerializeField] Transform playerSummon2Transform;
    [SerializeField] Transform playerWallTransform;
    [SerializeField] Transform enemyMainTransform;
    [SerializeField] Transform enemySummon1Transform;
    [SerializeField] Transform enemySummon2Transform;
    [SerializeField] Transform enemyWallTransform;
    [SerializeField] Transform targetParent;
    [SerializeField] Transform playerWallParent;
    [SerializeField] Transform enemyWallParent;
    [SerializeField] Transform spellParent;
    [SerializeField] GameObject[] hackedButtons;
    [SerializeField] GameObject castSpellBtn;
    [SerializeField] Transform UIParent;
    private void Awake()
    {
        OSM.Init(castSpellBtn);
        
        SDM.Init();
        CSM.Init(playerWallParent, enemyWallParent);
        
    }

    // Use this for initialization
    void Start ()
    {
        InitStats();
        InitTarget();
        
        EAM.Init(BSM.enemy, aiUnit);
        ORM.InitOrb(playerOrbData.orbTypes, orbsParent, orbsPos);
        SFM.Init(spellParent);
        InitUI();
    }

    void InitTarget()
    {
        List<Transform> targets = new List<Transform>();
        targets.Add(enemyMainTransform);
        targets.Add(enemySummon1Transform);
        targets.Add(enemySummon2Transform);
        targets.Add(enemyWallTransform);
        TM.InitTargets(targets, targetParent);
    }

    void InitUI()
    {
        List<Dictionary<ReferenceOperandScriptableObject, float>> stats = new List<Dictionary<ReferenceOperandScriptableObject, float>>();
        List<Transform> parentsT = new List<Transform>();

        stats.Add(BSM.player.mainReference);
        parentsT.Add(playerMainTransform);
        stats.Add(BSM.enemy.mainReference);
        parentsT.Add(enemyMainTransform);

        stats.Add(BSM.player.wall);
        parentsT.Add(playerWallParent);
        stats.Add(BSM.enemy.wall);
        parentsT.Add(enemyWallParent);

        if (BSM.enemy.summon1Reference  != null)
        {
            stats.Add(BSM.enemy.summon1Reference);
            parentsT.Add(enemySummon1Transform);
        }
        if(BSM.enemy.summon2Reference != null)
        {
            stats.Add(BSM.enemy.summon2Reference);
            parentsT.Add(enemySummon2Transform);
        }

        BTM.Init(stats.ToArray(), parentsT.ToArray());
        BUM.Init(stats.ToArray(), parentsT.ToArray(), UIParent);
        DFM.InitFeedbackManager(UIParent, stats.ToArray(), parentsT.ToArray());
        SkillInfoManagerScriptableObject.Instance.Init(UIParent);
    }

    void InitStats()
    {
        List<Dictionary<ReferenceOperandScriptableObject, float>> enemiesRefs = new List<Dictionary<ReferenceOperandScriptableObject, float>>();
        for (int i = 0; i < aiUnit.Length; ++i)
        {
            aiUnit[i].Init();
            enemiesRefs.Add(aiUnit[i].attributes);
            hackedButtons[i].SetActive(true);// HACKED
        }

        playerUnit.Init();

        BSM.Init(playerUnit.attributes, enemiesRefs);
    }
	
	// Update is called once per frame
	void Update () {
       
    }

    private void FixedUpdate()
    {
        DFM.UpdateConcatenation();
    }
}