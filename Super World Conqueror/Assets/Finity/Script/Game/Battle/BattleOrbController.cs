﻿using RoboRyanTron.Unite2017.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleOrbController : MonoBehaviour {
    public GameEvent onSelected;
    public Animator animator;
    public EnumScriptableObject orbType;
    public OrbSelectionManagerScriptableObject OSM;

    public bool isSelected;
    bool isRotating = false;

    public void Selected()
    {
        //if (isRotating) return;
        //Debug.Log(gameObject.name);
        isSelected = true;
        if (OSM.breakEverySelection)
        {
            GetComponent<Button>().interactable = false;
            animator.SetBool("IsActive", true);
            animator.SetTrigger("Breaks");
            OSM.SelectOrb(orbType);
        }
        else
        {
            animator.SetTrigger("Selected");

            OSM.SelectOrb(orbType);
        }
        
        onSelected.Raise();
        
    }
	
    public void SelfDestruct()
    {
        
        Destroy(gameObject);
    }

    public void SetActive(bool isActive)
    {
        GetComponent<Button>().interactable = isActive;
        animator.SetBool("IsActive",isActive);
    }

    public void OnPlayerSpellCast()
    {
        if(isSelected)
        {
            GetComponent<Button>().interactable = false;
            animator.SetBool("IsActive", true);
            animator.SetTrigger("Breaks");
        }
    }

    public void On3OrbsSelected()
    {
        if (isSelected) return;
        GetComponent<Button>().interactable = false;
        animator.SetBool("IsActive", false);
    }

    public void Break()
    {
        isSelected = true;
        animator.SetTrigger("Breaks");
    }

    public void OnStartRotation()
    {
        isRotating = true;
    }

    public void OnEndRotation()
    {
        isRotating = false;
    }
}
