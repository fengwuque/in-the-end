﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BattleHPUI : MonoBehaviour
{
    public Image currentHPImage;
    public TextMeshProUGUI currentHPText;
	
    public void UpdateHP(int current, int max)
    {
        if(current == 0)
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
            currentHPImage.fillAmount = (float)current / (float)max;
            currentHPText.text = current.ToString() + "/" + max.ToString();
        }
        
    }
}
