﻿namespace Mapbox.Unity.Map
{
	using System.Collections;
	using Mapbox.Unity.Location;
    using Mapbox.Unity.Utilities;
    using Mapbox.Utils;
    using UnityEngine;

	public class InitializeMapWithLocationProvider : MonoBehaviour
	{
		[SerializeField]
		AbstractMap _map;

		ILocationProvider _locationProvider;

        GSEventRequestManager GERM;

        PlayerData _pd;
        bool _gsInitialized;
        
        private void Start()
        {
            GERM = GameSparksManager.instance.GetComponent<GSEventRequestManager>();
            _gsInitialized = false;
            CheckGameSparkInitialized();
        }

        void CheckGameSparkInitialized()
        {
            if (GERM.IsInitialized())
            {
                Init(GERM.GetComponent<PlayerAccountManager>().GetPlayerData());
            }
            else
            {
                Invoke("CheckGameSparkInitialized", GERM.GetRetryInterval());
            }
        }

        //      IEnumerator Start()
        //{
        //	yield return null;
        //          //_locationProvider = LocationProviderFactory.Instance.DefaultLocationProvider;
        //          //_locationProvider.OnLocationUpdated += LocationProvider_OnLocationUpdated; ;
        //      }

        void Init(PlayerData pd)
        {
            _pd = pd;
            _locationProvider = LocationProviderFactory.Instance.DefaultLocationProvider;
            _locationProvider.OnLocationUpdated += LocationProvider_OnLocationUpdated; ;
        }

		void LocationProvider_OnLocationUpdated(Unity.Location.Location location)
		{
            Vector2d pos = Conversions.StringToLatLon(_pd.playerPos);

            _locationProvider.OnLocationUpdated -= LocationProvider_OnLocationUpdated;
			_map.Initialize(pos, _map.AbsoluteZoom);
		}
	}
}